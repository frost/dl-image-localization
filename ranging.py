import torch
import numpy as np

def rss_to_dist(rss, db_shift=-8, truth=None, mode='3gpp'):
    assert mode in ['3gpp', 'hata', 'cost231-hata']
    freq = 868
    h_t = 1.5
    h_r = 30

    if mode == 'hata':
        at = 3.2 * np.log10(11.75*h_t)**2 - 4.97
        numer = (-rss - 69.55 - 26.161 * np.log10(freq) + 13.82 * np.log10(h_r) + at)
        denom = 44.9 - 6.55 * np.log10(h_r)
        d = 10 ** ( numer / denom) * 1e3
    elif mode == 'cost231-hata':
        at = 3.2 * np.log10(11.75*h_t)**2 - 4.97
        numer = (-rss - 46.3 - 33.9 * np.log10(freq) + 13.82 * np.log10(h_r) + at - 3)
        denom = 44.9 - 6.55 * np.log10(h_r)
        d = 10 ** ( numer / denom) * 1e3
    elif mode == '3gpp':
        d = 10 ** ((-rss + db_shift)/ 37.6)
    #d4 = 10 ** ((-rss + 1.3 - 21*np.log10(freq/900)) / 36.7)

    return d

def min_max(distances, rx_locations):
    boxes = rx_locations - distances[:,None], rx_locations + distances[:,None]
    bounds = boxes[0].max(axis=0)[0], boxes[1].min(axis=0)[0]
    return (bounds[0] + bounds[1]) / 2