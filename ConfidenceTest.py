import time
import matplotlib
import numpy as np
import os
import pickle
import argparse
from IPython import embed
from localization import *
from Splot import *
from dataset import RSSLocDataset
from scipy.stats import pearsonr
import scipy
plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

lr = 5e-4
test_time_only = False
should_train = True
should_load_model = True
restart_optimizer = False
save_pickles = True
force_april_test = True

# Specify params
num_epochs = 4000
min_sensors = 6
arch = 'unet'
sensor_dropout = False
force_num_tx = 1
include_elevation_map = False
set_random_power = False

batch_size = 32 if should_train else 4
run_splot = True
threshold_percentile = 0.05
better_epoch_limit = 400

num_training_repeats = 4

device = torch.device('cuda')
all_results = {}

def main():
    global dataset_index
    global meter_scale
    cmd_line_params = []
    parser = argparse.ArgumentParser()
    parser.add_argument("--param_selector", type=int, default=-1, help='Index of pair for selecting params')
    args = parser.parse_args()

    for ms, di, split, marker_value in [
        #[0.25, 1, 'radius5', 0.01],
        #[20, 6, 'radius350', 0.01],
        #[20, 6, 'radius350', 0.0],
        #[10, 6, 'random', 0.01],
        #[20, 6, 'random', 0.01],
        [30, 6, 'random', 0.01],
        #[30, 6, 'april_nov', 0.01],
        #[30, 6, 'july_nov', 0.01],
        #[1, 4, 'random', 0.01],
        #[30, 6, 'july_selftest_nov_limited', 0.01],
        #[30, 6, 'nov_selftest_july_limited', 0.01],
        #[30, 6, 'july_selftest_nov', 0.01],
        #[30, 6, 'nov_selftest_july', 0.01],
        #[30, 6, 'grid2', 0.01],
        #[30, 6, 'grid3', 0.01],
        #[30, 6, 'grid4', 0.01],
        #[30, 6, 'grid5', 0.01],
        #[30, 6, 'grid7', 0.01],
        #[30, 6, 'grid10', 0.01],
        #[30, 6, 'grid15', 0.01],
        #[30, 6, 'grid20', 0.01],
        ##[30, 6, 'radius350', 0.01],
        ##[30, 6, 'radius350', 0.0],
        #[50, 7, 'random', 0.01], 
        #[70, 7, 'random', 0.01], 
        #[100, 7, 'random', 0.01], 
        #[150, 7, 'grid2', 0.01], 
        #[150, 7, 'grid3', 0.01], 
        #[150, 7, 'grid4', 0.01], 
        #[150, 7, 'grid5', 0.01], 
        #[150, 7, 'grid7', 0.01], 
        #[150, 7, 'grid10', 0.01], 
        #[150, 7, 'grid15', 0.01], 
        #[150, 7, 'grid20', 0.01], 
        #[150, 7, 'radius1200', 0.01], 
        #[150, 7, 'radius1200', 0.0], 
    ]:
        for random_state in range(0,num_training_repeats):
            for loss_func, loss_label in zip(
                [
                    #CoMLoss(),
                    #SlicedEarthMoversDistance(num_projections=100, scaling=1/ms, p=1),
                    nn.MSELoss()
                ], [
                    #'com',
                    #'emd_p1',
                    'mse'
            ]):
              for arch in ['unet_ensemble']:
                cmd_line_params.append([ms, di, split, random_state, loss_func, loss_label, marker_value, arch])
    
    if args.param_selector > -1:
        param_list = [cmd_line_params[args.param_selector] ]
    else:
        param_list = cmd_line_params
    
    pickle_index = 16

    for ind, param_set in enumerate(param_list):
        meter_scale, dataset_index, split, random_state, loss_func, loss_label, marker_value, arch = param_set
        print(param_set)
        params = LocConfig(dataset_index, data_split=split, arch=arch, batch_size=batch_size, meter_scale=meter_scale, random_state=random_state, include_elevation_map=include_elevation_map, force_num_tx=force_num_tx, sensor_dropout=sensor_dropout, min_sensors=min_sensors, device=device, set_random_power=set_random_power, tx_marker_value=marker_value, better_epoch_limit=better_epoch_limit)

        rldataset = RSSLocDataset(params, random_state=random_state)
        train_key, test_key = rldataset.make_datasets(make_val=True)

        rldataset.print_dataset_stats()

        dlloc = DLLocalization(rldataset, lr=lr, loss_object=loss_func)
        param_string = f"{params}_{loss_label}_{marker_value}"
        PATH = 'models/notchpeak/%s__model.pt' % param_string

        model_ending = 'train_val.'
        global all_results

        if not should_train: 
            filename = PATH.replace('model.', 'model_' + model_ending)
            if os.path.exists(filename):
                explore_confidence(filename, dlloc, rldataset, train_key, test_key, param_set)
        if should_train:
            print('Training on dataset', train_key, 'for', num_epochs, 'epochs')
            print('Training', PATH)
            load_model_file = PATH
            dlloc.train_model(num_epochs, save_model_file=PATH, load_model=should_load_model, restart_optimizer=restart_optimizer, load_model_file=load_model_file)
        if not should_train and ((ind+1) % (num_training_repeats)) == 0 and save_pickles:
            datatype = 'grid' if 'grid' in split else 'seasonal'
            pickle_file = f'ood_{datatype}_results_fixed_{pickle_index}.pkl'
            while os.path.exists(pickle_file):
                pickle_index += 1
                pickle_file = f'ood_{datatype}_results_fixed_{pickle_index}.pkl'
            with open(pickle_file, 'wb') as f:
                print('Printing', pickle_file)
                pickle.dump(all_results, f)
            all_results = {}
    if not should_train and len(all_results) > 0 and save_pickles:
        pickle_file = f'ood_{datatype}_results_fixed_{pickle_index}.pkl'
        while os.path.exists(pickle_file):
            pickle_index += 1
            pickle_file = f'ood_{datatype}_results_fixed_{pickle_index}.pkl'
        with open(pickle_file, 'wb') as f:
            print('Printing', pickle_file)
            pickle.dump(all_results, f)


def explore_confidence(filename, dlloc, rldataset, train_key, test_key, param_set):
    print('Loading model from %s for testing' % filename)
    if 'selftest' in rldataset.params.data_split:
        test_key = 'july_test' if not force_april_test else 'april_test'
        if force_april_test:
            rldataset.data[test_key].make_tensors()

    train_data = rldataset.data[train_key]
    test_data = rldataset.data[test_key]
    dlloc.load_model(filename)
    train_res = dlloc.test(dataloader=train_data.ordered_dataloader, save_images=True)
    test_res = dlloc.test(dataloader=test_data.ordered_dataloader, save_images=True)
    train_images = [train_res['pred_imgs'][0], train_res['x_imgs'][0], train_res['y_imgs'][0]]
    test_images = [test_res['pred_imgs'][0], test_res['x_imgs'][0], test_res['y_imgs'][0]]

    if dataset_index == 6:
        center_point =  np.array([1100,1200])
    elif dataset_index == 7:
        center_point = np.array([2500,3000])
    semd = SlicedEarthMoversDistance(num_projections=300)

    tr_truth = np.array(train_data.ordered_dataloader.dataset.tensors[1].cpu())[:,0,1:] * meter_scale
    tr_preds = np.array(train_res['preds'])[0] * meter_scale
    tr_err = np.array(train_res['error']).flatten()
    tr_max = train_images[0].reshape(len(train_images[0]), -1).max(axis=1)
    tr_max_mean = train_images[0].mean(axis=1).reshape(len(train_images[0]), -1).max(axis=1)
    tr_max_norm = (train_images[0] / train_images[0].reshape(len(train_images[0]),-1).sum(axis=1).reshape(-1,1,1,1)).reshape(len(train_images[0]), -1).max(axis=1)
    tr_min = train_images[0].reshape(len(train_images[0]), -1).min(axis=1)
    tr_swd = semd(torch.tensor(train_images[0]).to(device),torch.tensor(train_images[2]).to(device)).cpu().numpy()
    tr_dist = train_data.tx_vecs[:,0,3]
    tr_radial_dist = np.linalg.norm(tr_truth.squeeze() - center_point, axis=1)
    tr_rss = train_data.ordered_dataloader.dataset.tensors[0][:,:,0].cpu().numpy()
    tr_tx_separation = np.zeros_like(tr_err)
    for i in range(len(tr_tx_separation)):
        tx_dist = np.linalg.norm(tr_truth.squeeze() - tr_truth[i,0], axis=1)
        tx_dist[i] = 1e6
        tr_tx_separation[i] = tx_dist[tx_dist.argmin()]

    te_truth = np.array(test_data.ordered_dataloader.dataset.tensors[1].cpu())[:,0,1:] * meter_scale
    te_preds = np.array(test_res['preds'])[0] * meter_scale
    te_err = np.array(test_res['error']).flatten()
    te_max = test_images[0].reshape(len(test_images[0]), -1).max(axis=1)
    te_max_mean = test_images[0].mean(axis=1).reshape(len(test_images[0]), -1).max(axis=1)
    te_max_norm = (test_images[0] / test_images[0].reshape(len(test_images[0]),-1).sum(axis=1).reshape(-1,1,1,1)).reshape(len(test_images[0]), -1).max(axis=1)
    te_min = test_images[0].reshape(len(test_images[0]), -1).min(axis=1)
    te_swd = semd(torch.tensor(test_images[0]).to(device),torch.tensor(test_images[2]).to(device)).cpu().numpy()
    te_dist = test_data.tx_vecs[:,0,3]
    te_radial_dist = np.linalg.norm(te_truth.squeeze() - center_point, axis=1)
    te_rss = test_data.ordered_dataloader.dataset.tensors[0][:,:,0].cpu().numpy()
    te_tx_separation = np.zeros_like(te_err)
    for i in range(len(te_tx_separation)):
        tx_dist = np.linalg.norm(tr_truth.squeeze() - te_truth[i,0], axis=1)
        te_tx_separation[i] = tx_dist[tx_dist.argmin()]


    if run_splot:
        splot = Splot(rldataset, train_key, device)
        tr_splot_preds = splot.estimate(train_data.ordered_dataloader.dataset.tensors[0])
        te_splot_preds = splot.estimate(test_data.ordered_dataloader.dataset.tensors[0])

    if 'radius' in rldataset.params.data_split:
        tv_keys = [key for key in list(rldataset.data.keys())[1:] if 'inradius' in key and 'train_in' not in key]
    elif 'selftest' in rldataset.params.data_split:
        tv_keys = ['nov_test']
    else:
        tv_keys = [key for key in list(rldataset.data.keys())[1:] if 'train_val' in key]
    tv_images, tv_truth, tv_preds, tv_err, tv_max, tv_swd, tv_dist, tv_radial_dist, tv_rss, tv_tx_separation = [ [] for _ in range(10) ]
    tv_splot_preds = []
    for tv_ind, tv_key in enumerate(tv_keys):
        tv_data = rldataset.data[tv_key]
        if not hasattr(tv_data,'dataloader'):
            tv_data.make_tensors()
        tv_res = dlloc.test(dataloader=tv_data.ordered_dataloader, save_images=True)
        tv_image = [tv_res['pred_imgs'][0], tv_res['x_imgs'][0], tv_res['y_imgs'][0]]
        tv_images.append(tv_image)
        tv_truth.append(np.array(tv_data.ordered_dataloader.dataset.tensors[1].cpu())[:,0,1:] * meter_scale)
        #tv_truth.append(np.array(tv_res['truth']))
        tv_preds.append(np.array(tv_res['preds'])[0] * meter_scale)
        tv_err.append(np.array(tv_res['error']).flatten())
        tv_max.append(tv_image[0].reshape(len(tv_image[0]), -1).max(axis=1))
        tv_swd.append(semd(torch.tensor(tv_image[0]).to(device),torch.tensor(tv_image[2]).to(device)).cpu().numpy())
        tv_dist.append(tv_data.tx_vecs[:,0,3])
        tv_radial_dist.append(np.linalg.norm(tv_truth[tv_ind].squeeze() - center_point, axis=1))
        tv_rss.append(tv_data.ordered_dataloader.dataset.tensors[0][:,:,0].cpu().numpy())
        tv_tx_separation.append(np.zeros_like(tv_err[tv_ind]))
        for i in range(len(tv_tx_separation[tv_ind])):
            tx_dist = np.linalg.norm(tr_truth.squeeze() - tv_truth[tv_ind][i,0], axis=1)
            tv_tx_separation[tv_ind][i] = tx_dist[tx_dist.argmin()]
        if run_splot:
            tv_splot_preds.append( splot.estimate(tv_data.ordered_dataloader.dataset.tensors[0]) )

    tv_images = [np.concatenate([images[i] for images in tv_images]) for i in range(3) ]
    print([len(vec) for vec in tv_truth])
    tv_truth = np.concatenate(tv_truth)
    tv_preds = np.concatenate(tv_preds)
    tv_err = np.concatenate(tv_err)
    tv_max = np.concatenate(tv_max)
    tv_max_mean = tv_images[0].mean(axis=1).reshape(len(tv_images[0]), -1).max(axis=1)
    tv_max_norm = (tv_images[0] / tv_images[0].reshape(len(tv_images[0]),-1).sum(axis=1).reshape(-1,1,1,1)).reshape(len(tv_images[0]), -1).max(axis=1)
    tv_min = tv_images[0].reshape(len(tv_images[0]), -1).min(axis=1)
    #tv_swd = np.concatenate(tv_swd)
    tv_dist = np.concatenate(tv_dist)
    tv_radial_dist = np.concatenate(tv_radial_dist)
    tv_rss = np.concatenate(tv_rss)
    tv_tx_separation = np.concatenate(tv_tx_separation)
    if run_splot:
        tv_splot_preds = np.concatenate(tv_splot_preds)

    #np.savez('DS6_30m_splot_preds.npz', tr_splot_preds=tr_splot_preds, te_splot_preds=te_splot_preds, tv_splot_preds=tv_splot_preds)
    if run_splot:
        tr_splot_err = np.linalg.norm(tr_splot_preds - tr_truth, axis=1)
        tv_splot_err = np.linalg.norm(tv_splot_preds - tv_truth, axis=1)
        te_splot_err = np.linalg.norm(te_splot_preds - te_truth, axis=1)

    te_max_area = np.zeros_like(te_max)
    for i in range(len(te_err)):
        pred = te_preds[i,0:2].astype(int) // meter_scale
        te_max_area[i] = test_images[0][i,0, pred[1] - 2: pred[1]+3, pred[0]-2:pred[0]+3].sum()
    tr_max_area = np.zeros_like(tr_max)
    for i in range(len(tr_err)):
        pred = tr_preds[i,0:2].astype(int) // meter_scale
        tr_max_area[i] = train_images[0][i,0, pred[1] - 2: pred[1]+3, pred[0]-2:pred[0]+3].sum()
    tv_max_area = np.zeros_like(tv_max)
    for i in range(len(tv_err)):
        pred = tv_preds[i,0:2].astype(int) // meter_scale
        tv_max_area[i] = train_images[0][i,0, pred[1] - 2: pred[1]+3, pred[0]-2:pred[0]+3].sum()
    tr_pred_separation = np.zeros_like(tr_tx_separation)
    for i in range(len(tr_pred_separation)):
        pred_dist = np.linalg.norm(tr_truth.squeeze() - tr_preds[i,0:2], axis=1)
        pred_dist[i] = 1e6
        tr_pred_separation[i] = pred_dist[pred_dist.argmin()]
    te_pred_separation = np.zeros_like(te_tx_separation)
    for i in range(len(te_pred_separation)):
        pred_dist = np.linalg.norm(tr_truth.squeeze() - te_preds[i,0:2], axis=1)
        te_pred_separation[i] = pred_dist[pred_dist.argmin()]
    tv_pred_separation = np.zeros_like(tv_tx_separation)
    for i in range(len(tv_pred_separation)):
        pred_dist = np.linalg.norm(tr_truth.squeeze() - tv_preds[i,0:2], axis=1)
        tv_pred_separation[i] = pred_dist[pred_dist.argmin()]
    tr_combined = np.stack((tr_max, tr_max_area, tr_min, tr_pred_separation)).T
    tv_combined = np.stack((tv_max, tv_max_area, tv_min, tv_pred_separation)).T
    te_combined = np.stack((te_max, te_max_area, te_min, te_pred_separation)).T

    def make_error_line_plot( conf=0.2):
        conf_te_inds = np.where(te_max_mean > conf)[0]
        plt.scatter(tr_truth[:,0], tr_truth[:,1], c='red', marker='x', alpha=0.1)
        for truth, pred in zip(te_truth[conf_te_inds], te_preds[conf_te_inds]):
            plt.plot([truth[0], pred[0]], [truth[1], pred[1]], c='tab:blue', alpha=0.2, linewidth=0.5)
        sc = plt.scatter(te_preds[conf_te_inds,0], te_preds[conf_te_inds,1], c=te_err[conf_te_inds], norm=matplotlib.colors.LogNorm())
        plt.scatter(te_truth[conf_te_inds,0], te_truth[conf_te_inds,1], c=te_err[conf_te_inds], marker='x', norm=matplotlib.colors.LogNorm())
        plt.colorbar(sc)
        plt.show()

    def make_error_conf_plot(f='plots/error_conf_test_%s.png' % filename, figsize=(6,4)):
        fig = plt.figure(figsize=(6, 4))
        plt.scatter(te_max, te_err)
        plt.xlabel('Confidence')
        plt.ylabel('Error [m]')
    def save_test_images(test_ind=18):
        temp = 'plots/test_img_%s_60m_REP.png' % str(test_ind)
        titles = [ temp.replace('REP', 'pred'), temp.replace('REP', 'x'), temp.replace('REP', 'x_scaled'), temp.replace('REP', 'y')]
        images = [test_images[0][test_ind,0], test_images[1][test_ind,1], test_images[1][test_ind,0], test_images[2][test_ind,0]]
        for title, img in zip(titles, images):
            fig = plt.figure(frameon=False)
            ax = plt.Axes(fig, [0.,0., 1., 1.])
            ax.set_axis_off()
            fig.add_axes(ax)
            ax.imshow(img, origin='lower')
            fig.savefig(title)
    def plot_scatters(x_var='error', mode='test', use_radial=True, transpose=False):
        fig, axs = plt.subplots(2,2)
        modes = ['train', 'test', 'tv'] if mode == 'both' else [mode]

        for mode in modes:
            if mode == 'train':
                err, mean_max, pred_max, max_area, dist = tr_err, tr_max_mean, tr_max, tr_max_area, tr_dist
                if use_radial:
                    dist = tr_radial_dist
            elif mode == 'test':
                err, mean_max, pred_max, max_area, dist = te_err, te_max_mean, te_max, te_max_area, te_dist
                if use_radial:
                    dist = te_radial_dist
            elif mode == 'tv':
                err, mean_max, pred_max, max_area, dist = tv_err, tv_max_mean, tv_max, tv_max_area, tv_dist
                if use_radial:
                    dist = tv_radial_dist
        
            if x_var == 'error':
                x_data = err
                x_label = 'Error'
                others = max_area, pred_max, mean_max, dist
                labels = 'Max Area Conf', 'Max Confidence', 'Max Conf of Avg Pred', 'Tx Rx Distance' if not use_radial else "Distance from Center"
            elif x_var == 'max_area':
                x_data = max_area
                x_label = 'Max Area Conf'
                others = mean_max, pred_max, err, dist
                labels = 'Max Conf of Avg Pred', 'Max Confidence', 'Error', 'Tx Rx Distance' if not use_radial else "Distance from Center"
            elif x_var == 'max_conf':
                x_data = pred_max
                x_label = 'Max Confidence'
                others = max_area, mean_max, err, dist
                labels = 'Max Area Conf', 'Max Conf of Avg Pred', 'Error', 'Tx Rx Distance' if not use_radial else "Distance from Center"
            elif x_var == 'dist':
                x_data = dist
                x_label = 'Tx Rx Distance' if not use_radial else 'Distance from Center'
                others = max_area, pred_max, mean_max, err
                labels = 'Max Area Conf', 'Max Confidence', 'Max Conf of Avg Pred', 'Error'

            for ax, other, y_label in zip(axs.flat, others, labels):
                if transpose:
                    ax.scatter(other, x_data, label=mode)
                    ax.set_xlabel(y_label)
                    ax.set_ylabel(x_label)
                else:
                    ax.scatter(x_var, other, label=mode)
                    ax.set_xlabel(x_label)
                    ax.set_ylabel(y_label)
        plt.legend()
        plt.show()
    def plot_images(start_ind, left_images=tv_images, right_images=test_images, num_imgs=3):
        fig, axs = plt.subplots(num_imgs, 2, sharex=True, sharey=True)
        for i in range(num_imgs):
            axs[i,0].imshow(left_images[0][start_ind+i,0])
            axs[i,0].set_title('%.4f  %.4f' % (left_images[0][start_ind+i,0].max(), left_images[0][start_ind+i,0].min()) )
            axs[i,1].imshow(right_images[0][start_ind+i,0])
            axs[i,1].set_title('%.4f  %.4f' % (right_images[0][start_ind+i,0].max(), right_images[0][start_ind+i,0].min()) )
            print(tv_err[start_ind+i], te_err[start_ind+i])
        plt.show()
    def plot_mixed_images(start_ind, top_images=tv_images[0], bottom_images=test_images[0], savefig=False, filename=None):
        fig, axs = plt.subplots(2,5, sharex=True, sharey=True)
        for i in range(5):
            axs[0,i].imshow(top_images[start_ind,i])
            axs[1,i].imshow(bottom_images[start_ind,i])
            axs[0,i].set_title('%.4f' % top_images[start_ind,i].max())
            axs[1,i].set_title('%.4f' % bottom_images[start_ind,i].max())
        if savefig:
            fig.savefig(filename)
        plt.show()
    
    def plot_details(start_ind, images, truth, splot_preds=None, bad_slice=slice(None)):
        if rldataset.elevation_tensors is None:
            rldataset.make_elevation_tensors()
        fig, axs = plt.subplots(2,2, sharex=True, sharey=True)
        axs[0,0].imshow(images[0][bad_slice][start_ind].mean(axis=0), origin='lower')
        axs[0,0].scatter(truth[bad_slice][start_ind][0]/meter_scale, truth[bad_slice][start_ind][1]/meter_scale, c='white')
        if splot_preds is not None:
            axs[0,0].scatter(splot_preds[bad_slice][start_ind][0]/meter_scale, splot_preds[bad_slice][start_ind][1]/meter_scale, c='green')
        axs[0,1].imshow(rldataset.elevation_tensors[0], origin='lower')
        axs[1,0].imshow(images[1][bad_slice][start_ind,0], origin='lower')
        axs[1,1].imshow(images[1][bad_slice][start_ind,1], origin='lower')
        plt.show()

    def get_separation(percentile=.1, split='random', use_mean=False, use_tv_for_thresh=False, plot=True, savefig=False, filename=None):
        tr_max_arr = tr_max_mean if use_mean else tr_max
        tv_max_arr = tv_max_mean if use_mean else tv_max
        te_max_arr = te_max_mean if use_mean else te_max
        threshold = np.quantile(tv_max_arr, percentile) if use_tv_for_thresh else np.quantile(tr_max_arr, percentile)
        tv_percent_filtered = ((tv_max_arr > threshold)).sum() / len(tv_err)
        te_percent_filtered = ((te_max_arr > threshold)).sum() / len(te_err)
        print("TV Filtered: %.2f    Test Filtered: %.2f" % (tv_percent_filtered, te_percent_filtered))
        if plot or savefig:
            if 'radius' in split:
                plt.scatter(tr_max_arr, tr_radial_dist, label='Train')
                plt.scatter(tv_max_arr, tv_radial_dist, label='Val')
                plt.scatter(te_max_arr, te_radial_dist, label='Test')
                plt.ylabel('Distance from center of train data')
            elif 'grid' in split:
                plt.scatter(tr_max_arr, tr_tx_separation, label='Train')
                plt.scatter(tv_max_arr, tv_tx_separation, label='Val')
                plt.scatter(te_max_arr, te_tx_separation, label='Test')
                plt.ylabel('Distance from nearest Train Tx')
            plt.xlabel('Max Confidence') if use_mean else plt.xlabel('Max Confidence of Mean Preds')
            plt.title('10-percent Threshold %.2f, Allow %.2f of outliers' % (threshold, te_percent_filtered))
            plt.legend()
            if savefig and filename is not None:
                plt.savefig(filename)
            if plot:
                plt.show()
            else:
                plt.cla()
        return threshold, tv_percent_filtered, te_percent_filtered

    tr_spread = ((train_images[0] > 0.005).sum(axis=1) > 0).reshape(len(train_images[0]), -1).sum(axis=1)
    tv_spread = ((tv_images[0] > 0.005).sum(axis=1) > 0).reshape(len(tv_images[0]), -1).sum(axis=1)
    te_spread = ((test_images[0] > 0.005).sum(axis=1) > 0).reshape(len(test_images[0]), -1).sum(axis=1)

    maxes, sums = [], []
    for preds in [train_images[0], tv_images[0], test_images[0]]:
        peak_locs = np.argmax( preds.reshape((preds.shape[0], 5, -1)), axis=-1)
        peak_locs = np.array(np.unravel_index(peak_locs, preds.shape[2:])).T
        max_distances = np.zeros((5, len(peak_locs[0])))
        sum_distances = np.zeros((5, len(peak_locs[0])))
        for i, loc in enumerate(peak_locs):
            max_distances[i] = np.linalg.norm(peak_locs - loc[None,:], axis=2).max(axis=0)
            sum_distances[i] = np.linalg.norm(peak_locs - loc[None,:], axis=2).sum(axis=0)
        maxes.append(max_distances.max(axis=0))
        sums.append(sum_distances.max(axis=0))
    tr_spread_max, tv_spread_max, te_spread_max = maxes
    tr_spread_sum, tv_spread_sum, te_spread_sum = sums

    def get_spread_separation(percentile=.1, use_sum=False, use_tv_for_thresh=False):
        tr_max_arr = tr_spread_sum if use_sum else tr_spread_max
        tv_max_arr = tv_spread_sum if use_sum else tv_spread_max
        te_max_arr = te_spread_sum if use_sum else te_spread_max
        threshold = np.quantile(tv_max_arr, 1 - percentile) if use_tv_for_thresh else np.quantile(tr_max_arr, 1 - percentile)
        tv_percent_filtered = ((tv_max_arr > threshold)).sum() / len(tv_err)
        te_percent_filtered = ((te_max_arr > threshold)).sum() / len(te_err)
        print("Spread: TV Filtered: %.2f    Test Filtered: %.2f" % (tv_percent_filtered, te_percent_filtered))
        return threshold, tv_percent_filtered, te_percent_filtered
    
    #for use_mean in [True, False]:
    #    for use_tv in [True, False]:
    #        mean_str = "MeanMax" if use_mean else "Max"
    #        tv_str = "Train Val" if use_tv else "Train"
    #        loss_str = "mse" 
    #        if isinstance(dlloc.loss_func, nn.MSELoss):
    #            loss_str = "mse"
    #        else:
    #            loss_str = f"emd_p{dlloc.loss_func.p}"
    #            if dlloc.loss_func.normalize:
    #                loss_str += "_norm"
    #        loss_str += str(rldataset.params.tx_marker_value)
    #        plot_filename = os.path.join('plots/confidence_scatters', filename[7:-3] + mean_str + '.pdf')
    #        threshold, tv_percent_filtered, test_percent_filtered =  get_separation(split=rldataset.params.data_split, use_mean=use_mean, use_tv_for_thresh=use_tv, plot=False, savefig=True, filename=plot_filename)
    #        correlation = pearsonr(tr_max_mean if use_mean else tr_max, tr_radial_dist if 'radius' in rldataset.params.data_split else tr_tx_separation).statistic
    #        with open('confidence_emd_mse_comparison.txt', 'a') as fi:
    #            out_str = f"{dataset_index},{rldataset.params.data_split},{meter_scale},{loss_str},{rldataset.params.random_state},{mean_str},{tv_str},{tv_err.mean()},{te_err.mean()},{threshold},{tv_percent_filtered},{test_percent_filtered},{correlation}\n"
    #            fi.writelines([out_str])
    #        print(f"Threshold with {mean_str} and {tv_str}")
    #        print(f"Error: TR: {tr_err.mean()} TV: {tv_err.mean()} TE: {te_err.mean()}")
    #        print(f"Fiter: {threshold} TE: {test_percent_filtered}")
    plot_filename = os.path.join('plots/confidence_scatters', filename[7:-3] + '_fixed.pdf')
    threshold, tvpf, tepf = get_separation(percentile=threshold_percentile, split=rldataset.params.data_split, use_mean=True, use_tv_for_thresh=True, plot=False, savefig=False, filename=plot_filename)
    spread_threshold, spread_tvpf, spread_tepf = get_spread_separation(percentile=threshold_percentile, use_sum=False, use_tv_for_thresh=True)

    tr_combined_preds = np.zeros_like(tr_splot_preds)
    tv_combined_preds = np.zeros_like(tv_splot_preds)
    te_combined_preds = np.zeros_like(te_splot_preds)
    tr_combined_preds[tr_max_mean > threshold] = tr_preds[tr_max_mean > threshold, :2]
    tv_combined_preds[tv_max_mean > threshold] = tv_preds[tv_max_mean > threshold, :2]
    te_combined_preds[te_max_mean > threshold] = te_preds[te_max_mean > threshold, :2]
    tr_combined_preds[tr_max_mean <= threshold] = tr_splot_preds[tr_max_mean <= threshold, :2]
    tv_combined_preds[tv_max_mean <= threshold] = tv_splot_preds[tv_max_mean <= threshold, :2]
    te_combined_preds[te_max_mean <= threshold] = te_splot_preds[te_max_mean <= threshold, :2]
    tr_combined_err = np.linalg.norm(tr_combined_preds - tr_truth, axis=1)
    tv_combined_err = np.linalg.norm(tv_combined_preds - tv_truth, axis=1)
    te_combined_err = np.linalg.norm(te_combined_preds - te_truth, axis=1)

    tr_combined_spread_preds = np.zeros_like(tr_splot_preds)
    tv_combined_spread_preds = np.zeros_like(tv_splot_preds)
    te_combined_spread_preds = np.zeros_like(te_splot_preds)
    tr_combined_spread_preds[tr_spread_max <= spread_threshold] = tr_preds[tr_spread_max <= spread_threshold, :2]
    tv_combined_spread_preds[tv_spread_max <= spread_threshold] = tv_preds[tv_spread_max <= spread_threshold, :2]
    te_combined_spread_preds[te_spread_max <= spread_threshold] = te_preds[te_spread_max <= spread_threshold, :2]
    tr_combined_spread_preds[tr_spread_max > spread_threshold] = tr_splot_preds[tr_spread_max > spread_threshold, :2]
    tv_combined_spread_preds[tv_spread_max > spread_threshold] = tv_splot_preds[tv_spread_max > spread_threshold, :2]
    te_combined_spread_preds[te_spread_max > spread_threshold] = te_splot_preds[te_spread_max > spread_threshold, :2]
    tr_combined_spread_err = np.linalg.norm(tr_combined_spread_preds - tr_truth, axis=1)
    tv_combined_spread_err = np.linalg.norm(tv_combined_spread_preds - tv_truth, axis=1)
    te_combined_spread_err = np.linalg.norm(te_combined_spread_preds - te_truth, axis=1)

    tr_noml_inds = (tr_spread_max > spread_threshold) + (tr_max_mean <= threshold)
    tv_noml_inds = (tv_spread_max > spread_threshold) + (tv_max_mean <= threshold)
    te_noml_inds = (te_spread_max > spread_threshold) + (te_max_mean <= threshold)
    tr_combined_both_preds = np.zeros_like(tr_splot_preds)
    tv_combined_both_preds = np.zeros_like(tv_splot_preds)
    te_combined_both_preds = np.zeros_like(te_splot_preds)
    tr_combined_both_preds[~tr_noml_inds] = tr_preds[~tr_noml_inds, :2]
    tv_combined_both_preds[~tv_noml_inds] = tv_preds[~tv_noml_inds, :2]
    te_combined_both_preds[~te_noml_inds] = te_preds[~te_noml_inds, :2]
    tr_combined_both_preds[tr_noml_inds] = tr_splot_preds[tr_noml_inds, :2]
    tv_combined_both_preds[tv_noml_inds] = tv_splot_preds[tv_noml_inds, :2]
    te_combined_both_preds[te_noml_inds] = te_splot_preds[te_noml_inds, :2]
    tr_combined_both_err = np.linalg.norm(tr_combined_both_preds - tr_truth, axis=1)
    tv_combined_both_err = np.linalg.norm(tv_combined_both_preds - tv_truth, axis=1)
    te_combined_both_err = np.linalg.norm(te_combined_both_preds - te_truth, axis=1)

    if run_splot:
        print('SPLOT Med Err', np.median(tr_splot_err), np.median(tv_splot_err), np.median(te_splot_err))
        print('SPLOT mean Err', np.mean(tr_splot_err), np.mean(tv_splot_err), np.mean(te_splot_err))
        print('Splot med Err < thresh',np.median( tr_splot_err[tr_max_mean < threshold]), np.median(tv_splot_err[tv_max_mean < threshold]), np.median(te_splot_err[te_max_mean < threshold]))
        print('Splot Err < thresh', tr_splot_err[tr_max_mean < threshold].mean(), tv_splot_err[tv_max_mean < threshold].mean(), te_splot_err[te_max_mean < threshold].mean())
    print('CUTL Med Err', np.median(tr_err), np.median(tv_err), np.median(te_err))
    print('CUTL Mean Err', np.mean(tr_err), np.mean(tv_err), np.mean(te_err))
    print('CUTL Med Err < thresh',np.median( tr_err[tr_max_mean < threshold]), np.median(tv_err[tv_max_mean < threshold]), np.median(te_err[te_max_mean < threshold]))
    print('CUTL Mean Err < thresh',np.mean( tr_err[tr_max_mean < threshold]), np.mean(tv_err[tv_max_mean < threshold]), np.mean(te_err[te_max_mean < threshold]))
    print('Spearman corr: Tr, Tv, Te', spearmanr(tr_max_mean, tr_err)[0], spearmanr(tv_max_mean, tv_err)[0], spearmanr(te_max_mean, te_err)[0])

    print('comb Med Err', np.median(tr_combined_err), np.median(tv_combined_err), np.median(te_combined_err))
    print('comb Mean Err', np.mean(tr_combined_err), np.mean(tv_combined_err), np.mean(te_combined_err))

    print('SPREAD\n')
    if run_splot:
        print('Splot Spread > thresh',np.median( tr_splot_err[tr_spread_max > spread_threshold]), np.median(tv_splot_err[tv_spread_max > spread_threshold]), np.median(te_splot_err[te_spread_max > spread_threshold]))
        print('Splot Mean Spread > thresh', tr_splot_err[tr_spread_max > spread_threshold].mean(), tv_splot_err[tv_spread_max > spread_threshold].mean(), te_splot_err[te_spread_max > spread_threshold].mean())
    print('CUTL Med Spread > thresh',np.median( tr_err[tr_spread_max > spread_threshold]), np.median(tv_err[tv_spread_max > spread_threshold]), np.median(te_err[te_spread_max > spread_threshold]))
    print('CUTL Mean Spread > thresh',np.mean( tr_err[tr_spread_max > spread_threshold]), np.mean(tv_err[tv_spread_max > spread_threshold]), np.mean(te_err[te_spread_max > spread_threshold]))
    print('comb Med Spread', np.median(tr_combined_spread_err), np.median(tv_combined_spread_err), np.median(te_combined_spread_err))
    print('comb Mean Spread', np.mean(tr_combined_spread_err), np.mean(tv_combined_spread_err), np.mean(te_combined_spread_err))

    print('Filtered both:', tr_noml_inds.sum()/len(tr_noml_inds), tv_noml_inds.sum()/len(tv_noml_inds), te_noml_inds.sum()/len(te_noml_inds))
    print('comb Med both', np.median(tr_combined_both_err), np.median(tv_combined_both_err), np.median(te_combined_both_err))
    print('comb Mean both', np.mean(tr_combined_both_err), np.mean(tv_combined_both_err), np.mean(te_combined_both_err))

    #fig, ax = plt.subplots()
    #ax.scatter(tr_max_mean, tr_err, label='Training')
    #ax.scatter(tv_max_mean, tv_err, label='Validation')
    #ax.scatter(te_max_mean, te_err, label='Test')
    #ax.legend()
    #plot_filename = os.path.join('plots/confidence_scatters', filename[7:-3] + '_all.pdf')
    ##fig.savefig(plot_filename)
    #plt.show()
    #fig, ax = plt.subplots()
    #ax.scatter(tv_max_mean, tv_err, label='Validation')
    #ax.scatter(te_max_mean, te_err, label='Test')
    #ax.legend()
    #plot_filename = os.path.join('plots/confidence_scatters', filename[7:-3] + '_val_test.pdf')
    ##fig.savefig(plot_filename)
    #plt.cla()

    plot_filename = os.path.join('plots/confidence_scatters', filename[7:-3] + '_preds.pdf')
    #plot_mixed_images(10)

    #thresh = get_separation(percentile=0.05, split=rldataset.params.data_split, use_mean=True, use_tv_for_thresh=True, plot=False, savefig=False, filename=plot_filename)[0]
    #fig, axs = plt.subplots(2,1, sharex=True, sharey=True)
    #axs[0].scatter(tv_max_mean, tv_err, label='Validation')
    #axs[0].legend()
    #axs[0].set_ylabel('Error [m]')
    #axs[0].set_xlabel('Max Confidence')
    #axs[0].plot([thresh, thresh], [0, 1700])
    #axs[1].scatter(te_max_mean, te_err, label='Test', c='tab:orange')
    #axs[1].set_ylabel('Error [m]')
    #axs[1].set_xlabel('Max Confidence')
    #axs[1].plot([thresh, thresh], [0, 1700], c='tab:orange')
    #axs[1].legend()
    #plt.show()

    def get_uncovered_areas(data=train_data):
        coverage = np.zeros((rldataset.img_width(), rldataset.img_height()))
        tx_vec = data.ordered_dataloader.dataset.tensors[1].cpu().int().numpy()[:,0,1:3]
        num_pixels = int(np.ceil(tv_err.mean() / meter_scale))
        for vec in tx_vec:
            coverage[vec[1] - num_pixels:vec[1]+num_pixels+1,vec[0] - num_pixels:vec[0]+num_pixels+1] = 1
        return coverage

    def find_largest_squares(coverage: np.ndarray, threshold):
        coverage = coverage.copy()
        sizes = []
        for i in range(coverage.shape[0]):
            for j in range(coverage.shape[1]):
                size = 1
                while (np.count_nonzero(coverage[i:i+size,j:j+size]) == 0) and (i+size)<coverage.shape[0] and (j+size)<coverage.shape[1]:
                    size += 1
                if size >= threshold:
                    coverage[i:i+size, j:j+size] = 1
                    sizes.append(size)
        return np.array(sizes), coverage
    
    def plot_spread_err():
        fig, axs = plt.subplots(3,1)
        axs[0].scatter(te_spread, te_err)
        axs[1].scatter(tr_spread_max, tr_err)
        axs[1].scatter(tv_spread_max, tv_err)
        axs[1].scatter(te_spread_max, te_err)
        axs[2].scatter(tr_spread_sum, tr_err)
        axs[2].scatter(tv_spread_sum, tv_err)
        axs[2].scatter(te_spread_sum, te_err)
        plt.show()

    dist = np.zeros((rldataset.img_height(),rldataset.img_width()))
    for i in range(rldataset.img_height()):
        for j in range(rldataset.img_width()):
            coord = np.array([i,j]) * rldataset.params.meter_scale
            dist[i,j] = min(np.linalg.norm(coord - tr_truth, axis=1))
    print(np.median(dist[1:-1,1:-1]), dist[1:-1,1:-1].mean())
    coverage = get_uncovered_areas()
    sizes, cov = find_largest_squares(coverage[2:-2,2:-2], tv_err.mean() / meter_scale)
    #print(np.quantile(sizes, [0.5,0.9,0.95,0.99,1]) * meter_scale / 2)
    if force_april_test:
        param_set[2] = 'july_selftest_april_limited' if param_set[2][:4] == 'july' else 'nov_selftest_april_limited'
    all_results[tuple(param_set)] = [(train_images, tv_images, test_images), (tr_truth, tv_truth, te_truth), (tr_preds, tv_preds, te_preds), (tr_splot_preds, tv_splot_preds, te_splot_preds), (tr_err, tv_err, te_err)]

    def plot_rss_scatter(figsize=(6,3)):
        tr_dat = train_data.ordered_dataloader.dataset.tensors[0]
        db_rss = [(sample[:,0] * (splot.max_rss - splot.min_rss) + splot.min_rss)[sample[:,0] > 0] for sample in tr_dat]
        rx_online_pos = [sample[sample[:,0] > 0][:,1:3] * splot.meter_scale for sample in tr_dat]
        dist = []
        for pos, truth in zip(rx_online_pos, tr_truth):
            dist.append( torch.linalg.norm( pos-torch.Tensor(truth).cuda(), axis=1))
        rss = torch.cat(db_rss).cpu()
        dis = torch.cat(dist).cpu()
        fig = plt.figure(figsize=figsize)
        plt.scatter(rss.cpu(), dis.cpu(), s=1)
        x = np.linspace(rss.max(),rss.min(),100)
        plt.plot(x, 10**((-x+splot.db_shift) / 37.6), c='black')
        plt.ylabel('Rx to Tx Distance [m]')
        plt.xlabel('RSS [dB]')
        plt.tight_layout()
        fig.savefig(f'plots/dyspan2024_plots/rss_scatter_{meter_scale}_{dataset_index}.png',dpi=300, bbox_inches='tight')
        fig.savefig(f'plots/dyspan2024_plots/rss_scatter_{meter_scale}_{dataset_index}.pdf', bbox_inches='tight')
    #plot_rss_scatter()
    #embed()


if __name__ == '__main__':
    main()
