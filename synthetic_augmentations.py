import os
import numpy as np
from scipy.interpolate import LinearNDInterpolator, NearestNDInterpolator, RBFInterpolator
from scipy.optimize import curve_fit
import torch

from dataset import RSSLocDataset
from CELF_POWDERdata import *
from tirem_helper import load_tirem_alt_params, load_tirem_prop_map


class CELFDict(dict):
    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, key, value):
        self[key] = value


class SyntheticAugmentor():
    def __init__(self, method: str, rldataset: RSSLocDataset, resolution_factor: int = 1) -> None:
        assert method in ['linear', 'nearest', 'rbf', 'tirem', 'tirem_nn', 'celf']
        self.method = method
        self.rldataset = rldataset
        self.resolution_factor = resolution_factor

    def train(self, method=None):
        method = self.method if method is None else method
        # Keep a dictionary of separate augmentors for seperate sensors, though some models (celf, tirem, tirem_nn) share them between sensor types
        self.augmentors = {}

        ### Generate TIREM features and save to file for portability
        ##all_data_key = 'campus' if self.rldataset.params.dataset_index in [6,8] else None
        ##self.rldataset.data[all_data_key].make_tensors()
        #features = {}
        #dbs = {}
        #coords = {}
        #for key in ['0.2testsize_train', '0.2testsize_test']:
        #    if not self.rldataset.data[key].made_tensors:
        #        self.rldataset.data[key].make_tensors()
        #    data = self.get_rx_data_by_tx_location(key, combine_sensors=True, required_limit=1)
        #    for i, name in enumerate(data):
        #        if name not in coords:
        #            coords[name] = []
        #            dbs[name] = []
        #            features[name] = []
        #        few_coords, rss, sensor_loc, keys, sensor_type = data[name]
        #        input_features, tirem_tensor, tirem_db = self.get_tirem_features(name, few_coords, keys, False)
        #        features[name].append(input_features.cpu().numpy())
        #        dbs[name].append(tirem_db)
        #        coords[name].append(few_coords)
        #data = self.get_rx_data_by_tx_location(self.rldataset.train_key, combine_sensors=True, required_limit=0)
        #for i, name in enumerate(data):
        #    input_features = np.concatenate(features[name])
        #    tirem_db = np.concatenate(dbs[name])
        #    few_coords = np.concatenate(coords[name])
        #    np.savez(f'tirem_features_DS{self.rldataset.params.dataset_index}_{name}.npz', features=input_features, rss_db=tirem_db, coords=few_coords)
        self.train_data = self.get_rx_data_by_tx_location(self.rldataset.train_key, combine_sensors=True, required_limit=100)
        self.test_datasets = [self.get_rx_data_by_tx_location(test_key, combine_sensors=True, required_limit=0) for test_key in ([self.rldataset.train_key] + self.rldataset.test_keys)]
        if method == 'celf':
            self.augmentors = self.train_CELF()
        elif 'tirem_nn' in method:
            self.augmentors = self.train_combined_tirem_correction_model(
                train_data=self.train_data,
                val_data=self.test_datasets[2],
                test_data=self.test_datasets[1],
            )
        elif method in ['nearest', 'linear', 'rbf']:
            self.augmentors = self.train_interpolator(method)
        elif method == 'tirem':
            self.augmentors = self.fit_tirem()
    

    def get_results(self, method=None):
        method = self.method if method is None else method
        grid = np.array(np.meshgrid(np.arange(0,self.rldataset.img_width()-1,1/self.resolution_factor), np.arange(0,self.rldataset.img_height()-1,1/self.resolution_factor))).reshape(2,-1).T
        self.grid = grid
        prop_maps = np.zeros((len(self.train_data), (self.rldataset.img_height())*self.resolution_factor, (self.rldataset.img_width())*self.resolution_factor))
        all_keys = []
        all_rx_locs = []
        all_rx_types = []
        all_rx_test_preds = []
        all_rx_test_rss = []
        all_tx_test_coords = []
        for i, name in enumerate(self.train_data):
            few_coords, rss, sensor_loc, keys, sensor_type = self.train_data[name]
            if method in ['nearest', 'linear', 'rbf']:
                preds = self.augmentors[name](grid+0.5)
                test_preds = [self.augmentors[name](test_dataset[name][0]) for test_dataset in self.test_datasets]
            elif method == 'tirem':
                preds = self.augmentors[name](name, grid)
                test_preds = [self.augmentors[name](name, test_dataset[name][0]) for test_dataset in self.test_datasets]
            elif 'tirem_nn' in method:
                preds = self.augmentors[sensor_type](name, keys, grid, save_files=True)
                test_preds = [self.augmentors[sensor_type](name, keys, test_dataset[name][0]) for test_dataset in self.test_datasets]
            elif method == 'celf':
                preds = self.pred_CELF(sensor_loc, grid, keys[0], sensor_type)
                test_preds = [self.pred_CELF(sensor_loc, test_dataset[name][0], keys[0], sensor_type) for test_dataset in self.test_datasets]
            prop_maps[i, (grid[:,1]*self.resolution_factor).astype(int), (grid[:,0]*self.resolution_factor).astype(int)] = preds
            all_keys.append(keys)
            all_rx_locs.append(sensor_loc)
            all_rx_types.append(sensor_type)
            all_rx_test_preds.append(test_preds)
            all_rx_test_rss.append([test_dataset[name][1] for test_dataset in self.test_datasets])
            all_tx_test_coords.append([test_dataset[name][0] for test_dataset in self.test_datasets])

        return prop_maps, all_keys, all_rx_locs, all_rx_types, all_rx_test_preds, all_rx_test_rss, all_tx_test_coords


    def train_interpolator(self, method):
        augmentors = {}
        assert method in ['nearest', 'linear', 'rbf']
        for i, name in enumerate(self.train_data):
            if self.params.dataset_index in [6,8]:
                bound = 600//self.params.meter_scale if 'dd' in name else 1200//self.params.meter_scale
            else:
                bound = 3300//self.params.meter_scale
            few_coords, rss, sensor_loc, keys, sensor_type = self.train_data[name]
            dist = np.linalg.norm(few_coords - sensor_loc, axis=1)
            pl_model_d_to_r, pl_model_r_to_d = self.get_pathloss_func(dist[dist<bound], rss[dist<bound])
            corners = [
                [0,0],
                [0, self.img_height()],
                [self.img_width(),0],
                [self.img_width(), self.img_height()],
                [0, self.img_height()/2],
                [self.img_width()/2,0],
                [self.img_width()/2, self.img_height()],
                [self.img_width(), self.img_height()/2],
                ]
            few_coords = np.vstack((few_coords, corners))

            corner_rss = pl_model_d_to_r(np.linalg.norm(np.array(corners) - sensor_loc, axis=1))
            corner_rss = np.maximum(corner_rss, 0.01)
            rss = np.concatenate((rss, corner_rss))
            if method == 'nearest':
                interpolator = NearestNDInterpolator(few_coords, rss)
            elif method == 'linear':
                interpolator = LinearNDInterpolator(few_coords, rss, fill_value=0)
            elif method == 'rbf':
                interpolator = RBFInterpolator(few_coords, rss, smoothing=1, kernel='linear')
            augmentors[name] = interpolator
        return augmentors



    def train_CELF(self, method='ellipse'):
        celf = {}
        rldataset = self.rldataset
        train_inputs = rldataset.data[rldataset.train_key].ordered_dataloader.dataset.tensors[0].cpu().numpy()
        train_tx = rldataset.data[rldataset.train_key].ordered_dataloader.dataset.tensors[1].cpu().numpy()
        # Remove sensors from train data for experiments on adding new devices at inference time
        if hasattr(rldataset, 'sensors_to_remove') and rldataset.sensors_to_remove:
            for sensor in rldataset.sensors_to_remove:
                train_inputs[:,sensor] = 0
        if not hasattr(rldataset, 'delta_p'):
            rldataset.delta_p = rldataset.params.meter_scale
        if rldataset.params.dataset_index == 6: # Utah FRS
            labels = [2,3,4]
        elif rldataset.params.dataset_index == 7: # Antwerp LoRA
            labels = [4]
        elif rldataset.params.dataset_index == 8: # Utah CBRS
            labels = [2]
        for label in labels:
            #rx_type = [1 if 'bus-' in name else 2 if 'cnode' in name else 3 if 'cbrs' in name else 4 for name in [rx_name]][0]
            inds = train_inputs[:,:,4] == label
            if inds.sum() == 0:
                continue
            RssTrain = (train_inputs[:,:,0] * (rldataset.all_max_rss - rldataset.all_min_rss) + rldataset.all_min_rss)[inds]
            RxCoords = train_inputs[inds][:,1:3]*rldataset.params.meter_scale
            TxCoords = np.repeat(train_tx[:,0,1:3],inds.sum(axis=1), axis=0)*rldataset.params.meter_scale
            DisTrain = np.linalg.norm(RxCoords - TxCoords, axis=1)

            fadTrain = calcPathLossRecLink(RssTrain, DisTrain, d0=1, debug=False)
            FadLossTrain, FadmeanTrain, SigmadB2Train, n_p, refLossLink = fadTrain
            ## rooftop
            if label== 3: #'cbrssdr'
                excessPathLen = 105 #best
                delta_opt, ratioShad_opt = 35, 0.58
                Regularizer = 0.3
            ## nuc
            if label== 4: #'nuc'
                excessPathLen = 150#125 # utm
                delta_opt, ratioShad_opt = 15, 0.4
                Regularizer = 0.6 #1.5
            ## bus
            if label== 1: #'bus'
                excessPathLen = 145 # utm 
                delta_opt, ratioShad_opt = 13, 0.37
                Regularizer = 0.45 #1.5 #
            ## DD
            if label== 2: #'dd'
                excessPathLen = 185 # utm
                delta_opt, ratioShad_opt = 13, 0.46
                Regularizer = 0.3
            pixelCoords, DistPixels, xVals, yVals = calcGridPixelCoordsNoCHM(
                (0,0),
                ((rldataset.img_width()-1)*rldataset.params.meter_scale, (rldataset.img_height()-1)*rldataset.params.meter_scale),
                rldataset.delta_p,
                debug=0
            )
            LossField, FadLossTrainEst, metric_arr = CELF(
                FadLossTrain,
                delta_opt,
                ratioShad_opt,
                SigmadB2Train,
                excessPathLen,
                Regularizer,
                debug=False,
                xVals=xVals,
                yVals=yVals,
                DistPixels=DistPixels,
                pixelCoords=pixelCoords,
                TXCoords=TxCoords,
                RXCoords=RxCoords,
                delta_p=rldataset.delta_p,
                model=method,
                k_fold=1
            )
            celf[label] = CELFDict()
            celf[label].pixelCoords = pixelCoords
            celf[label].DistPixels = DistPixels
            celf[label].xVals = xVals
            celf[label].yVals = yVals
            celf[label].LossField = LossField
            celf[label].excessPathLen = excessPathLen
            celf[label].method = method 
            celf[label].n_p = n_p
            celf[label].refLossLink = refLossLink
        return celf
    
    def pred_CELF(self, rx_locs, tx_locs, key, label):
        rldataset = self.rldataset
        min_rss, max_rss = rldataset.get_min_max_rss_from_key(key)
        rx_locs = rx_locs*rldataset.params.meter_scale
        tx_locs = tx_locs*rldataset.params.meter_scale
        if len(tx_locs) != len(rx_locs):
            if len(tx_locs) < len(rx_locs):
                tx_locs = np.broadcast_to(tx_locs, rx_locs.shape)
            else:
                rx_locs = np.broadcast_to(rx_locs, tx_locs.shape)
        distance = np.linalg.norm(rx_locs - tx_locs, axis=1)
        celf = self.augmentors[label]

        FadLossTestEst = predShadLossLinkFast(
            LossField=celf.LossField,
            excessPathLen=celf.excessPathLen,
            xVals=celf.xVals,
            yVals=celf.yVals,
            distPixels=celf.DistPixels,
            pixelCoords=celf.pixelCoords,
            TXCoords=tx_locs,
            RXCoords=rx_locs,
            delta_p=rldataset.delta_p,
            model=celf.method,
            debug=0
        )
        rss_estimate = celf.n_p * (-10.0 * np.log10(distance)) + celf.refLossLink - FadLossTestEst
        norm_rss = (rss_estimate - min_rss) / (max_rss - min_rss)
        return norm_rss.clip(0,1)

    
    def eval_CELF(self, test_keys=None):
        rldataset = self.rldataset
        results = []
        if test_keys is None:
            test_keys = rldataset.test_keys
        for test_key in test_keys:
            results.append({})
            test_inputs = rldataset.data[test_key].ordered_dataloader.dataset.tensors[0].cpu().numpy()
            test_tx = rldataset.data[test_key].ordered_dataloader.dataset.tensors[1].cpu().numpy()
            for label in [2,3,4]:
                celf = self.augmentors[label]
                #rx_type = [1 if 'bus-' in name else 2 if 'cnode' in name else 3 if 'cbrs' in name else 4 for name in [rx_name]][0]
                inds = test_inputs[:,:,4] == label
                if inds.sum() == 0:
                    continue
                if rldataset.params.dataset_index == 6:
                    rssTest = (test_inputs[:,:,0] * (rldataset.all_max_rss - rldataset.all_min_rss) + rldataset.all_min_rss)[inds]
                    RxCoords = test_inputs[inds][:,1:3]*rldataset.params.meter_scale
                else:
                    rssTest = (test_inputs[:,:,0] * (rldataset.all_max_rss - rldataset.all_min_rss) + rldataset.all_min_rss)
                    RxCoords = test_inputs[:,1:3]*rldataset.params.meter_scale
                TxCoords = np.repeat(test_tx[:,0,1:3],inds.sum(axis=1), axis=0)*rldataset.params.meter_scale
                DisTest = np.linalg.norm(RxCoords - TxCoords, axis=1)

                FadLossTestEst = predShadLossLinkFast(
                    LossField=celf.LossField,
                    excessPathLen=celf.excessPathLen,
                    xVals=celf.xVals,
                    yVals=celf.yVals,
                    distPixels=celf.DistPixels,
                    pixelCoords=celf.pixelCoords,
                    TXCoords=TxCoords,
                    RXCoords=RxCoords,
                    delta_p=rldataset.delta_p,
                    model=celf.method,
                    debug=0
                )
                rssEstimate = celf.n_p * (-10.0 * np.log10(DisTest)) + celf.refLossLink - FadLossTestEst
                results[-1][label] = (rssTest, rssEstimate, DisTest)
        return results


    def train_combined_tirem_correction_model(self, train_data, val_data=None, test_data=None, num_epochs=40000, model: torch.nn.Module=None, optimizer=None, use_coords_and_rbf=False, method=None):
        method = self.method if method is None else method
        from models import TiremMLP
        rldataset = self.rldataset
        rldataset.make_elevation_tensors(meter_scale=5)
        use_coords_and_rbf = 'rbf' in method
        tirem_correction_models = {}
        features, tirem_preds, labels, eval_errors = {}, {}, {}, {}
        for d in features, tirem_preds, labels, eval_errors:
            for key, option in zip(['train', 'val', 'test'], [train_data, val_data, test_data]):
                if option is not None:
                    if rldataset.params.dataset_index == 6:
                        d[key] = {a:[] for a in range(2,5)}
                    elif rldataset.params.dataset_index == 7:
                        d[key] = {4:[]}
                    else:
                        d[key] = {2:[]}
        for i, name in enumerate(train_data):
            for dataset, set_name in zip([train_data, val_data, test_data], ['train', 'val', 'test']):
                if dataset is None or name not in dataset:
                    continue
                few_coords, rss, sensor_loc, keys, sensor_type = dataset[name]
                # Remove sensors from train data for experiments on adding new devices at inference time
                if set_name == 'train' and hasattr(rldataset, 'sensors_to_remove') and keys[0] in rldataset.sensors_to_remove:
                    continue
                sensor_type = int(sensor_type)
                input_features, tirem_tensor, tirem_db = self.get_tirem_features(name, few_coords, keys, use_coords_and_rbf)
                rss_tensor = torch.tensor(rss, dtype=torch.float32, device=rldataset.params.device) 
                features[set_name][sensor_type].append(input_features)
                tirem_preds[set_name][sensor_type].append(tirem_tensor)
                labels[set_name][sensor_type].append(rss_tensor)
        for set_name in features:
            for sensor_type in features[set_name]:
                features[set_name][sensor_type] = torch.concatenate(features[set_name][sensor_type])
                tirem_preds[set_name][sensor_type] = torch.concatenate(tirem_preds[set_name][sensor_type])
                labels[set_name][sensor_type] = torch.concatenate(labels[set_name][sensor_type])
        for sensor_type in features['train']:
            model = TiremMLP([features[set_name][sensor_type].shape[-1],200,100]) if model is None else model
            model_file = f'models/tirem_nn/{rldataset.params}_{sensor_type}{"" if (not hasattr(rldataset, "sensors_to_remove") or len(rldataset.sensors_to_remove) == 0) else "muted_sensors"}.pt'
            backup_file = model_file.replace('unet__', 'unet2__')
            if use_coords_and_rbf:
                backup_file = model_file.replace('tirem_nn__', 'tirem_nn_rbf_')
            if os.path.exists(model_file) or os.path.exists(backup_file):
                try:
                    ckpt = torch.load(model_file)
                except:
                    ckpt = torch.load(backup_file)
                model.load_state_dict(ckpt)
                print('Loaded TIREM correction model for', sensor_type )
            else:
                mse_loss = torch.nn.MSELoss()
                optimizer = torch.optim.Adam(model.parameters(), lr=0.001, weight_decay=1e-6) if optimizer is None else optimizer
                tv_err = 1e9
                for n in range(num_epochs):
                    y_pred = model(features['train'][sensor_type], tirem_preds['train'][sensor_type])
                    loss = mse_loss(labels['train'][sensor_type], y_pred.squeeze())
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()
                    #print('Epoch', n)
                    if n % 100 == 0:
                        for key in eval_errors:
                            preds = model(features[key][sensor_type], tirem_preds[key][sensor_type])
                            errors = labels[key][sensor_type] - preds.squeeze()
                            mean_err = abs(errors).mean().item()
                            eval_errors[key][sensor_type].append(mean_err)
                            if key == 'val' and mean_err < tv_err:
                                tv_err = mean_err
                                best_model = copy.deepcopy(model)
                            print(f"{n}/{num_epochs}", end='\r')

                print('Generated TIREM correction model for', sensor_type )
                torch.save(model.state_dict(), model_file)
                model = best_model

            def eval_tirem_nn(name, keys, eval_coords, save_files=False):
                input_features, input_db, tirem_db = self.get_tirem_features(name, eval_coords, keys)
                #if save_files:
                #    all_info = np.load(f'tirem_features_DS{self.rldataset.params.dataset_index}_{name}.npz')
                #    new_input_features = np.concatenate((input_features.cpu().numpy(), all_info['features']))
                #    new_rss_db = np.concatenate((tirem_db, all_info['rss_db']))
                #    new_coords = np.concatenate((eval_coords, all_info['coords']))
                #    np.savez(f'tirem_all_features_DS{self.rldataset.params.dataset_index}_{name}.npz', features=new_input_features, rss_db=new_rss_db, coords=new_coords)
                preds = model(input_features, input_db).cpu().detach().squeeze()
                return preds
            tirem_correction_models[sensor_type] = eval_tirem_nn

        return tirem_correction_models
    

    def get_rx_data_by_tx_location(self, source_key=None, combine_sensors=True, use_db_for_combination=True, required_limit=100):
        rldataset = self.rldataset
        if source_key is None and hasattr(rldataset, 'train_key'):
            source_key = rldataset.train_key
        source_data = rldataset.data[source_key]
        data = {}
        train_x, train_y = source_data.ordered_dataloader.dataset.tensors[:2]
        train_x, train_y = train_x.cpu().numpy(), train_y.cpu().numpy()
        coords = train_y[:,0,1:]
        for key in rldataset.location_index_dict:
            name = rldataset.location_index_dict[key]
            if isinstance(name, str):
                if 'bus' in name: continue
                if combine_sensors:
                    if 'nuc' in name and 'b210' in name:
                        name = name[:-6]
                    elif 'cellsdr' in name:
                        name = name.replace('cell','cbrs')
            rss = train_x[:,key,0]
            valid_inds = rss != 0
            if valid_inds.sum() < required_limit or valid_inds.sum() == 0: continue
            sensor_loc = train_x[valid_inds][0,key,1:3]
            sensor_type = train_x[valid_inds][0,key,4]
            rss = rss[valid_inds]
            if combine_sensors and use_db_for_combination:
                min_rss, max_rss = rldataset.get_min_max_rss_from_key(key)
                rss = rss * (max_rss - min_rss) + min_rss
            few_coords = coords[valid_inds]
            if name not in data:
                data[name] = [few_coords, rss, sensor_loc, [key], sensor_type]
            else:
                data[name][3].append(key)
                data[name] = [
                    np.concatenate((data[name][0], few_coords)),
                    np.concatenate((data[name][1], rss)),
                    sensor_loc,
                    data[name][3],
                    sensor_type
                    ]
        for name in data:
            few_coords, rss, sensor_loc, keys, sensor_type = data[name]
            sorted_inds = np.lexsort(few_coords.T)
            few_coords = few_coords[sorted_inds]
            rss = rss[sorted_inds]
            if combine_sensors and use_db_for_combination:
                min_rss = min(rldataset.get_min_max_rss_from_key(key)[0] for key in keys)
                max_rss = min(rldataset.get_min_max_rss_from_key(key)[1] for key in keys)
                rss = (rss - min_rss) / (max_rss - min_rss)
            row_mask = np.append([True], np.any(np.diff(few_coords,axis=0),1))
            data[name][0] = few_coords[row_mask]
            data[name][1] = rss[row_mask]
        return data


    def get_tirem_db(self, name, few_coords):
        few_coords = few_coords.astype(np.float32)
        if os.path.exists(f'tirem_features/tirem_all_features_DS{self.rldataset.params.dataset_index}_{name}.npz'):
            features_dict = np.load(f'tirem_features/tirem_all_features_DS{self.rldataset.params.dataset_index}_{name}.npz')
            rss_db = features_dict['rss_db']
            library_coords = features_dict['coords'].astype(np.float32)
            db_dict = {coord.tobytes():rss_db[i] for i, coord in enumerate(library_coords)}
            tirem_db = np.array([db_dict[coord.tobytes()] for coord in few_coords])
        else:
            tirem_prop_map = load_tirem_prop_map(self.rldataset.params.dataset_index, name, tuple(self.rldataset.corners.flatten()))
            tirem_coords = np.rint(few_coords*self.rldataset.params.meter_scale).astype(int)
            shape = tirem_prop_map.shape
            tirem_db = tirem_prop_map[np.minimum(tirem_coords[:,1], shape[0]-1), np.minimum(tirem_coords[:,0], shape[1]-1)]
        return tirem_db


    def get_tirem_features(self, name, few_coords, keys, use_coords_and_rbf=False):
        few_coords = few_coords.astype(np.float32)
        tirem_db = self.get_tirem_db(name, few_coords)
        min_rss, max_rss = self.rldataset.get_min_max_rss_from_key(keys[0])
        tirem_normalized = (tirem_db - min_rss) / (max_rss - min_rss)
        if os.path.exists(f'tirem_features/tirem_all_features_DS{self.rldataset.params.dataset_index}_{name}.npz'):
            features_dict = np.load(f'tirem_features/tirem_all_features_DS{self.rldataset.params.dataset_index}_{name}.npz')
            rss_db = features_dict['rss_db']
            features = features_dict['features']
            library_coords = features_dict['coords'].astype(np.float32)
            features_dict = {coord.tobytes():features[i] for i, coord in enumerate(library_coords)}
            tirem_features = np.array([features_dict[coord.tobytes()] for coord in few_coords])
        else:
            tirem_params = load_tirem_alt_params(self.rldataset.params.dataset_index, name, tuple(self.rldataset.corners.flatten()))
            tirem_coords = np.rint(few_coords*self.rldataset.params.meter_scale).astype(int)
            shape = tirem_params.shape
            tirem_features = tirem_params[np.minimum(tirem_coords[:,1], shape[0]-1), np.minimum(tirem_coords[:,0], shape[1]-1)]
            tirem_features = np.concatenate((tirem_features, tirem_normalized[:,None]), axis=1)
        if use_coords_and_rbf:
            if not hasattr(self, "interpolators") or not isinstance(self.interpolators[name], RBFInterpolator):
                self.interpolators = self.train_interpolator('rbf')
            preds = self.interpolators[name](few_coords)
            tirem_features = np.concatenate((tirem_features, few_coords / np.array([self.rldataset.img_width(), self.rldataset.img_height()]), preds[:,None]), axis=1)
        input_features = torch.tensor(tirem_features, dtype=torch.float32, device=self.rldataset.params.device)
        tirem_tensor = torch.tensor(tirem_normalized, dtype=torch.float32, device=self.rldataset.params.device) 
        return input_features, tirem_tensor, tirem_db
    

    def fit_tirem(self):
        augmentors = {}
        rldataset = self.rldataset
        rldataset.make_elevation_tensors(meter_scale=5)
        for i, name in enumerate(self.train_data):
            if self.params.dataset_index in [6,8]:
                bound = 600//self.params.meter_scale if 'dd' in name else 1200//self.params.meter_scale
            else:
                bound = 3300//self.params.meter_scale
            few_coords, rss, sensor_loc, keys, sensor_type = self.train_data[name]
            dist = np.linalg.norm(few_coords - sensor_loc, axis=1)

            tirem_db = self.get_tirem_db(name, few_coords)
            min_rss, max_rss = self.get_min_max_rss_from_key(keys[0])
            rss_db = rss * (max_rss - min_rss) + min_rss
            calibration_param = rss_db[dist<bound].mean()  - tirem_db[dist<bound].mean()
            def eval_tirem(name, eval_coords):
                eval_db = self.get_tirem_db(name, eval_coords)
                eval_db_scaled = eval_db + calibration_param
                eval_db_bounded = np.maximum(eval_db_scaled, min_rss)
                preds = (eval_db_bounded - min_rss) / (max_rss - min_rss)
                return preds
            augmentors[name] = eval_tirem
        return augmentors


    def rss_evaluation(self, methods=None, resolution_factor=1, save_evaluation=True, filename=None):
        if methods is None:
            methods = ['linear', 'nearest', 'rbf', 'celf', 'tirem', 'tirem_nn']
        if not os.path.exists('rss_prediction/'):
            os.mkdir('rss_prediction')
        if not os.path.exists('rss_prediction/synth_evaluation'):
            os.mkdir('rss_prediction/synth_evaluation')
        if save_evaluation:
            evaluation_file = f'synth_evaluation/{self.params}_{"_".join([method for method in methods])}_rfactor{resolution_factor}.npz'
            results = {
                'train_rss':[], 'val_rss':[], 'test_rss':[],
                'train_pred':[], 'val_pred':[], 'test_pred':[],
                'train_coords':[], 'val_coords':[], 'test_coords':[],
                }
        for method in methods:
            prop_results = self.get_results()
            prop_maps, keys, rx_locs, rx_types, rx_test_preds, rx_test_rss, tx_test_coords = prop_results
            print('Loaded', method)
            if save_evaluation:
                for i in range(len(rx_test_rss)):
                    min_rss, max_rss = self.get_min_max_rss_from_key(keys[i][0])
                    results['train_rss'].append(rx_test_rss[i][0] * (max_rss - min_rss) + min_rss )
                    results['test_rss'].append(rx_test_rss[i][1] * (max_rss - min_rss) + min_rss )
                    results['val_rss'].append(rx_test_rss[i][2] * (max_rss - min_rss) + min_rss )
                    results['train_pred'].append(rx_test_preds[i][0] * (max_rss - min_rss) + min_rss )
                    results['test_pred'].append(rx_test_preds[i][1] * (max_rss - min_rss) + min_rss )
                    results['val_pred'].append(rx_test_preds[i][2] * (max_rss - min_rss) + min_rss )
                    results['train_coords'].append(tx_test_coords[i][0])
                    results['test_coords'].append(tx_test_coords[i][1])
                    results['val_coords'].append(tx_test_coords[i][2])
        keys = np.array(keys, dtype=object)
        rx_types = np.array(rx_types)
        if save_evaluation:
            for key in results:
                results[key] = np.array(results[key], dtype=object)
            np.savez(evaluation_file, results=results, rx_locs=rx_locs)


    def get_pathloss_func(self, distance, rss):
        def fit_f(dist, a, b):
            return a * np.log10(dist) + b
        def f_generator(a,b):
            def f(dist):
                return a * np.log10(dist) + b
            return f
        def f_inv_generator(a,b):
            def f(rss):
                return 10.0 ** ( (rss-b) / a)
            return f
        
        popt, pcov = curve_fit(fit_f, distance, rss)
        return f_generator(*popt), f_inv_generator(*popt)


def run_evaluation(dataset_name='utah_frs', data_split='grid2', method='celf'):
    from locconfig import LocConfig
    meter_scale = 100 if 'antwerp' in dataset_name else 25
    # data split one of : 'random', 'grid2', 'gridN' for N=2,5,10,11,etc
    # set min_inputs=1 for antwerp_lora to add more samples for RSS prediction, not for localization
    config = LocConfig(dataset_name, data_split=data_split, meter_scale=meter_scale)
    rldataset = RSSLocDataset(config, sensors_to_remove=[])
    # you can find device index by name in rldataset.location_index_dict and add to sensors_to_remove=[25,14,...]
    rss_predictor = SyntheticAugmentor(method, rldataset) # one of 'celf', 'rbf', 'linear', 'tirem', 'tirem_nn'
    rss_predictor.train()
    prop_results = rss_predictor.get_results()
    prop_maps, keys, pred_locs, sensor_types, test_preds, test_rss, test_coords = prop_results
    results = {
        'train_rss':[], 'val_rss':[], 'test_rss':[],
        'train_pred':[], 'val_pred':[], 'test_pred':[],
        'train_coords':[], 'val_coords':[], 'test_coords':[],
        }
    for i in range(len(test_rss)):
        min_rss, max_rss = rldataset.get_min_max_rss_from_key(keys[i][0])
        results['train_rss'].append(test_rss[i][0] * (max_rss - min_rss) + min_rss )
        results['test_rss'].append(test_rss[i][1] * (max_rss - min_rss) + min_rss )
        results['val_rss'].append(test_rss[i][2] * (max_rss - min_rss) + min_rss )
        results['train_pred'].append(test_preds[i][0] * (max_rss - min_rss) + min_rss )
        results['test_pred'].append(test_preds[i][1] * (max_rss - min_rss) + min_rss )
        results['val_pred'].append(test_preds[i][2] * (max_rss - min_rss) + min_rss )
        results['train_coords'].append(test_coords[i][0])
        results['test_coords'].append(test_coords[i][1])
        results['val_coords'].append(test_coords[i][2])
    
    from IPython import embed
    embed()


if __name__ == '__main__':
    run_evaluation('antwerp_lora', method='tirem_nn')
    run_evaluation('utah_frs', method='tirem_nn') # Pass 6,8,7 or 'utah_frs', 'utah_cbrs', or 'antwerp_lora'
    run_evaluation('utah_cbrs', method='tirem_nn')