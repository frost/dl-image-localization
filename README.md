# DL-Image-Localization

Code for deep learning localization techniques.

If you're looking to run or train the model, start with `ModelRunner.py`. As is
the file will load the pretrained model and evaluate on the train and test sets, but
can be easily changed to train the model, modify inputs, or more.

Requirements:
* Numpy
* Scipy
* OpenCV
* Scikit-Learn
* Pytorch
* utm

All should be easily available with `pip install`
