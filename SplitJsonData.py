import json
import numpy as np
from dataset import TLDLParams, ImageDataset

def split_json_data(datafile='powder_462.7_rss_data.json', folder='split_data'):
    with open(datafile) as f:
        data = json.load(f)
    single_tx_data = {key:data[key] for key in data if 'tx_coords' in data[key].keys() and len(data[key]['tx_coords']) == 1 and 'stationary' not in data[key]['metadata'][0]}

    params = TLDLParams(force_num_tx=1)
    imd = ImageDataset(params)
    imd.make_datasets(make_val=False, eval_special=True)
    imd.make_datasets(split='grid10', make_val=False)
    imd.make_datasets(split='july', make_val=False)
    imd.make_datasets(split='driving', make_val=False)
    dataset_keys = {'july':'july', 'april':'april', 'driving':'driving_driving', 'non_driving':'driving_non-driving', 'campus':'campus', 'random_train':'0.2testsize_train', 'random_test':'0.2testsize_test', 'off_campus':'off_campus', 'indoor':'indoor', 'grid_train':'0.2testsizegrid10_train', 'grid_test':'0.2testsizegrid10_test', 'unused':'unused'}
    used_keys = []
    data_dict = {}
    for pretty_name in dataset_keys:
        dataset_key = dataset_keys[pretty_name]
        if dataset_key == 'unused':
            unused_keys = [key for key in single_tx_data if key not in used_keys ]
            subset_data = {key:single_tx_data[key] for key in unused_keys}
        else:
            if dataset_key == 'indoor':
                keys = [str(metadata[0]['time']) for metadata in imd.data[dataset_key].tx_metadata]
            else:
                keys = [str(metadata[0]['time']) for metadata in imd.data[dataset_key].tx_metadata if metadata[0]['transport'] != 'inside']
            used_keys += keys
            subset_data = {key:single_tx_data[key] for key in keys}
            if dataset_key == 'driving_non-driving':
                walking_keys = [str(metadata[0]['time']) for metadata in imd.data[dataset_key].tx_metadata if 'walking' == metadata[0]['transport']]
                biking_keys = [str(metadata[0]['time']) for metadata in imd.data[dataset_key].tx_metadata if 'biking' == metadata[0]['transport']]
                walking_data = {key:single_tx_data[key] for key in walking_keys}
                biking_data = {key:single_tx_data[key] for key in biking_keys}
                data_dict['walking'] = walking_data
                data_dict['biking'] = biking_data
        data_dict[pretty_name] = subset_data

    stationary_tx_data = {key:data[key] for key in data if 'tx_coords' in data[key].keys() and 'stationary' in data[key]['metadata'][0]}
    two_tx_data = {key:data[key] for key in data if 'tx_coords' in data[key].keys() and len(data[key]['tx_coords']) == 2}
    no_tx_data = {key:data[key] for key in data if 'tx_coords' not in data[key].keys()}
    for key in stationary_tx_data:
        stationary_index = stationary_tx_data[key]['metadata'][0]['stationary']
        new_key = 'stationary%i' % stationary_index
        if new_key not in data_dict:
            data_dict[new_key] = {}
        data_dict[new_key][key] = stationary_tx_data[key]

    data_dict['single_tx'] = single_tx_data
    data_dict['single_tx'] = single_tx_data
    data_dict['two_tx'] = two_tx_data
    data_dict['no_tx'] = no_tx_data

    for dataset_key in data_dict:
        filename = '%s.json' % dataset_key
        with open(filename, 'w') as f:
            json.dump(data_dict[dataset_key], f)

if __name__ == '__main__':
     split_json_data()