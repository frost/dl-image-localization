import matplotlib
import numpy as np
import os
import pickle
import argparse
from scipy.stats import gaussian_kde
from IPython import embed
from locconfig import LocConfig
from Splot import *
from dataset import RSSLocDataset
from CELF_POWDERdata import calcPathLossRecLink
import torch
import torch.nn as nn

plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

meter_scale = 30
lr = 0.001
should_train = True
should_load_model = True
restart_optimizer = True
force_april_test = True

# Specify params
num_epochs = 100000
force_num_tx = 1
include_elevation_map = True

batch_size = 128
better_epoch_limit = 100

num_training_repeats = 1

global_device = torch.device('cuda')

def gaussian_kernel(n, sigma):
    r = np.arange(-int(n/2), int(n/2)+1)
    return 1 / (sigma * np.sqrt(2*np.pi)) * np.exp(-(r)**2/(2*sigma**2))
normal_kernel = gaussian_kernel(50, 1)

class DistanceMLP(nn.Module):
    def __init__(self, input_channels, device=global_device, sensor_dropout=False, min_sensors=10) -> None:
        super().__init__()
        self.layers = nn.Sequential(
            nn.Dropout(0.1),
            nn.Linear(input_channels, 64), nn.LeakyReLU(), nn.Dropout(0.1),
            nn.Linear(64, 256), nn.LeakyReLU(), nn.Dropout(0.1),
            nn.Linear(256, 512), nn.LeakyReLU(), nn.Dropout(0.2),
            nn.Linear(512, 1024), nn.LeakyReLU(), nn.Dropout(0.3),
            nn.Linear(1024, 64), nn.LeakyReLU(), nn.Dropout(0.1),
            nn.Linear(64, input_channels)
        )
        self.refLoss = torch.nn.Parameter(torch.ones(1) * 0.5)
        self.distance_bias = torch.nn.Parameter(torch.zeros(1))
        self.n_p = torch.nn.Parameter(torch.ones(1) * 0.05)
        self.normal_kernel = torch.tensor(normal_kernel, dtype=torch.float32).unsqueeze(0).unsqueeze(0)
        self.use_sensor_dropout = sensor_dropout
        self.min_sensors = min_sensors
        #self.pl_models = pl_models
        self.to(device)
    
    def sensor_dropout(self, rx_vec):
        num_sensors = (rx_vec != 0).sum(axis=1)
        for i, num in enumerate(num_sensors):
            nonzeros = rx_vec[i].nonzero()
            if num > self.min_sensors:
                num_indices = torch.randint(self.min_sensors, num, size=(1,)).long().item()
                to_remove = torch.randperm(num)[num_indices:]
                rx_vec[i,nonzeros[to_remove]] = 0
        return rx_vec
        
    @staticmethod
    def complement_idx(idx, dim):
        """
        Compute the complement: set(range(dim)) - set(idx).
        idx is a multi-dimensional tensor, find the complement for its trailing dimension,
        all other dimension is considered batched.
        Args:
            idx: input index, shape: [N, *, K]
            dim: the max index for complement
        """
        a = torch.arange(dim, device=idx.device)
        ndim = idx.ndim
        dims = idx.shape
        n_idx = dims[-1]
        dims = dims[:-1] + (-1, )
        for i in range(1, ndim):
            a = a.unsqueeze(0)
        a = a.expand(*dims)
        masked = torch.scatter(a, -1, idx, 0)
        compl, _ = torch.sort(masked, dim=-1, descending=False)
        compl = compl.permute(-1, *tuple(range(ndim - 1)))
        compl = compl[n_idx:].permute(*(tuple(range(1, ndim)) + (0,)))
        return compl
    
    def forward(self, x, points=None):
        x = x.clone()
        #if self.use_sensor_dropout:
        #    x = self.sensor_dropout(x)
        #topk = torch.topk(x, 10, dim=1)
        #mask = self.complement_idx(topk[1], x.shape[1])
        #x.scatter_(1, mask, 0.0)
        #x[x < x.mean(dim=1, keepdim=True)] = 0
        #path_loss_distance = torch.zeros_like(x)
        #for i in range(x.shape[-1]):
        #    if i in self.pl_models:
        #        path_loss_distance[:,i] = torch.clamp(self.pl_models[i](x[:,i]), max=200)
        path_loss_distance = nn.functional.relu(10.0 ** ((x - self.refLoss) / (self.n_p * -10.0)) + self.distance_bias)
        radii = path_loss_distance + self.layers(x) #+ self.distance_bias
        return radii, path_loss_distance
        #return self.circle_preds(points, radii)

    def circle_preds(self, all_points, all_radii, resolution=200):
        def get_circle_intersections(circles):
            '''
            @summary: calculates intersection points of all circles
            @param circles: [N,3] np.array (x, y, r)
            @result: tuple of intersection points (which are (x,y) tuple)
            '''
            def pairwise_combs(ab):
                n = len(ab)
                N = n*(n-1)//2
                out = torch.empty((N,2,ab.shape[-1]),dtype=ab.dtype)
                idx = torch.tensor(np.concatenate(( [0], np.arange(n-1,0,-1).cumsum() ))).to(int)
                start, stop = idx[:-1], idx[1:]
                for j,i in enumerate(range(n-1)):
                    out[start[j]:stop[j],0] = ab[j]
                    out[start[j]:stop[j],1] = ab[j+1:]
                return out
            all_circles = pairwise_combs(circles)
            c1, c2 = all_circles[:,0], all_circles[:,1]
            r1, r2 = all_circles[:,:,2].T
            dc = (c2[:,:2] - c1[:,:2])
            d = torch.sqrt((dc**2).sum(axis=1))
            a = (r1*r1-r2*r2+d*d)/(2*d)
            h = torch.sqrt(r1*r1-a*a)
            m = c1[:,:2] + a[:,None] * dc/d[:,None]
            s1 = m + h[:,None] * torch.tensor([1,-1]) * torch.fliplr(dc) / d[:,None]
            s2 = m + h[:,None] * torch.tensor([-1,1]) * torch.fliplr(dc) / d[:,None]
            intersections =  torch.concatenate((s1,s2))
            return intersections[~torch.isnan(intersections).any(axis=1)]

        def max_density_conv(intersections, resolution=500):
            intersections = intersections[intersections.amin(axis=1) > 0]
            max_d = intersections.amax()
            scaled = intersections * (resolution - 1) / max_d
            int_ints = scaled.round().to(int)
            p1 = torch.nn.functional.conv1d(torch.bincount(int_ints[:,0]).to(torch.float32)[None,:], self.normal_kernel, padding='same').argmax() * max_d / (resolution-1)
            p2 = torch.nn.functional.conv1d(torch.bincount(int_ints[:,1]).to(torch.float32)[None,:], self.normal_kernel, padding='same').argmax() * max_d / (resolution-1)
            return torch.tensor([p1,p2])
        
        preds = torch.zeros((len(all_points),2))
        for i, (points, radii) in enumerate(zip(all_points, all_radii)):
            points = points[radii > 0]
            radii = radii[radii > 0]
            all_intersections = get_circle_intersections(torch.hstack((points, radii[:,None])))
            pred = max_density_conv(all_intersections, resolution)
            preds[i] = pred

    def train_model(self, train_dataloader, eval_dataloaders=None, num_epochs=10000, lr=0.010):
        loss_func = nn.MSELoss()
        optimizer = torch.optim.Adam(self.parameters(), lr=lr)
        tr_mse = []
        eval_mse_list = [[] for val in eval_dataloaders]
        train_x, train_locs = train_dataloader.dataset.tensors[:2]
        train_distances = torch.linalg.norm(train_x[:,:,1:3] - train_locs[:,:,1:], axis=2)
        eval_x_list = [dataloader.dataset.tensors[0] for dataloader in eval_dataloaders]
        eval_loc_list = [dataloader.dataset.tensors[1] for dataloader in eval_dataloaders]
        eval_distances_list = [torch.linalg.norm(x[:,:,1:3] - locs[:,:,1:], axis=2) for x, locs in zip(eval_x_list, eval_loc_list)]
        valid_distances = train_x[:,:,1] != 0
        eval_valid_distances = [eval_x[:,:,1] != 0 for eval_x in eval_x_list]
        for i in range(num_epochs):
            self.train()
            tr_pred, pl_pred = self(train_x[:,:,0], train_x[:,:,1:3])
            loss = loss_func(tr_pred[valid_distances], train_distances[valid_distances])
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            tr_mse.append(loss.item())
            self.eval()
            for eval_ind, (eval_x, eval_distances, eval_valid) in enumerate(zip(eval_x_list, eval_distances_list, eval_valid_distances)):
                y_pred, pl_pred = self(eval_x[:,:,0])
                eval_mse_list[eval_ind].append(loss_func(y_pred[eval_valid], eval_distances[eval_valid]).item())
            if i % 500 == 0 and i > 1000:
                print(f'Epoch {i} {min(tr_mse)} {min(eval_mse_list[1])} {min(eval_mse_list[0])}')
                tr_preds, pl_pred = self(train_x[:,:,0])
                te_preds, pl_pred = self(eval_x_list[0][:,:,0])
                tv_preds, pl_pred = self(eval_x_list[1][:,:,0])
                te_locs = eval_loc_list[0]
                tv_locs = eval_loc_list[1]
                tr_ring_preds = np.array([self.get_circle_intersection_prediction(train_x[i,:,1:3].cpu().numpy(), tr_preds[i].detach().cpu().numpy())[0] for i in range(len(train_locs))])
                tv_ring_preds = np.array([self.get_circle_intersection_prediction(eval_x_list[1][i,:,1:3].cpu().numpy(), tv_preds[i].detach().cpu().numpy())[0] for i in range(len(tv_locs))])
                te_ring_preds = np.array([self.get_circle_intersection_prediction(eval_x_list[0][i,:,1:3].cpu().numpy(), te_preds[i].detach().cpu().numpy())[0] for i in range(len(te_locs))])
                tr_ring_err = np.linalg.norm(tr_ring_preds - train_locs[:,0,1:].cpu().numpy(), axis=1)
                tv_ring_err = np.linalg.norm(tv_ring_preds - eval_loc_list[1][:,0,1:].cpu().numpy(), axis=1)
                te_ring_err = np.linalg.norm(te_ring_preds - eval_loc_list[0][:,0,1:].cpu().numpy(), axis=1)
                print('Ring error:', 'Train:', abs(tr_ring_err).mean() * meter_scale,
                      'Val:', abs(tv_ring_err).mean() * meter_scale,
                      'Test:', abs(te_ring_err).mean() * meter_scale)
            #    plt.cla()
            #    plt.plot(tr_mse[100:])
            #    plt.plot(eval_mse_list[1][100:])
            #    plt.plot(eval_mse_list[0][100:])
            #    plt.pause(0.00001)
        #plt.plot(tr_mse[100:])
        #plt.plot(eval_mse_list[1][100:])
        #plt.plot(eval_mse_list[0][100:])
        print('Train:', min(tr_mse), 'Val:', min(eval_mse_list[1]), 'Test:', min(eval_mse_list[0]))
        train_pred = self(train_x[:,:,0])
        train_pred[~valid_distances] = -1 
        train_err = train_pred[valid_distances] - train_distances[valid_distances]

        valid_te = eval_valid_distances[0]
        valid_tv = eval_valid_distances[1]
        te_preds = self(eval_x_list[0][:,:,0])
        tv_preds = self(eval_x_list[1][:,:,0])
        te_locs = eval_loc_list[0]
        tv_locs = eval_loc_list[1]
        tr_ring_preds = np.array([self.get_circle_intersection_prediction(train_x[i,:,1:3].cpu().numpy(), train_pred[i].detach().cpu().numpy())[0] for i in range(len(train_locs))])
        tv_ring_preds = np.array([self.get_circle_intersection_prediction(eval_x_list[1][i,:,1:3].cpu().numpy(), tv_preds[i].detach().cpu().numpy())[0] for i in range(len(tv_locs))])
        te_ring_preds = np.array([self.get_circle_intersection_prediction(eval_x_list[0][i,:,1:3].cpu().numpy(), te_preds[i].detach().cpu().numpy())[0] for i in range(len(te_locs))])
        tr_ring_err = np.linalg.norm(tr_ring_preds - train_locs[:,0,1:].cpu().numpy(), axis=1)
        tv_ring_err = np.linalg.norm(tv_ring_preds - eval_loc_list[1][:,0,1:].cpu().numpy(), axis=1)
        te_ring_err = np.linalg.norm(te_ring_preds - eval_loc_list[0][:,0,1:].cpu().numpy(), axis=1)
        print('Train:', abs(tr_ring_err).mean() * meter_scale,
              'Val:', abs(tv_ring_err).mean() * meter_scale,
              'Test:', abs(te_ring_err).mean() * meter_scale)
        embed()
        return tr_mse, eval_mse_list
    
    def get_circle_intersection_prediction(self, points, radii, kernel=normal_kernel, resolution=200, method='conv'):
        def get_circle_intersections(circles):
            '''
            @summary: calculates intersection points of all circles
            @param circles: [N,3] np.array (x, y, r)
            @result: tuple of intersection points (which are (x,y) tuple)
            '''
            def pairwise_combs(ab):
                n = len(ab)
                N = n*(n-1)//2
                out = np.empty((N,2,ab.shape[-1]),dtype=ab.dtype)
                idx = np.concatenate(( [0], np.arange(n-1,0,-1).cumsum() ))
                start, stop = idx[:-1], idx[1:]
                for j,i in enumerate(range(n-1)):
                    out[start[j]:stop[j],0] = ab[j]
                    out[start[j]:stop[j],1] = ab[j+1:]
                return out
            all_circles = pairwise_combs(circles)
            c1, c2 = all_circles[:,0], all_circles[:,1]
            r1, r2 = all_circles[:,:,2].T
            dc = (c2[:,:2] - c1[:,:2])
            d = np.sqrt((dc**2).sum(axis=1))
            a = (r1*r1-r2*r2+d*d)/(2*d)
            h = np.sqrt(r1*r1-a*a)
            m = c1[:,:2] + a[:,None] * dc/d[:,None]
            s1 = m + h[:,None] * np.array([1,-1]) * np.fliplr(dc) / d[:,None]
            s2 = m + h[:,None] * np.array([-1,1]) * np.fliplr(dc) / d[:,None]
            intersections =  np.concatenate((s1,s2))
            return intersections[~np.isnan(intersections).any(axis=1)]

        points = points[radii > 0]
        radii = radii[radii > 0]
        all_intersections = get_circle_intersections(np.hstack((points, radii[:,None])))
        pts_x, pts_y = all_intersections.T
        if method == 'kde':
            grid_x = np.linspace(0, max(pts_x), num=resolution)
            grid_y = np.linspace(0, max(pts_y), num=resolution)
            x, y = np.meshgrid(grid_x, grid_y, indexing='ij')
            positions = np.vstack([x.ravel(), y.ravel()])
            kernel = gaussian_kde(all_intersections.T)
            z = np.reshape(kernel(positions).T, x.shape)
            xi, yi = np.where(z == np.amax(z))
            pred = np.array([grid_x[xi][0], grid_y[yi][0]])
        else:
            pred = max_density_conv(all_intersections, kernel, resolution)
        return pred, all_intersections

def max_density_conv(intersections, kernel=normal_kernel, resolution=500):
    intersections = intersections[intersections.min(axis=1) > 0]
    if len(intersections) == 0:
        return np.array([0,0])
    max_d = intersections.max()
    scaled = intersections * (resolution - 1) / max_d
    int_ints = scaled.round().astype(int)
    p1 = np.convolve(kernel, np.bincount(int_ints[:,0]), mode='same').argmax() * max_d / (resolution-1)
    p2 = np.convolve(kernel, np.bincount(int_ints[:,1]), mode='same').argmax() * max_d / (resolution-1)
    return np.array([p1,p2])

def main():
    cmd_line_params = []

    for split in [
        #'random',
        #'july_selftest_nov_limited',
        #'nov_selftest_july_limited',
        #'july_selftest_nov',
        #'nov_selftest_july',
        'grid2',
        #'grid3',
        #'grid4',
        'grid5',
        #'grid7',
        'grid10',
        #'grid15',
        #'grid20',
    ]:
        for random_state in range(0,num_training_repeats):
            augs = [
                #[False, False, False, False],
                [True, False, False, False],
                #[False, True, False, False],
                #[False, False, True, False],
                #[False, False, False, True],
            ]
            limits = [ 
                #[0],
                [15], #[6,10,15,20],
                #[0.4,0.5,0.6,0.7,0.9],
                #[0.1,0.2,0.3,0.4],
                #[0]
            ]
            for aug, limit in zip(augs, limits):
                for l in limit:
                    cmd_line_params.append([split, random_state, aug, l])
    
    param_list = cmd_line_params
    
    for ind, param_set in enumerate(param_list):
        split, random_state, aug, limit = param_set
        sensor_dropout, apply_rss_noise, apply_power_scaling, adv_train = aug
        #apply_rss_noise = True
        print(param_set)
        params = LocConfig(dataset_index=6, meter_scale=meter_scale, data_split=split, batch_size=batch_size, random_state=random_state, include_elevation_map=include_elevation_map, force_num_tx=force_num_tx, sensor_dropout=sensor_dropout, apply_rss_noise=apply_rss_noise, apply_power_scaling=apply_power_scaling, power_limit=0.3, scale_limit=limit, min_sensors=limit, device=global_device, better_epoch_limit=better_epoch_limit, adv_train=adv_train, remove_mobile=True, augmentation='tirem_nn')

        rldataset = RSSLocDataset(params, random_state=random_state)
        train_key, test_key = rldataset.make_datasets(make_val=True, should_interpolate=False)

        rldataset.print_dataset_stats()

        distance_model = DistanceMLP(40, sensor_dropout=sensor_dropout)
        distance_model.train_model(
            rldataset.data[train_key].ordered_dataloader,
            [rldataset.data[test_key].ordered_dataloader for test_key in rldataset.test_keys],
            lr=lr,
            num_epochs=num_epochs)




if __name__ == '__main__':
    main()
