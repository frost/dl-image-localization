import time
import numpy as np
import os
import pickle
import argparse
from IPython import embed
from localization import *
from Splot import *
from dataset import ImageDataset
from scipy.stats import pearsonr
plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

lr = 1e-3
test_time_only = False
should_train = False
should_load_model = True
restart_optimizer = False

# Specify params
num_epochs = 500
min_sensors = 6
arch = 'unet_ensemble'
sensor_dropout = False
force_num_tx = 1
include_elevation_map = False
set_random_power = False

batch_size = 4
dataset_index = 7
meter_scale = 200
#dataset_index = 6
#meter_scale = 30 

num_training_repeats = 3

device = torch.device('cpu')

def main():
    global dataset_index
    global meter_scale
    cmd_line_params = []
    parser = argparse.ArgumentParser()
    parser.add_argument("--param_selector", type=int, default=-1, help='Index of pair for selecting params')
    args = parser.parse_args()
    bound = 5000

    for ms, di, split in [
        #[25, 6, 'radius350'],
        [30, 6, 'radius350'],
        #[50, 6, 'radius350'],
        #[20, 6, 'radius350'],
        #[10, 6, 'radius350'],
        #[150, 7, 'radius1200'], 
    ]:
        random_state = 0 
        #Jie's formulation: 3.3, 0.58/10, 25, 10? Something like that...
        #for random_state in range(0,1):
        for pathloss in np.arange(-25,25,5):
          for radius in [500]:
            for sigma2n in [15]:
                for delta_c in [25]:
                  for sigma2x in [0.58]:#, 0.58/sigma2n, 0.58, 100]:
                    cmd_line_params.append([ms, di, split, random_state, sigma2x, delta_c, sigma2n, pathloss, radius])
    
    if args.param_selector > -1:
        param_list = [cmd_line_params[args.params_selector] ]
    else:
        param_list = cmd_line_params
    for param_set in param_list:
        meter_scale, dataset_index, split, random_state, sigma2x, delta_c, sigma2n, pathloss, radius = param_set
        print(param_set)
        params = LocConfig(dataset_index, data_split=split, arch=arch, batch_size=batch_size, meter_scale=30, random_state=random_state, include_elevation_map=include_elevation_map, force_num_tx=force_num_tx, sensor_dropout=sensor_dropout, min_sensors=min_sensors, device=device, set_random_power=set_random_power)

        imd = ImageDataset(params, random_state=random_state)
        train_key, test_key = imd.make_datasets(make_val=True)
        train_data = imd.data[train_key]
        test_data = imd.data[test_key]
        splot_params = get_splot_parameters(imd, train_key, sigma2x=sigma2x, delta_c=delta_c, pathloss=pathloss, sigma2n=sigma2n, pixel_size=meter_scale)
        splot = Splot(imd, train_key, device, params=splot_params, radius=radius)
        tr_truth = np.array(train_data.ordered_dataloader.dataset.tensors[1].cpu())[:,0,1:][:bound] * imd.params.meter_scale
        te_truth = np.array(test_data.ordered_dataloader.dataset.tensors[1].cpu())[:,0,1:][:bound] * imd.params.meter_scale
        tr_splot_preds = splot.estimate(train_data.ordered_dataloader.dataset.tensors[0][:bound], method='range', db_shift=pathloss)
        te_splot_preds = splot.estimate(test_data.ordered_dataloader.dataset.tensors[0][:bound], te_truth, method='range', db_shift=pathloss)

        if 'radius' in imd.params.data_split:
            tv_keys = [key for key in list(imd.data.keys())[1:] if 'inradius' in key and 'train_in' not in key]
        else:
            tv_keys = [key for key in list(imd.data.keys())[1:] if 'train_val' in key]
        tv_splot_preds, tv_truth = [], []
        for tv_ind, tv_key in enumerate(tv_keys):
            tv_data = imd.data[tv_key]
            if not hasattr(tv_data,'dataloader'):
                tv_data.make_tensors()
            tv_truth.append(np.array(tv_data.ordered_dataloader.dataset.tensors[1].cpu())[:,0,1:][:bound] * imd.params.meter_scale)
            tv_splot_preds.append( splot.estimate(tv_data.ordered_dataloader.dataset.tensors[0][:bound], method='range', db_shift=pathloss) )
        tv_truth = np.concatenate(tv_truth)
        tv_splot_preds = np.concatenate(tv_splot_preds)

        tr_splot_err = np.linalg.norm(tr_splot_preds - tr_truth, axis=1)
        tv_splot_err = np.linalg.norm(tv_splot_preds - tv_truth, axis=1)
        te_splot_err = np.linalg.norm(te_splot_preds - te_truth, axis=1)

        print('%.1f  %.1f  %.1f' % (tr_splot_err.mean(), tv_splot_err.mean(), te_splot_err.mean()) )
        print('%.1f  %.1f  %.1f' % (np.median(tr_splot_err), np.median(tv_splot_err), np.median(te_splot_err)) )
        #print('%.1f' % (tr_splot_err.mean()))

if __name__ == '__main__':
    main()
