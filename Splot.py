import math
import numpy as np
from ranging import rss_to_dist, min_max
import matplotlib.pyplot as plt
from dataset import RSSLocDataset
from scipy.stats import linregress
from scipy.spatial import distance_matrix
import torch

class SplotParams:
    def __init__(self) -> None:
        pass
        #x_min, x_max, y_min, y_max, pixel_size, pathloss, sigma2x, delta_c, minpl, sigma2n
        #self.x_min = x_min
        #self.x_max = x_max
        #self.y_min = y_min
        #self.y_max = y_max
        #self.pixel_size = pixel_size
        #self.pathloss = pathloss
        #self.minpl = minpl
        #self.sigma2x = sigma2x
        #self.sigma2n = sigma2n
        #self.delta_c = delta_c

def get_splot_parameters(rldataset: RSSLocDataset, data_key: str, pathloss=3.25, minpl=1, sigma2x=0.58, sigma2n=10.0, delta_c=25.0, pixel_size=None):
    sensor_data = rldataset.data[data_key].ordered_dataloader.dataset.tensors[0]
    targets = rldataset.data[data_key].ordered_dataloader.dataset.tensors[1]
    params = SplotParams()
    params.pixel_size = rldataset.params.meter_scale if pixel_size is None else pixel_size
    params.pathloss = pathloss
    params.minpl = minpl
    params.x_min = 0
    params.y_min = 0
    params.x_max = rldataset.data[data_key].rectangle_width
    params.y_max = rldataset.data[data_key].rectangle_height
    params.sigma2x = sigma2x
    params.sigma2n = sigma2n
    params.delta_c = delta_c
    distance_pairs = []
    #for i in range(40):
    #    cleaned_data = sensor_data[:,i][sensor_data[:,i,0] > 0]
    #    rss = cleaned_data[:,0]
    #    coords = cleaned_data[:,1:3]
    #    distance_pairs.append( (rss, np.linalg.norm(coords - targets[sensor_data[:,i,0] > 0][:,0,1:3], axis=1)) )
    #for i, pair in enumerate(distance_pairs):
    #    small_dist = pair[1][pair[1] < 40]
    #    small_rss = pair[0][pair[1] < 40]
    #    if len(small_dist) < 50: continue
    #    min_rss = rldataset.location_25p_dict[i]
    #    max_rss = rldataset.location_100p_dict[i]
    #    db_rss = pair[0] * (max_rss - min_rss) + min_rss
    #    plt.scatter(pair[1]*meter_scale, db_rss)
    #    plt.xscale('log')
    #    small_db_rss = small_rss * (max_rss - min_rss) + min_rss
    #    line = linregress(np.log10(small_dist*meter_scale), small_db_rss)
    #    x_line = np.array([pair[1].min()-1, pair[1].max()+1])*meter_scale
    #    plt.plot( x_line, line.slope * np.log10(x_line) + line.intercept)
    #    plt.title('%s %.3f  %.3f' % (rldataset.location_index_dict[i], line.slope/10, line.rvalue))
    #    print('%s %.3f  %.3f' % (rldataset.location_index_dict[i], line.slope, line.rvalue))
    #    plt.show()
    return params


class WeightedRSSLocalization:
    def __init__(self, rldataset: RSSLocDataset, data_key: str, radius=600) -> None:
        self.meter_scale
        pass


class Splot:

    def __init__(self, rldataset: RSSLocDataset, data_key: str, device, radius = 500, rss_thresh = -200, params=None):
        self.device = device
        self.method = 'splot'
        if params is None:
            if rldataset.dataset_index == 6:
                self.para = get_splot_parameters(rldataset, data_key, pathloss=3.0, sigma2x=0.58, delta_c=25, pixel_size=30)
                self.method = 'range'
                radius = 500
                self.db_shift = 19.5
            elif rldataset.dataset_index == 7:
                self.para = get_splot_parameters(rldataset, data_key, pathloss=1.4, sigma2x=0.1, delta_c=10, pixel_size=300)
                self.method = 'range'
                radius = 5000
                self.db_shift = 19.5
        else:
            self.para = params
        self.meter_scale = rldataset.params.meter_scale
        self.radius = radius
        self.rss_thresh = rss_thresh
        if rldataset.dataset_index == 6:
            self.min_rss = torch.tensor([rldataset.location_25p_dict[i] if i in rldataset.location_25p_dict else -100 for i in range(rldataset.max_num_rx) ]).double().to(device)
            self.max_rss = torch.tensor([rldataset.location_100p_dict[i] if i in rldataset.location_100p_dict else -4 for i in range(rldataset.max_num_rx) ]).double().to(device)
        elif rldataset.dataset_index == 7:
            self.min_rss = -126
            self.max_rss = -70
        self.grid_centers = self.calculate_grid_centers()
        self.C = self.calculate_covariance_matrix()
        self.C_inv = torch.linalg.inv(self.C)

    def calculate_grid_centers(self):
        x_min, y_min, x_max, y_max = self.para.x_min, self.para.y_min, self.para.x_max, self.para.y_max
        pixel_size = self.para.pixel_size
        x = torch.arange(x_min, x_max, pixel_size).to(self.device) + pixel_size / 2.0
        y = torch.arange(y_min, y_max, pixel_size).to(self.device) + pixel_size / 2.0
        meshgrid = torch.meshgrid(x, y, indexing='ij')
        centers = torch.stack(meshgrid).reshape(2,-1).T
        return centers

    @staticmethod
    def get_distance(x, y):
        return math.sqrt((x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2)

    # Function for calculating the Covariance matrix
    def calculate_covariance_matrix(self):
        voxels = self.grid_centers
        C = self.para.sigma2x * torch.e ** (-1 * torch.cdist(voxels, voxels) / self.para.delta_c)
        return C

    def estimate(self, rx_tensor, truths=None, method=None, db_shift=None):
        if method is None:
            method = self.method
        assert method in ['splot', 'range']
        if not isinstance(rx_tensor, torch.Tensor):
            rx_tensor = torch.tensor(rx_tensor)
        if len(rx_tensor.shape) == 2:
            rx_tensor = rx_tensor[None]
        results = []
        length = len(rx_tensor)
        if method == 'range' and db_shift is None:
            db_shift = self.db_shift
        for i, sample in enumerate(rx_tensor):
            rx_online_pos = sample[sample[:,0] > 0][:,1:3] * self.meter_scale
            db_rss = sample[:,0] * (self.max_rss - self.min_rss) + self.min_rss
            db_rss = db_rss[sample[:,0] > 0]
            truth = None if truths is None else truths[i]
            if method == 'splot':
                results.append( self._splot_estimation(rx_online_pos, db_rss, truth) )
            elif method == 'range':
                results.append( self._range_estimation(rx_online_pos, db_rss, truth, db_shift=db_shift) )

            #print('%i/%i          ' % (i,length), end='\r')
        return torch.stack(results).cpu().numpy()

    def _range_estimation(self, rx_online_pos, rss, truth=None, db_shift=-8):
        ### Only returning a single prediction, not like SPLOT paper which has MTL
        local_maximum = rx_online_pos[rss.argmax()]
        inds = torch.linalg.norm(rx_online_pos - local_maximum, axis=1) < self.radius
        temp_loc = rx_online_pos[inds]
        temp_rss = rss[inds]
        if truth is not None:
            true_distances = np.linalg.norm(truth - temp_loc.numpy(), axis=1)
        else:
            true_distances = None
        distances = rss_to_dist(temp_rss, truth=true_distances, db_shift=db_shift)
        range_estimate = min_max(distances, temp_loc)
        if False: #truth is not None:
            fig, ax = plt.subplots()
            ax.scatter(rx_online_pos[:,0], rx_online_pos[:,1])
            ax.scatter(truth[0], truth[1], c='green', marker='*')
            ax.scatter(range_estimate[0], range_estimate[1], c='red', marker='+')
            print(np.linalg.norm(truth - range_estimate.numpy()))
            for pos, dist in zip(temp_loc, distances):
                circle = plt.Circle((pos[0], pos[1]), dist, fill=False)
                ax.add_patch(circle)
            plt.show()
        return range_estimate

    def _splot_estimation(self, rx_online_pos, rss, truth=None):
        ### Only returning a single prediction, not like SPLOT paper which has MTL
        local_maximum = rx_online_pos[rss.argmax()]
        inds = torch.linalg.norm(rx_online_pos - local_maximum, axis=1) < self.radius
        temp_loc = rx_online_pos[inds]
        temp_rss = rss[inds]

        # Function call for creating the weight matrix
        W = self.compute_weight_matrix(temp_loc, self.grid_centers)
        # Computation of pi
        pi = torch.linalg.inv(W.T @ W + self.para.sigma2n * self.C_inv) @ W.T

        # Convert the rss measurement in watts
        temp_rss = (10 ** (temp_rss/10.0) / 1000.0).double()

        # Matrix inversion algorithm
        x_cap = pi @ temp_rss
        max_index = torch.argmax(x_cap)
        tx_prediction = self.grid_centers[max_index]
        return tx_prediction

    # Function for creating the weight matrix
    def compute_weight_matrix(self, receivers, voxels):
        dist = torch.cdist(receivers, voxels, ).double()
        dist[dist < self.para.minpl] = self.para.minpl
        W = dist ** (-1 * self.para.pathloss)
        return W