import matplotlib
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import numpy as np
import pandas as pd
import torch
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
#from GetResults import *
from dataset import RSSLocDataset
from locconfig import LocConfig
from localization import DLLocalization
from IPython import embed

plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

dpi = 300

#plt.scatter(tr_truth[:,0,0], tr_truth[:,0,1], c='red', marker='x', alpha=0.1)
#
#for truth, pred in zip(te_truth[conf_te_inds], te_preds[conf_te_inds]):
#    plt.plot([truth[0,0], pred[0,0]], [truth[0,1], pred[0,1]], c='tab:blue', alpha=0.3)
#sc = plt.scatter(te_preds[conf_te_inds,0,0], te_preds[conf_te_inds,0,1], c=te_err[conf_te_inds])
#
#plt.scatter(te_truth[conf_te_inds,0,0], te_truth[conf_te_inds,0,1], c=te_err[conf_te_inds], marker='x')
#plt.colorbar(sc); plt.show()

def make_powder_map(figsize=(4.5,4.5), mode='rx'):
    assert mode in ['rx', 'tx']
    params = TLDLParams(force_num_tx=1, batch_size=2, meter_scale=1, include_elevation_map=True)
    import pdb
    pdb.set_trace()
    imd = ImageDataset(params)
    imd.make_elevation_tensors()
    fig, ax = plt.subplots(figsize=figsize)
    if mode == 'tx':
        all_tx_locations = []
        for tx_vec in imd.data[None].tx_vecs:
            for tx in tx_vec:
                all_tx_locations.append(tx)
        data = np.array(all_tx_locations) - imd.data[None].origin
        ax.scatter(data[:,0], data[:,1], marker='x', s=8, alpha=0.4)
        
    elif mode == 'rx':
        all_rx_locations = {}
        for rx_vec in imd.data[None].rx_vecs:
            for rx in rx_vec:
                loc_cat = rx[1:]
                id = int(rx[3])
                if id not in all_rx_locations:
                    all_rx_locations[id] = []
                if not any(np.array_equal(loc_cat, x) for x in all_rx_locations[id]):
                    all_rx_locations[id].append(loc_cat)
        for id in all_rx_locations:
            all_rx_locations[id] = np.array(all_rx_locations[id])
        loc_dict = copy.copy(imd.location_index_dict)
        old_names = {}
        categories = ['bus', 'cbrs', 'cnode', 'nuc']
        pretty_keys = []
        key_counters = {'M':0, 'R':0, 'D':0, 'F':0}
        simplify_labels = True
        ordered_names = sorted(loc_dict.values() )
        s = ordered_names[0]
        ordered_names.remove(ordered_names[0])
        ordered_names.insert(19, s)
        for bad_name in ordered_names:
            name = copy.copy(bad_name)
            name = name.replace('bus','Mobile-').replace('-comp','').replace('cbrssdr1','Rooftop').replace('cnode','Dense').replace('-dd','').replace('-b210','')
            if 'nuc' in name:
                name = 'Fixed-' + name.split('-nuc')[0]
            if simplify_labels and name not in old_names:
                old_name = name
                name = '%s-%i' % (name[0], key_counters[name[0]] + 1)
                key_counters[name[0]] += 1
                old_names[old_name] = name
            else:
                name = old_names[old_name]
            key = list(loc_dict.keys())[list(loc_dict.values()).index(bad_name)]
            loc_dict[key] = name

        #[1 if 'bus-' in name else 2 if 'cnode' in name else 3 if 'cbrs' in name else 4 for name in location_names]
        # Color order from the category ordering, matching the RSS distribution plots.
        colors = [0, 'tab:blue', 'tab:green', 'tab:orange', 'tab:red']
        markers = [0, 'x', '^', 'v', 's']
        labels = [0, 'Mobile', 'Dense', 'Rooftop', 'Fixed']
        used_markers = []
        done = []
        for id in all_rx_locations:
            data = all_rx_locations[id].copy()
            data[:,:2] -= imd.data[None].origin
            label = loc_dict[int(data[0,-2])]
            color = colors[int(data[0,-1])]
            marker = markers[int(data[0,-1])]
            if len(data) == 1: # Fixed transmitter
                if label in done: continue
                if marker in used_markers:
                    ax.scatter(data[0,0], data[0,1], c=color, marker=marker)
                else:
                    ax.scatter(data[0,0], data[0,1], c=color, marker=marker, label=labels[int(data[0,-1])])
                    used_markers.append(marker)
                #if label == 'D-3':
                #    ax.annotate(label, (data[0,0]-80, data[0,1]+15))
                #else:
                #    ax.annotate(label, (data[0,0]-10, data[0,1]+20))
                done.append(label)
            else:
                if marker in used_markers:
                    ax.scatter(data[:,0], data[:,1], c=color, marker='x', s=1, alpha=0.3)
                else:
                    ax.scatter(data[0,0], data[0,1], c=color, marker='x', s=8, label=labels[int(data[0,-1])])
                    used_markers.append(marker)
            
    plt.imshow(imd.elevation_tensors[0], origin='lower', cmap='Greys', norm=LogNorm(vmin=0.09, vmax=3))
    plt.ylim(500,2170)
    plt.xlim(50,2100)
    plt.xticks([],[])
    plt.yticks([],[])
    if mode == 'rx':
        plt.legend()
    plt.tight_layout()
    plt.savefig('plots/tx_powder_map.pdf' , bbox_inches='tight')
    plt.savefig('plots/tx_powder_map.png' , bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')
   

def get_image_scale_test_images():
    params = TLDLParams(force_num_tx=1, batch_size=2, meter_scale=10)
    imd = ImageDataset(params)
    train_key, test_key = imd.make_datasets()
    test_data = imd.data[test_key]
    filename = 'models/arch:unet__CatMult:True__split:random__DS:6__DevMult:True__elev:False__img_size:1000__img_scale:10__randstate:0__test_size:0.2__model_train_val.pt'
    tldl.load_model(filename)
    test_loss, test_res, test_images = tldl.get_results(test_data, force_ordered=True, save_images=True)
    fig = plt.figure(frameon=False)
    ax = plt.Axes(fig, [0.,0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(test_images[0][6,0], origin='lower')
    fig.savefig('plots/map_images/test_img_6_10m_pred.png')
    print('10m max value:', test_images[0][6,0].max(), test_res['error'][6])

    params = TLDLParams(force_num_tx=1, batch_size=2, meter_scale=60)
    imd = ImageDataset(params)
    train_key, test_key = imd.make_datasets()
    test_data = imd.data[test_key]
    filename = 'final_models/arch:unet__CatMult:True__split:random__DS:6__DevMult:True__elev:False__img_size:1000__img_scale:60__Final__test_size:0.2__model_train.pt'
    tldl = TLDL(imd, vec2im_for_loss=True)
    tldl.load_model(filename)
    test_loss, test_res, test_images = tldl.get_results(test_data, force_ordered=True, save_images=True)
    fig = plt.figure(frameon=False)
    ax = plt.Axes(fig, [0.,0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(test_images[0][6,0], origin='lower')
    fig.savefig('plots/map_images/test_img_6_60m_pred.png')
    print('60m max value:', test_images[0][6,0].max(), test_res['error'][6])


def plot_rss_distributions(figsize=(8,3), simplify_labels=False):
    with open('powder_data.pkl', 'rb') as f:
        tx_dicts, no_tx_dicts, location_index_dict, bounds, q25, q100 = pickle.load(f)
    new_location_index_dict = {}
    for key in location_index_dict:
        new_location_index_dict[ location_index_dict[key]] = key
    all_rss = {}
    for time in tx_dicts:
        data = tx_dicts[time]['rx_data']
        for row in data:
            loc = new_location_index_dict[int(row[-1])].replace('-nuc1','-nuc').replace('-nuc2','-nuc')
            if loc not in all_rss:
                all_rss[loc] = []
            all_rss[loc].append(row[0])
    make_violinplots(all_rss, 'Uncalibrated RSS (dB)', figsize, simplify_labels=simplify_labels)
    plt.savefig('plots/rss_distributions%s.pdf' % '_simple' if simplify_labels else '', bbox_inches='tight')
    plt.savefig('plots/rss_distributions%s.png' % '_simple' if simplify_labels else '', bbox_inches='tight', dpi=dpi)


def plot_calibrated_distributions(figsize=(8,3), simplify_labels=False):
    params = TLDLParams(force_num_tx=1, batch_size=2, meter_scale=60)
    imd = ImageDataset(params)
    train_key, test_key = imd.make_datasets()
    train_data = imd.data[train_key]
    test_data = imd.data[test_key]
    filename = 'final_models/final_model_60m.pt'
    tldl = TLDL(imd, vec2im_for_loss=True)
    tldl.load_model(filename)

    calibrated_rss = {}
    normalized_rss = {}
    for data in [train_data, test_data]:
        for rx_vec in data.rx_vecs:
            for row in rx_vec:
                loc = imd.location_index_dict[int(row[-2])].replace('-nuc1','-nuc').replace('-nuc2','-nuc')
                if loc not in calibrated_rss:
                    calibrated_rss[loc] = []
                    normalized_rss[loc] = []
                calibrated_rss[loc].append((row[0] * tldl.vec2im.category_weights[int(row[-1])] + tldl.vec2im.category_bias[int(row[-1])]).item() )
                normalized_rss[loc].append(row[0])

    ylabel = 'Calibrated RSS (normalized)'
    make_violinplots(calibrated_rss, ylabel, figsize, simplify_labels=simplify_labels)
    plt.savefig('plots/calibrated_rss_distributions%s.pdf' % '_simple' if simplify_labels else '', bbox_inches='tight')
    plt.savefig('plots/calibrated_rss_distributions%s.png' % '_simple' if simplify_labels else '', bbox_inches='tight', dpi=dpi)

    ylabel = 'Normalized RSS'
    make_violinplots(normalized_rss, ylabel, figsize, simplify_labels=simplify_labels)
    plt.savefig('plots/normalized_rss_distributions%s.pdf' % '_simple' if simplify_labels else '', bbox_inches='tight')
    plt.savefig('plots/normalized_rss_distributions%s.png' % '_simple' if simplify_labels else '', bbox_inches='tight', dpi=dpi)


def make_violinplots(all_rss, ylabel, figsize=(8,5), simplify_labels=False):
    for loc in all_rss:
        arr = np.array(all_rss[loc])
        all_rss[loc] = arr[arr > -np.inf]
    ordered_keys = sorted(all_rss.keys() )
    s = ordered_keys[0]
    ordered_keys.remove(ordered_keys[0])
    ordered_keys.insert(19, s)
    ordered_values = [all_rss[key] for key in ordered_keys]
    categories = ['bus', 'cbrs', 'cnode', 'nuc']
    ind = 0
    fig = plt.figure(figsize=figsize)
    for cat in categories:
        rss = [all_rss[key] for key in ordered_keys if cat in key]
        plt.violinplot(rss, positions=range(ind, len(rss)+ind), showextrema=False)
        ind += len(rss)
    pretty_keys = []
    key_counters = {'M':0, 'R':0, 'D':0, 'F':0}
    for key in ordered_keys:
        key = key.replace('bus','Mobile-').replace('-comp','').replace('cbrssdr1','Rooftop').replace('cnode','Dense').replace('-dd','').replace('-b210','')
        if 'nuc' in key:
            key = 'Fixed-' + key.replace('-nuc','')
        if simplify_labels:
            key = '%s-%i' % (key[0], key_counters[key[0]] + 1)
            key_counters[key[0]] += 1
        pretty_keys.append(key)
    plt.xticks(range(0, len(all_rss)), pretty_keys, rotation=-90)
    plt.ylabel(ylabel)
    patches4 = [
        mpatches.Patch(color='tab:blue', alpha=0.3, label='Mobile'),
        mpatches.Patch(color='tab:orange',alpha=0.3,  label='Rooftop'),
        mpatches.Patch(color='tab:green', alpha=0.3, label='Dense'),
        mpatches.Patch(color='tab:red', alpha=0.3, label='Fixed'), ]
    plt.legend(handles=patches4, ncol=4, loc='upper center')
    plt.tight_layout()

def get_calibrated_lines():
    params = TLDLParams(force_num_tx=1, batch_size=2, meter_scale=60)
    imd = ImageDataset(params)
    train_key, test_key = imd.make_datasets()
    train_data = imd.data[train_key]
    test_data = imd.data[test_key]

    filenames = [
        #'final_models/arch:unet__CatMult:True__split:random__DS:6__DevMult:True__elev:False__img_size:1000__img_scale:60__Final__test_size:0.2__model_train.pt',
        #'final_models/arch:unet__split:random__DS:6__DevMult:True__elev:False__img_size:1000__img_scale:60__Final__test_size:0.2__model_train.pt',
        #'final_models/arch:unet__CatMult:True__split:random__DS:6__elev:False__img_size:1000__img_scale:60__Final__test_size:0.2__model_train.pt',
        ]

    fig, axs = plt.subplots(1,len(filenames), sharex=True, sharey=True)
    for axs_ind, filename in enumerate(filenames):
        tldl = TLDL(imd, vec2im_for_loss=True)
        tldl.load_model(filename)
        x = torch.linspace(0,1,50).to(params.device)
        colors = ['tab:purple','tab:blue', 'tab:orange', 'tab:red','tab:green']
        valid_inds = []
        all_test_rss = []
        all_new_test_rss = []
        for device in range(34):
            name = imd.location_index_dict[device]
            category = 1 if 'bus-' in name else 2 if 'cnode' in name else 3 if 'cbrs' in name else 4 
            entry_min_rss = imd.location_25p_dict[device] - 10
            if device not in imd.location_100p_dict:
                continue
            entry_max_rss = imd.location_100p_dict[device]
            new_rss = ((tldl.vec2im.device_weights[device] * x + tldl.vec2im.device_bias[device]) * tldl.vec2im.category_weights[category] + tldl.vec2im.category_bias[category]) * (entry_max_rss - entry_min_rss) - entry_min_rss
            axs[axs_ind].plot(x.detach().cpu(), new_rss.detach().cpu(), label=device, c=colors[category])
            true_rss = test_data.ordered_dataloader.dataset.tensors[0][:,device,0]
            new_rss = ((tldl.vec2im.device_weights[device] * true_rss + tldl.vec2im.device_bias[device]) * tldl.vec2im.category_weights[category] + tldl.vec2im.category_bias[category]) * (entry_max_rss - entry_min_rss) - entry_min_rss
            valid_inds.append(true_rss != 0)
            all_test_rss.append(true_rss * (entry_max_rss - entry_min_rss) - entry_min_rss)
            all_new_test_rss.append(new_rss)
            axs[axs_ind].scatter(true_rss.cpu().detach().numpy(), new_rss.cpu().detach().numpy(), c=colors[category])
    from IPython import embed
    plt.show()
    embed()


def plot_competition(results_dict, figsize=(6,3.3), show_image=False):
    results = {}
    pretty_arches = [ 'U-Net',  
        'U-Net+Linear', 
        'DeepTx', 'DeepMTL', 'LLOCUS', 'SPLOT']
    arches = [ 'unet60', 
        'unet_notarget_linear',  
        'deeptx', 'deepmtl', 'knn', 'mi']
    set_ordering = ['random', 'grid10', 'driving', 'non-driving', 'april', 'july']
    pretty_sets =     ['Random', 'Grid', 'Pedestrian', 'Driving', 'July', 'April']
    for arch in arches:
        results[arch] = get_split_results(results_dict, arch=arch, final=False)
    plot_res = {}
    for label, arch in zip(pretty_arches, arches):
        plot_res[label] = []
        for key in set_ordering:
            plot_res[label].append(np.median(results[arch][key]['error']))
    df = pd.DataFrame(plot_res, index=pretty_sets)
    ax = df.plot.bar(rot=0, figsize=figsize)
    ax.legend(ncol=3, framealpha=0.5)
    plt.ylim(0, 490)
    plt.ylabel('Median Error [m]')
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig('plots/all_competition.pdf', bbox_inches='tight')
        plt.savefig('plots/all_competition.png', bbox_inches='tight', dpi=dpi)
        plt.cla()
    #embed()

def plot_llocus_comparison(results_dict, figsize=(6,3.1)):
    results = get_llocus_results(results_dict)
    plot_res = {}
    pretty_arches = [
        'LLOCUS$_{KC}$', 'LLOCUS$_K$', 
        #'LLOCUS$_{RC}$', 'LLOCUS$_R$',
        'SPLOT$_C$', 'SPLOT'
        ]
    arches = [
        ('knn',True), ('knn',False),
        #('rbf',True), ('rbf',False),
        ('mi',True), ('mi',False), 
        ]
    set_ordering = ['random', 'grid10', 'driving', 'non-driving', 'april', 'july']
    pretty_sets =     ['Random', 'Grid', 'Pedestrian', 'Driving', 'July', 'April']
    for label, arch in zip(pretty_arches, arches):
        plot_res[label] = []
        for key in set_ordering:
            med_error = np.median(results[(arch[0],key,False,arch[1])]['error'])
            plot_res[label].append( med_error )
    for arch in ['knn', 'rbf', 'mi']:
        change = np.zeros(len(set_ordering))
        for i, key in enumerate(set_ordering):
            cat_error = np.median(results[(arch,key,False,True)]['error'])
            simple_error = np.median(results[(arch,key,False,False)]['error'])
            change[i] = simple_error - cat_error
        print(arch, abs(change).mean(), change)
    df = pd.DataFrame(plot_res, index=pretty_sets)
    ax = df.plot.bar(rot=0, figsize=figsize, )
    ax.legend(ncol=2)
    plt.ylabel('Median Error [m]')
    plt.tight_layout()
    plt.savefig('plots/llocus_calibration_comparison.pdf', bbox_inches='tight')
    plt.savefig('plots/llocus_calibration_comparison.png', bbox_inches='tight', dpi=dpi)
    #embed()


def plot_error_lines(results_dict, cropped_figsize=(7,3), full_figsize=(8,5)):
    split_results = get_split_results(results_dict)
    split_train_locations = get_train_locations(results_dict)

    for key in split_results:
        make_error_line_plot(split_results[key], split_train_locations[key], crop_plot=True, figsize=cropped_figsize, key_title=key)
        make_error_line_plot(split_results[key], split_train_locations[key], crop_plot=False, figsize=full_figsize, key_title=key)


def plot_special_cases(results_dict, conf_limit=0.5, figsize=(6,4)):
    special_results = get_special_results(results_dict)
    split_results = get_split_results(results_dict)
    results = {**special_results, **split_results}

    total_dataset_size = len(results['april']['error']) + len(results['july']['error'])
    table_data = []
    headers = [
        'Test Set', 
        'Train Set', 
        'Train Size', 
        'Mean Error', 
        'Median Error', 
        'Test Size',
        #'99th \% Error', 
        'Mean Error', 
        'Median Error', 
        'Test Size',
        #'99th \% Error', 
        ]
    table_data.append(headers)
    key_ordering = ['random', 'grid10', 'driving', 'non-driving', 'april', 'july', None, '2tx', 'indoor', 'off_campus']
    training_sets = ['Random', 'Grid', 'Driving', 'Pedestrian', 'April', 'July', None, 'Random', 'Random', 'Random']
    test_sets =     ['Random', 'Grid', 'Pedestrian', 'Driving', 'July', 'April', None, '2-Tx', 'Indoor', 'Off-Campus']
    for ind, key in enumerate(key_ordering):
        train_set = training_sets[ind]
        test_set = test_sets[ind]
        if key == None:
            table_data.append([])
            continue
        errors = results[key]['error']
        conf = results[key]['preds'][:,:,2]
        test_size = len(errors)
        conf_inds = conf > conf_limit
        row = [
            test_set,                          
            train_set,                       
            total_dataset_size - test_size,  
            np.mean(errors, axis=0),              
            np.median(errors, axis=0),              
            test_size,                       
            #np.quantile(errors, 0.99, axis=0),      
            np.mean(errors[conf_inds], axis=0),              
            np.median(errors[conf_inds], axis=0),              
            len(errors[conf_inds]),
            #np.quantile(errors[conf_inds], 0.99, axis=0),      
            ]
        if key == '2tx':
            row = [
                test_set,
                train_set,
                total_dataset_size - test_size,
                np.mean(errors, axis=0),
                np.median(errors, axis=0),
                '%s, %s' % (test_size, test_size),
                #np.quantile(errors, 0.99, axis=0),
                np.array([np.mean(errors[conf_inds[:,0],0]), np.mean(errors[conf_inds[:,1],1])]),
                np.array([np.median(errors[conf_inds[:,0],0]), np.median(errors[conf_inds[:,1],1])]),
                '%s, %s' % (len(errors[conf_inds[:,0],0]), len(errors[conf_inds[:,1],1])),
                #np.array([np.quantile(errors[conf_inds[:,0],0], 0.99), np.quantile(errors[conf_inds[:,1],1], 0.99)])
            ]
        table_data.append(row)
    make_latex_table(table_data)
    #embed()
    
    #for key in special_keys:


def make_latex_table(data):
    num_cols = len(data[0])
    text = '\\begin{table*}[]\n    \centering\n'
    text += '    \\begin{tabular}{'
    for i in range(num_cols): 
        text += 'c|'
        if i == 2: text += '|'
    text += '}\n'
    pad = ' ' * 8
    text += pad + '\multicolumn{3}{c||}{} &  \multicolumn{3}{c|}{All Samples} & \multicolumn{3}{c|}{Confidence > 0.5}  \\\\ \n'
    #text += pad + '\hline\n'
    for row_ind, row in enumerate(data):
        text += pad
        if len(row) == 0:
            text += '\hline \n'
            continue
        for col in row:
            if isinstance(col, float):
                col = round(col,1)
            elif isinstance(col, np.ndarray):
                col = str([a for a in col.round(1)]).replace('[','').replace(']','')
            text += str(col) + ' & '
        text = text[:-2]
        text += '\\\\'
        if row_ind == 0: # or row_ind == len(data) - 1:
            text += ' \hline'
        text += '\n' 
    text += pad + ' \hline \n'
    text += '    \end{tabular}\n'
    text += '\caption{Caption}\n'
    text += '\label{tab:table}\n'
    text += '\end{table*}\n'
    print(text)
    with open('plots/latex_table.txt', 'w') as f:
        f.write(text)


def make_error_line_plot(res, train_locations=None, conf=0.2, err_bound=1200, lower_bound=0, crop_plot=True, figsize=(7,3), key_title='', show_image=False):
    te_truth = np.array(res['truth'])
    te_err = np.array(res['error']).squeeze()
    te_preds = np.array(res['preds'])
    te_max = np.array(res['preds'])[:,:,2].squeeze()
    conf_te_inds = (te_max > conf) * (te_err < err_bound) * (te_err >= lower_bound)
    fig, ax = plt.subplots(figsize=figsize)
    for truth, pred in zip(te_truth[conf_te_inds], te_preds[conf_te_inds]):
        try:
            plt.plot([truth[0,0], pred[0,0]], [truth[0,1], pred[0,1]], c='tab:blue', alpha=0.3)
        except:
            plt.plot([truth[0], pred[0]], [truth[1], pred[1]], c='tab:blue', alpha=0.3)
    if train_locations is not None:
        plt.scatter(train_locations[:,0], train_locations[:,1], marker='.', label='Training Data', c='tab:orange', alpha=0.3)
    try:
        plt.scatter(te_truth[conf_te_inds,0,0], te_truth[conf_te_inds,0,1], c=te_err[conf_te_inds], marker='o', label='Ground Truth')
        sc = plt.scatter(te_preds[conf_te_inds,0,0], te_preds[conf_te_inds,0,1], c=te_err[conf_te_inds], marker='x',label='Prediction')
        x_min = min(te_preds[conf_te_inds,0,0].min(), te_truth[conf_te_inds,0,0].min())
        x_max = max(te_preds[conf_te_inds,0,0].max(), te_truth[conf_te_inds,0,0].max())
        y_min = min(te_preds[conf_te_inds,0,1].min(), te_truth[conf_te_inds,0,1].min())
        y_max = max(te_preds[conf_te_inds,0,1].max(), te_truth[conf_te_inds,0,1].max())
    except:
        plt.scatter(te_truth[conf_te_inds,0], te_truth[conf_te_inds,1], c=te_err[conf_te_inds], marker='o', label='Ground Truth')
        sc = plt.scatter(te_preds[conf_te_inds,0], te_preds[conf_te_inds,1], c=te_err[conf_te_inds], marker='x', label='Prediction')
        x_min = min(te_preds[conf_te_inds,0].min(), te_truth[conf_te_inds,0].min())
        x_max = max(te_preds[conf_te_inds,0].max(), te_truth[conf_te_inds,0].max())
        y_min = min(te_preds[conf_te_inds,1].min(), te_truth[conf_te_inds,1].min())
        y_max = max(te_preds[conf_te_inds,1].max(), te_truth[conf_te_inds,1].max())
    if crop_plot:
        plt.xlim(120, 1850)
        plt.ylim(520, 1350)
    else:
        plt.xlim(x_min - 10, x_max + 10)
        plt.ylim(y_min - 10, y_max + 10)
    plt.xticks([],[])
    plt.yticks([],[])
    ax.set_aspect('equal')
    plt.colorbar(sc, label='Localization Error [m]')
    plt.legend(framealpha=0.9)
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig('plots/%s%s_error_line_plots.pdf' % (key_title,'_cropped' if crop_plot else ''), bbox_inches='tight')
        plt.savefig('plots/%s%s_error_line_plots.png' % (key_title,'_cropped' if crop_plot else ''), bbox_inches='tight', dpi=dpi)
        plt.clf()
        plt.close('all')


def plot_default_results(results_dict, figsize=(6,3)):
    data = np.loadtxt('random_testdata.txt')
    distance = data[:,0]
    num_rx = data[:,1]
    res = get_default_results(results_dict)
    errors = res['error'].squeeze()
    preds = res['preds']
    truth = res['truth']
    conf = preds[:,2]
    plot_distance_curve(errors, distance)
    plot_confidence_curve(errors, conf)
    embed()


def plot_distance_curve(errors, distance, figsize=(6,3), outlier_threshold=300):
    inds = errors < outlier_threshold
    fig = plt.figure(figsize=figsize)
    a,b = np.polyfit(distance[inds], errors[inds], 1)
    plt.scatter(distance[inds], errors[inds], marker='.')
    plt.plot(np.arange(0,600), a*np.arange(0,600)+b, c='tab:red')
    plt.xlim([-25,distance[inds].max()+50])
    plt.xlabel('Distance from Tx to nearest Rx [m]')
    plt.ylabel('Error [m]')
    plt.tight_layout()
    plt.savefig('plots/random_distance_vs_error.pdf', bbox_inches='tight')
    plt.savefig('plots/random_distance_vs_error.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')


def plot_confidence_curve(errors, conf, figsize=(6,3), outlier_threshold=1e6):
    inds = errors < outlier_threshold
    fig = plt.figure(figsize=figsize)
    plt.scatter(conf[inds], errors[inds], marker='.')
    #a,b = np.polyfit(conf[inds], errors[inds], 1)
    #plt.plot(np.arange(0,700), a*np.arange(0,700)+b, c='tab:red')
    plt.xlabel('Confidence')
    plt.ylabel('Error [m]')
    plt.tight_layout()
    plt.savefig('plots/random_confidence_vs_error.pdf', bbox_inches='tight')
    plt.savefig('plots/random_confidence_vs_error.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')


def plot_technique_boosting(results_dict, figsize=(6,2.5), show_image=False):
    for split in ['random']:#, 'grid10']:
        results = get_boosted_techniques(results_dict, split)
        arches = ['unet', 'unet_notarget_linear', 'deepmtl', 'deeptx']
        pretty_arches = ['U-Net', 'U-Net+Lin', 'DeepMTL', 'DeepTx', ]
        #pretty_arches = ['U-Net$_{60}$', 'U-Net+Lin$_{60}$', 'DeepMTL$_{100}$', 'DeepTx$_{110}$', ]
        pretty_boost_options = ['Default', 'Bagged']
        plot_med = {}
        plot_mean = {}
        for label, arg in zip(pretty_boost_options, [True, False]):
            med_arr = []
            mean_arr = []
            for arch in arches:
                try:
                    med_arr.append( np.median(results[(arg, arch)]['error']) )
                    mean_arr.append( np.mean(results[(arg, arch)]['error']) )
                except:
                    med_arr.append( 0 )
                    mean_arr.append( 0 )
            plot_med[label] = med_arr
            plot_mean['_'+label] = mean_arr
        df1 = pd.DataFrame(plot_med, index=pretty_arches)
        df2 = pd.DataFrame(plot_mean, index=pretty_arches)
        ax = df2.plot.bar(rot=0, figsize=figsize, alpha=0.5)
        df1.plot.bar(rot=0, figsize=figsize, ax=ax)
        ax.set_ylabel('Median \& Mean Error [m]')
        ax.legend(ncol=2)
        #plt.tight_layout()
        if show_image:
            plt.show()
        else:
            plt.savefig('plots/%s_boosted_techniques.pdf' % split, bbox_inches='tight')
            plt.savefig('plots/%s_boosted_techniques.png' % split, bbox_inches='tight', dpi=dpi)
            plt.clf()
            plt.close('all')
    #plt.xticks()
    #embed()


def plot_calibration_methods_all_techniques(results_dict, figsize=(6,2.5), include_none=False, show_image=False):
    arches = ['unet']#, 'unet_notarget_linear', 'deepmtl', 'deeptx']
    pretty_arches = ['U-Net']#, 'U-Net+Linear', 'DeepMTL', 'DeepTx', ]
    if include_none:
        ordered_args = ['None', 'cat', 'dev']#,  'dist']
        pretty_args = ['Uncalib.', 'Category', 'Sensor']#, 'Distance']
    else:
        ordered_args = ['cat', 'dev']#,  'dist']
        pretty_args = ['Category', 'Sensor']#, 'Distance']
    set_ordering = ['random', 'grid10', 'driving', 'non-driving', 'april', 'july']
    pretty_sets =     ['Random', 'Grid', 'Pedestrian', 'Driving', 'July', 'April']
    for arch, pretty_arch in zip(arches, pretty_arches):
        results = get_calibration_methods(results_dict, arch)
        print(pretty_arch)
        #print(results.keys())
        plot_res = {}
        min_err = 1e6
        max_err = 0
        for label, arg in zip(pretty_args, ordered_args):
            #label = label.replace('TECH', pretty_arch)
            mean_arr = []
            med_arr = []
            for split in set_ordering:
                try:
                    med_arr.append( np.median(results[(split, arg)]['error']) / np.median(results[(split,'None')]['error']) )
                    mean_arr.append( np.mean(results[(split, arg)]['error']) / np.mean(results[(split,'None')]['error']) )
                except:
                    med_arr.append( 0 )
                    mean_arr.append( 0 )
            plot_res[label] = med_arr
            min_err = min(min_err, min(plot_res[label]))
            max_err = max(max_err, max(plot_res[label]))
        try:
            df = pd.DataFrame(plot_res, index=pretty_sets)
        except:
            continue
        fig, ax = plt.subplots(1,1, figsize=figsize)
        ax.plot(range(-2,8), [1]*10, linestyle='--', c='black',alpha=0.5)
        df.plot.bar(rot=0, figsize=figsize, ax=ax)
        ax.set_ylabel('Median Err. (Norm.)')
        ax.set_ylim(min_err - 0.2, max_err + 0.05)
        ax.legend(loc='upper left', ncol=2)
        plt.tight_layout()
        if show_image:
            plt.show()
        else:
            plt.savefig('plots/%s_boosted_calibration_methods.pdf' % arch, bbox_inches='tight')
            plt.savefig('plots/%s_boosted_calibration_methods.png' % arch, bbox_inches='tight', dpi=dpi)
            plt.clf()
            plt.close('all')
    #plt.xticks()
    #embed()


def plot_image_scale(results_dict, figsize=(6,3)):
    scales, results, all_results = get_image_scale(results_dict)
    fig = plt.figure(figsize=figsize)
    # Plot average
    for i in range(results.shape[1]):
        plt.plot(scales,results[:,i], c='tab:blue', alpha=0.3, linestyle='solid')
        if i == 0:
            plt.plot(scales,results[:,i], c='tab:blue', alpha=0.3, linestyle='-', label='Mean error (1 trial)')
    plt.plot(scales,results.mean(axis=1), c='tab:blue', marker='o', ms=3, linestyle='-', label='Average of 5 trials')
    # Plot median
    for i in range(results.shape[1]):
        plt.plot(scales,np.median(all_results[:,i], axis=1), c='tab:red', alpha=0.3, linestyle='-.')
        if i == 0:
            plt.plot(scales,np.median(all_results[:,i], axis=1), c='tab:red', alpha=0.3, linestyle='-.', label='Median error (1 trial)')
    plt.plot(scales,all_results.mean(axis=1), c='tab:red', marker='o', ms=3, linestyle='-.', label='Mean of median error of 5 trials')

    plt.xlabel('Meters per pixel')
    plt.ylabel('Error [m]')
    plt.legend()
    plt.tight_layout()
    plt.savefig('plots/image_scale_random.pdf', bbox_inches='tight')
    plt.savefig('plots/image_scale_random.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')
    print(all_results.shape)
    return all_results


def plot_image_scale_competition(results_dict, figsize=(6,2.7), show_image=False ):
    categories = ["Cross-Validation Average"]
    arches = ['unet', 'unet_notarget_linear', 'deepmtl', 'deeptx']
    pretty_arches = ['U-Net', 'U-Net+Linear', 'DeepMTL', 'DeepTx', ]
    markers = ['v','1','2','3','4']
    fig, axs = plt.subplots(1,1, figsize=figsize)
    for i, category in enumerate(categories):
        for arch, pretty_arch, marker in zip(arches, pretty_arches, markers):
            scales, results, all_results = get_image_scale(results_dict, arch, False, False, boosted="Boosted" in category)
            #for scale, res, med_res in zip(scales, results.mean(axis=1), all_results.mean(axis=1)):
            #axs[0,i].plot(scales,results.mean(axis=1), label=pretty_arch)
            axs.plot(scales, all_results.mean(axis=1), label=pretty_arch, marker=marker)
        #axs[0,i].set_xlabel('Meters per Pixel')
        #axs[0,i].set_ylabel('Mean Error [m]')
        #axs[0,i].set_yscale('log')
        #axs[0,i].set_yticks([30, 50, 100, 500])
        #axs[0,i].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        #axs[0,i].set_title(category)
        #axs[0,i].legend()
        axs.set_xlabel('Meters per Pixel')
        axs.set_ylabel('Median Error [m]')
        axs.set_yscale('log')
        axs.set_yticks([40, 60, 100, 600])
        axs.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        #axs.set_title(category)
        axs.legend()
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig('plots/image_competition_scale_random.pdf', bbox_inches='tight')
        plt.savefig('plots/image_competition_scale_random.png', bbox_inches='tight', dpi=dpi)
        plt.clf()
        plt.close('all')


def plot_image_scale_splits(results_dict, figsize=(6,3.5), show_image=False ):
    categories = ["Cross-Validation Average"]
    arch = 'unet'
    pretty_arch = 'U-Net'
    set_ordering = ['random', 'grid10', 'driving', 'non-driving', 'april', 'july']
    pretty_sets =     ['Random', 'Grid', 'Pedestrian', 'Driving', 'July', 'April']
    markers = ['v','1','2','3','4', 'x']
    fig, axs = plt.subplots(1,1, figsize=figsize)
    for i, category in enumerate(categories):
        for split, pretty_split, marker in zip(set_ordering, pretty_sets, markers):
            scales, results, all_results = get_image_scale(results_dict, arch, False, False, boosted="Boosted" in category, split=split)
            #for scale, res, med_res in zip(scales, results.mean(axis=1), all_results.mean(axis=1)):
            #axs[0,i].plot(scales,results.mean(axis=1), label=pretty_arch)
            axs.plot(scales, all_results.mean(axis=1), label=pretty_split, marker=marker)
        #axs[0,i].set_xlabel('Meters per Pixel')
        #axs[0,i].set_ylabel('Mean Error [m]')
        #axs[0,i].set_yscale('log')
        #axs[0,i].set_yticks([30, 50, 100, 500])
        #axs[0,i].get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        #axs[0,i].set_title(category)
        #axs[0,i].legend()
        axs.set_xlabel('Meters per Pixel')
        axs.set_ylabel('Median Error [m]')
        axs.set_yscale('log')
        axs.set_yticks([40, 60, 100, 600])
        axs.get_yaxis().set_major_formatter(matplotlib.ticker.ScalarFormatter())
        #axs.set_title(category)
        axs.legend()
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig('plots/image_scale_unet_splits.pdf', bbox_inches='tight')
        plt.savefig('plots/image_scale_unet_splits.png', bbox_inches='tight', dpi=dpi)
        plt.clf()
        plt.close('all')


def plot_split_image_scale_competition(results_dict, figsize=(6,4) ):
    categories = ["Image Loss", "Distance Loss", "Boosted Image Loss", "Boosted Distance Loss"]
    split_arches = [
        ['deepmtl', 'unet3', 'unet_nores', 'unet', 'unet_k5', 'unet5'],
        ['deeptx', 'deepertx', 'unet_notarget_linear', 'unet_linear'],
        ['deepmtl', 'unet3', 'unet_nores', 'unet', 'unet_k5', 'unet5'],
        ['deeptx', 'deepertx', 'unet_notarget_linear', 'unet_linear'],
    ]
    split_pretty_arches = [
        ['DeepMTL', 'U-Net3', 'U-Net4 (NoRes)', 'U-Net4', 'U-Net4 (k5)', 'U-Net5'],
        ['DeepTx', 'DeeperTx', 'U-Net+Linear', 'U-Net+Linear (Img)'],
        ['DeepMTL', 'U-Net3', 'U-Net4 (NoRes)', 'U-Net4', 'U-Net4 (k5)', 'U-Net5'],
        ['DeepTx', 'DeeperTx', 'U-Net+Linear', 'U-Net+Linear (Img)'],
    ]
    fig, axs = plt.subplots(2,4, sharex=True, sharey=True)
    for i, (category, arches, pretty_arches) in enumerate(zip(categories, split_arches, split_pretty_arches)):
        for arch, pretty_arch in zip(arches, pretty_arches):
            scales, results, all_results = get_image_scale(results_dict, arch, False, False, boosted="Boosted" in category)
            #for scale, res, med_res in zip(scales, results.mean(axis=1), all_results.mean(axis=1)):
            axs[0,i].plot(scales,results.mean(axis=1), label=pretty_arch)
            axs[1,i].plot(scales, all_results.mean(axis=1), label=pretty_arch)
        axs[0,i].set_xlabel('Meters per Pixel')
        axs[1,i].set_xlabel('Meters per Pixel')
        axs[0,i].set_ylabel('Mean Error [m]')
        axs[1,i].set_ylabel('Median Error [m]')
        axs[0,i].set_title(category)
        axs[1,i].set_title(category)
        axs[0,i].legend()
        axs[1,i].legend()
    plt.show()




def plot_tx_rx_maps(figsize=(3.5,3.5)):
    tx_data = {}
    rx_data = {}
    for split in ['grid10', 'april_combined', 'biking']:#, 'indoor', 'off_campus', '2tx']:
        params = LocConfig(data_split=split, make_val=False)
        imd = RSSLocDataset(params)
        for key in imd.data.keys():
            if key not in rx_data:
                print('Adding key:', key)
                tx_data[key] = imd.data[key].tx_vecs
                rx_data[key] = imd.data[key].rx_vecs


    ### Test divisions
    alpha = 0.8
    size = 3
    lg_size = 10
    fig = plt.figure(figsize=figsize)
    for label, key, marker in [('Grid-Train', '0.2testsizegrid10_train', '+'), ('Grid-Test', '0.2testsizegrid10_test', 'x')]:
        dat = tx_data[key]
        plt.scatter(dat[:,:,0].flatten(), dat[:,:,1].flatten(), label=label, marker=marker, alpha=alpha, s=size)
    lgnd = plt.legend()
    for handle in lgnd.legend_handles:
        handle.set_sizes([lg_size])
    plt.yticks([],[])
    plt.xticks([],[]) 
    plt.tight_layout()
    #plt.show()
    fig.savefig('plots/dissertation/map_grid_tx.pdf', bbox_inches='tight', pad_inches=0.01)
    fig.savefig('plots/dissertation/map_grid_tx.png', bbox_inches='tight', pad_inches=0.01, dpi=dpi)
    plt.clf()
    plt.close('all')
    fig = plt.figure(figsize=figsize)
    for label, key, marker in [
        ('Driving', 'driving_driving', '+'), 
        ('Pedestrian', 'walking_walking', 'x'),
        ('Cycling', 'walking_biking', '1')
    ]:
        dat = tx_data[key]
        plt.scatter(dat[:,:,0].flatten(), dat[:,:,1].flatten(), label=label, marker=marker, alpha=alpha, s=size)
    lgnd = plt.legend()
    for handle in lgnd.legend_handles:
        handle.set_sizes([lg_size])
    plt.yticks([],[])
    plt.xticks([],[]) 
    plt.tight_layout()
    #plt.show()
    fig.savefig('plots/dissertation/map_driving_tx.pdf', bbox_inches='tight', pad_inches=0.01)
    fig.savefig('plots/dissertation/map_driving_tx.png', bbox_inches='tight', pad_inches=0.01, dpi=dpi)
    plt.clf()
    plt.close('all')
    fig = plt.figure(figsize=figsize)
    for label, key, marker in [('April', 'april', '+'), ('July', 'july', 'x'), ('November', 'nov', '1')]:
        dat = tx_data[key]
        plt.scatter(dat[:,:,0].flatten(), dat[:,:,1].flatten(), label=label, marker=marker, alpha=0.8, s=size)
    lgnd = plt.legend()
    for handle in lgnd.legend_handles:
        handle.set_sizes([lg_size])
    plt.yticks([],[])
    plt.xticks([],[]) 
    plt.tight_layout()
    plt.savefig('plots/dissertation/map_seasonal_tx.pdf', bbox_inches='tight', pad_inches=0.01)
    plt.savefig('plots/dissertation/map_seasonal_tx.png', bbox_inches='tight', pad_inches=0.01, dpi=dpi)
    plt.clf()
    plt.close('all')

    ### Make random plot 
    fig = plt.figure(figsize=figsize)
    for label, key, marker in [('Random-Train', '0.2testsize_train', '+'), ('Random-Test', '0.2testsize_test', 'x')]:
        dat = tx_data[key]
        plt.scatter(dat[:,:,0].flatten(), dat[:,:,1].flatten(), label=label, marker=marker, alpha=0.8, s=size)
    lgnd = plt.legend()
    for handle in lgnd.legend_handles:
        handle.set_sizes([lg_size])
    plt.yticks([],[])
    plt.xticks([],[]) 
    plt.tight_layout()
    plt.savefig('plots/dissertation/map_random_tx.pdf', bbox_inches='tight', pad_inches=0.01)
    plt.savefig('plots/dissertation/map_random_tx.png', bbox_inches='tight', pad_inches=0.01, dpi=dpi)
    plt.clf()
    plt.close('all')
    return

    ### Make plot highlighting Test Cases
    alpha = 0.1
    fig = plt.figure(figsize=figsize)
    for label, key, marker, color in [
        ('Random-Train', '0.2testsize_train', '.', 'lightgray'), 
        ('Indoor-Tx', 'indoor', '1', 'tab:orange'), 
        ('Off-Campus', 'off_campus', '2', 'tab:green'), 
        ('2-Tx', '2tx','3', 'tab:red')]:
        dat = tx_data[key]
        plt.scatter(dat[:,:,0].flatten(), dat[:,:,1].flatten(), c=color, label=label, marker=marker)
    plt.legend()
    plt.yticks([],[])
    plt.xticks([],[]) 
    plt.tight_layout()
    plt.savefig('plots/map_highlight_special_cases.pdf', bbox_inches='tight', pad_inches=0.01)
    plt.savefig('plots/map_highlight_special_cases.png', bbox_inches='tight', pad_inches=0.01, dpi=dpi)
    plt.clf()
    plt.close('all')
    for label, key, marker, color in [
        ('Indoor-Tx', 'indoor', '1', 'tab:orange'), 
        ('Off-Campus', 'off_campus', '2', 'tab:green'), 
        ('2-Tx', '2tx','3', 'tab:red'),
        ('2-Tx', '2tx','4', 'tab:red')]:
        fig = plt.figure(figsize=figsize)
        random_dat = tx_data['0.2testsize_train']
        dat = tx_data[key]
        plt.scatter(random_dat[:,:,0].flatten(), random_dat[:,:,1].flatten(), c='lightgray', label='Random-Train', marker='.')
        if marker == '4':
            key = key + '_both'
            plt.scatter(dat[:,0,0].flatten(), dat[:,0,1].flatten(), marker=marker, c='tab:red', label='2-Tx A')
            plt.scatter(dat[:,1,0].flatten(), dat[:,1,1].flatten(), marker=str(int(marker)-1), c='tab:green', label='2-Tx B')
            #for sample in dat:
            #    plt.plot(sample[0], sample[1], c='grey', alpha=0.5)
        else:
            plt.scatter(dat[:,:,0].flatten(), dat[:,:,1].flatten(), label=label, marker=marker, c=color)
        plt.legend()
        plt.yticks([],[])
        plt.xticks([],[]) 
        plt.tight_layout()
        plt.savefig('plots/map_highlight_%s.pdf' % key, bbox_inches='tight', pad_inches=0.01)
        plt.savefig('plots/map_highlight_%s.png' % key, bbox_inches='tight', pad_inches=0.01, dpi=dpi)
        plt.clf()
        plt.close('all')





def main():
    #get_calibration_lines()
    #plot_rss_distributions(simplify_labels=True)
    #plot_calibrated_distributions(simplify_labels=True)
    #results_dict = get_all_results(filter_words=[], print_results=False)
    #plot_image_scale(results_dict)
    #plot_special_cases(results_dict)
    #plot_calibration_methods(results_dict)
    #plot_default_results(results_dict)
    plot_tx_rx_maps()
    #plot_competition(results_dict)
    #plot_llocus_comparison(results_dict)
    #plot_error_lines(results_dict)
    #plot_technique_boosting(results_dict,)


if __name__ == '__main__':
    main()
