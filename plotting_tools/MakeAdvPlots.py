import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib.patches as mpatches
import numpy as np
import pandas
import torch
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from localization import TLDL
from IPython import embed
import pickle

plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

dpi = 300

#colors = ['tab:blue', 'tab:green', 'tab:orange', 'tab:red']
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']

with open('blahomni_attack.pkl', 'rb') as infile:
    attack_dict, tmp_headers = pickle.load(infile)
with open('blahomni_attack_adversarial.pkl', 'rb') as infile:
    adversarial_dict, _ = pickle.load(infile)
with open('naive_attack2.pkl', 'rb') as infile:
    error_dict = pickle.load(infile)
    test_err = error_dict['test_err']
    naive_err = error_dict['naive'] * 60
with open('naive_attack2_adversarial.pkl', 'rb') as infile:
    adv_error_dict = pickle.load(infile)
    adv_test_err = adv_error_dict['test_err']
    adv_naive_err = adv_error_dict['naive'] * 60
with open('worst_case_attack2.pkl', 'rb') as infile:
    error_dict = pickle.load(infile)
    low_rss, high_rss = list(error_dict.keys())[1:3]
    wcel = error_dict[low_rss].max(axis=1) * 60
    wceh = error_dict[high_rss].max(axis=1) * 60
    wc_err = np.max((wcel, wceh), axis=0)
with open('worst_case_attack2_adversarial.pkl', 'rb') as infile:
    error_dict = pickle.load(infile)
    adv_wcel = error_dict[low_rss].max(axis=1) * 60
    adv_wceh = error_dict[high_rss].max(axis=1) * 60
    adv_wc_err = np.max((adv_wcel, adv_wceh), axis=0)

default_figsize = (6,4)

success_threshold = 100

headers = []
tmp_headers[0] = 'NoAttack'
for header in tmp_headers:
    header = header.replace('top100', 'All')
    header = header.replace('drop100', 'DropAll')
    header = header.replace('top', 'Top')
    header = header.replace('drop', 'Drop')
    header = header.replace('hi', 'Hi')
    header = header.replace('low', 'Lo')
    if 'Lo' not in header and ('Top' in header or ('Drop' in header and 'All' not in header)):
        header = header + '\%'
    headers.append(header)

combined_dict = {key:np.hstack((attack_dict[key], adversarial_dict[key])) for key in attack_dict}
combined_headers = headers + ['AdvTr-'+head for head in headers]

def plot_drop_over_percent_compare_adversarial(attack_dict=attack_dict, adversarial_dict=adversarial_dict, headers=headers, figsize=default_figsize, show_image=False, use_median=False):
    attack_headers=['Drop50\%','Drop40\%','Drop30\%','Drop20\%','Drop10\%', 'NoAttack']
    med_data = {key:np.median(attack_dict[key], axis=0) for key in attack_dict}
    adv_med_data = {key:np.median(adversarial_dict[key], axis=0) for key in adversarial_dict}
    success_data = {key: (attack_dict[key] >= success_threshold).sum(axis=0)/attack_dict[key].shape[0] for key in attack_dict}
    adv_success_data = {key: (adversarial_dict[key] >= success_threshold).sum(axis=0)/adversarial_dict[key].shape[0] for key in adversarial_dict}
    data = med_data if use_median else success_data
    adv_data = adv_med_data if use_median else adv_success_data
    df = pandas.DataFrame.from_dict(data, orient='index', columns=headers)
    df_adv = pandas.DataFrame.from_dict(adv_data, orient='index', columns=headers)
    y_data = df[attack_headers].iloc[0]
    adv_y_data = df_adv[attack_headers].iloc[0]
    x_data = np.arange(0.5,-0.1,-0.1)
    fig = plt.figure(figsize=figsize)
    plt.plot(x_data, y_data, label='Drop\%', marker="1")
    plt.plot(x_data, adv_y_data, label='AdvTr-Drop\%', marker="2")
    plt.xlabel('Percent of Sensors Withheld')
    if use_median:
        plt.ylabel('Median Localization Error [m]')
    else:
        plt.ylabel('Attack Success Rate')
    plt.legend()
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/drop_{"med_" if use_median else "success_"}over_percent.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/drop_{"med_" if use_median else "success_"}over_percent.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')
    
def plot_top_drop_over_percent_compare_adversarial(attack_dict=attack_dict, adversarial_dict=adversarial_dict, headers=headers, epsilon=0.2, figsize=default_figsize, show_image=False, use_median=False):
    drop_headers=['Drop50\%','Drop40\%','Drop30\%','Drop20\%','Drop10\%','NoAttack']
    top_headers=['Top50\%','Top40\%','Top30\%','Top20\%','Top10\%','NoAttack']
    med_data = {key:np.median(attack_dict[key], axis=0) for key in attack_dict}
    adv_med_data = {key:np.median(adversarial_dict[key], axis=0) for key in adversarial_dict}
    success_data = {key: (attack_dict[key] >= success_threshold).sum(axis=0)/attack_dict[key].shape[0] for key in attack_dict}
    adv_success_data = {key: (adversarial_dict[key] >= success_threshold).sum(axis=0)/adversarial_dict[key].shape[0] for key in adversarial_dict}
    data = med_data if use_median else success_data
    adv_data = adv_med_data if use_median else adv_success_data
    df = pandas.DataFrame.from_dict(data, orient='index', columns=headers)
    df_adv = pandas.DataFrame.from_dict(adv_data, orient='index', columns=headers)
    drop_data = df[drop_headers].iloc[0]
    adv_drop_data = df_adv[drop_headers].iloc[0]
    top_data = df[top_headers].loc[epsilon]
    adv_top_data = df_adv[top_headers].loc[epsilon]
    x_data = np.arange(0.5,-0.1,-0.1)
    fig = plt.figure(figsize=figsize)
    plt.plot(x_data, drop_data, label='Drop\%', marker="^", c=colors[0])
    plt.plot(x_data, adv_drop_data, label='AdvTr-Drop\%', marker="^", c=colors[0], linestyle='-.')
    plt.plot(x_data, top_data, label='Top\% ($\epsilon=0.2$)', marker="+", c=colors[1])
    plt.plot(x_data, adv_top_data, label='AdvTr-Top\% ($\epsilon=0.2$)', marker="+", c=colors[1], linestyle='-.')
    plt.xlabel('Percent of Adversarial Sensors')
    if use_median:
        plt.ylabel('Median Error [m]')
    else:
        plt.ylabel('Attack Success Rate')
    plt.legend(ncol=1, borderpad=0.3, handlelength=1.5, columnspacing=1.5)
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/small_top_drop_{"med_" if use_median else "success_"}over_percent.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/small_top_drop_{"med_" if use_median else "success_"}over_percent.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')
    
def plot_top_over_epsilon(error_dict, headers=headers, figsize=default_figsize, show_image=False, use_median=False):
    attack_headers=['All','Top50\%','Top40\%','Top30\%','Top20\%','Top10\%', 'NoAttack']
    med_data = {key:np.median(error_dict[key], axis=0) for key in error_dict}
    success_data = {key: (error_dict[key] >= success_threshold).sum(axis=0)/error_dict[key].shape[0] for key in error_dict}
    data = med_data if use_median else success_data
    df_median = pandas.DataFrame.from_dict(data, orient='index', columns=headers)
    df_median[attack_headers].plot.line(figsize=figsize, style=['1-','2-','3-','4-','<-','>-','^-'] )
    plt.xlabel('$\epsilon$: Attack Magnitude')
    if use_median:
        plt.ylabel('Median Localization Error [m]')
    else:
        plt.ylabel('Attack Success Rate')
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/top10-100_{"med_" if use_median else "success_"}over_epsilon.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/top10-100_{"med_" if use_median else "success_"}over_epsilon.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')
    
def plot_hi_low_bars(error_dict, headers=headers, epsilon=0.5, figsize=default_figsize, show_image=False, use_median=False):
    hi_low_vals = [ [1,1], [5,1], [1,5], [5,5] ]
    h1 = ['Hi%i-Lo%i' % (hi,low) for hi,low in hi_low_vals]
    h2 = ['Top1-Hi%i-Lo%i' % (hi,low) for hi,low in hi_low_vals]
    h3 = ['Drop1-Hi%i-Lo%i' % (hi,low) for hi,low in hi_low_vals]
    has_adv = any(['AdvTr' in head for head in headers])
    if has_adv:
        attack_headers = h1+h2+h3
        attack_headers += ['AdvTr-'+head for head in attack_headers]
    else:
        attack_headers = h1+h2+h3
    med_data = {key:np.median(error_dict[key], axis=0) for key in error_dict}
    success_data = {key: (error_dict[key] >= success_threshold).sum(axis=0)/error_dict[key].shape[0] for key in error_dict}
    data = med_data if use_median else success_data
    df = pandas.DataFrame.from_dict(data, orient='index', columns=headers)
    if has_adv:
        index = ['HiLo', 'Top1+HiLo', 'Drop1+HiLo',
                'AdvTr-HiLo', 'AdvTr-Top1+HiLo', 'AdvTr-Drop1+HiLo'] 
    else: 
        index =['HiLo', 'Top1+HiLo', 'Drop1+HiLo']
    hilow_df = pandas.DataFrame(
        {key:np.array(df.loc[epsilon][[head for head in attack_headers if key in head]]) for key in h1},
        index=index)
    hilow_df.T.plot.bar(rot=0, figsize=figsize) # Plot transpose because it illustrates point better
    if use_median:
        plt.ylabel('Median  Error [m]')
        plt.ylim(top=198)
    else:
        plt.ylabel('Attack Success Rate')
        plt.ylim(top=0.72)
    if has_adv:
        plt.legend(ncol=2, borderpad=0.3, handlelength=1.5, columnspacing=1, framealpha=0.3)
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/small_hilo_bar_{"med" if use_median else "success"}{"_adv" if has_adv else ""}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/small_hilo_bar_{"med" if use_median else "success"}{"_adv" if has_adv else ""}.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')
    
def plot_top_violins_at_const_epsilon(error_dict, headers=headers, epsilon=0.2, figsize=default_figsize, show_image=False):
    attack_headers=['NoAttack','Top10\%','Top20\%','Top30\%','Top40\%','Top50\%','All']
    num_violins = len(attack_headers)
    fig = plt.figure(figsize=figsize)
    data_df = pandas.DataFrame(error_dict[epsilon], columns=headers)
    ind = 0
    #for attack in attack_headers:
    plt.violinplot(data_df[attack_headers], showextrema=False, showmedians=True, widths=0.7)
    plt.plot([0.5, num_violins+.5], [success_threshold, success_threshold], linewidth=0.5, linestyle='--')
    plt.xticks(range(1,1+num_violins), attack_headers)
    plt.xlabel(f'Attack Magnitude $\epsilon={epsilon}$')
    plt.ylabel('Distribution of Localization Error [m]')
    plt.ylim(top=2000)
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig('plots/advs/top10-100_violins.pdf', bbox_inches='tight')
        plt.savefig('plots/advs/top10-100_violins.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')


def plot_top_over_epsilon_compare_adversarial(attack_dict=attack_dict, adversarial_dict=adversarial_dict, figsize=default_figsize, show_image=False, use_median=False):
    attack_headers=['All', 'Top50\%', 'Top20\%']
    pretty_labels = {
        'All': '100\%',
        'Top50\%': '50\%',
        'Top20\%': '20\%',
    }
    markers = ["^", "+", "X"]
    med_data = {key:np.median(attack_dict[key], axis=0) for key in attack_dict}
    adv_med_data = {key:np.median(adversarial_dict[key], axis=0) for key in adversarial_dict}
    success_data = {key: (attack_dict[key] >= success_threshold).sum(axis=0)/attack_dict[key].shape[0] for key in attack_dict}
    adv_success_data = {key: (adversarial_dict[key] >= success_threshold).sum(axis=0)/adversarial_dict[key].shape[0] for key in adversarial_dict}
    data = med_data if use_median else success_data
    adv_data = adv_med_data if use_median else adv_success_data
    df = pandas.DataFrame.from_dict(data, orient='index', columns=headers)
    df_adv = pandas.DataFrame.from_dict(adv_data, orient='index', columns=["AdvTr-" + header for header in headers])
    for ind, col in enumerate(attack_headers):
        ax = df[col].plot.line(figsize=figsize,c=colors[ind], label='Top-'+pretty_labels[col], marker=markers[ind])
    for ind, col in enumerate(["AdvTr-" + header for header in attack_headers]):
        df_adv[col].plot.line(ax=ax, linewidth=1.5, linestyle='-.', c=colors[ind], label='AdvTr-'+pretty_labels[col.replace('AdvTr-','')], marker=markers[ind])
    plt.xlabel('$\epsilon$: Attack Magnitude')
    if use_median:
        plt.ylabel('Median Error [m]')
    else:
        plt.ylabel('Attack Success Rate')
        plt.ylim(bottom=0.02)
    plt.legend(ncol=2, borderpad=0.2, handlelength=1.5, columnspacing=0.5, framealpha=0.3)
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/small_top_compare_{"med_" if use_median else "success_"}adversarial.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/small_top_compare_{"med_" if use_median else "success_"}adversarial.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')

def plot_wc_distribution(figsize=default_figsize, show_image=False):
    fig = plt.figure(figsize=figsize)
    plt.violinplot((test_err, wc_err, adv_wc_err), showmeans=False, showmedians=True, showextrema=False)
    plt.xticks([1,2,3], labels=["No Attack", "Worst Case", "Worst Case +" "\n" "Adv. Training"])
    plt.ylabel("Localization Error [m]")
    plt.tight_layout()

    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/wc_boxplot.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/wc_boxplot.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')

def plot_naive_wc_bars(figsize=default_figsize, show_image=False):
    plot_res = {
        "Baseline": [
            np.median(test_err),
            np.median(naive_err),
            np.median(wc_err)
        ],
        "Adv. Training": [
            np.median(adv_test_err),
            np.median(adv_naive_err),
            np.median(adv_wc_err)
        ],
    }
    order=["No Attack", "Naive", "Worst Case"]
    df = pandas.DataFrame(plot_res, index=order)
    ax = df.plot.bar(rot=0, figsize=figsize, logy=True)
    ax.legend(framealpha=0.5)
    #plot_res = {
    #    "No Attack": [
    #        np.median(test_err),
    #        np.median(adv_test_err),
    #    ],
    #    "Naive Attack": [
    #        np.median(naive_err),
    #        np.median(adv_naive_err),
    #    ],
    #    "Worst Case": [
    #        np.median(wc_err),
    #        np.median(adv_wc_err),
    #    ],
    #}
    #order=["Baseline", "Adv. Training"]
    #df = pandas.DataFrame(plot_res, index=order)
    #ax = df.plot.bar(rot=0, figsize=figsize, logy=True)
    #ax.legend(framealpha=0.5)

    plt.ylabel("Median Localization Error [m]")
    plt.tight_layout()

    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/naive_wc_med_bars.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/naive_wc_med_bars.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')
    
    

def main():
    #plot_top_over_epsilon(attack_dict)
    #plot_top_over_epsilon(attack_dict, use_median=True)
    #plot_top_violins_at_const_epsilon(attack_dict)
    #plot_hi_low_bars(attack_dict)
    #plot_hi_low_bars(attack_dict, use_median=True)
    #plot_drop_over_percent_compare_adversarial()
    #plot_drop_over_percent_compare_adversarial(use_median=True)
    #plot_top_over_epsilon_compare_adversarial()
    #plot_hi_low_bars(combined_dict, headers=combined_headers)
    #plot_top_drop_over_percent_compare_adversarial()
    #plot_top_over_epsilon_compare_adversarial(use_median=True)
    #plot_hi_low_bars(combined_dict, headers=combined_headers, use_median=True)
    #plot_top_drop_over_percent_compare_adversarial(use_median=True)
    plot_wc_distribution()


if __name__ == "__main__":
    main()
