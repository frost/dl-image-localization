import matplotlib
import numpy as np
import os
import pickle
import argparse
from IPython import embed
from localization import *
from Splot import *
from dataset import ImageDataset
import matplotlib.colors as mcolors
import pandas

plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

tab_colors = list(mcolors.TABLEAU_COLORS.keys())
#tab_colors.remove(tab_colors[4])
markers = ["+", "*", "x", "d", "1", 'v', 6, 7]
pretty_datasets = ['DS1', 'DS2', 'DS3']
linestyles = ['solid', 'dotted', 'dashed', 'dashdot']
gridsizes = ['grid2', 'grid5', 'grid10', 'grid20', 'grid50', 'random']
gridpositions = [2, 5, 10, 20, 50, 200]
gridlabels = [r'$2\times2$',r'$5\times5$',r'$10\times10$',r'$20\times20$',r'$50\times50$','Random']
attacks = ['', '_top', '_drop', '_hilo', '_hilotop', '_worst']
pretty_attacks = ['No Attack', 'Top20%', 'Drop20%', 'Hi1Lo5', 'Hi1Lo5+Top1', 'Worst Case']

# Specify params
num_epochs = 2000
arch = 'unet'
force_num_tx = 1
include_elevation_map = True

better_epoch_limit = 75

num_training_repeats = 5

device = torch.device('cuda')
with open('results/partial_basic_results.pkl', 'rb') as f:
    partial_results = pickle.load(f)

for results in [partial_results]:
    keys = list(results.keys())
    for key in keys:
        new_key = key.split('augment/')[1]
        results[new_key] = results[key]
        del results[key]

all_results = {}

noaug = (False, False, False, False, False)
dropout = (True, False, False, False, False)
noise = (False, True, False, False, False)
advtr = (False, False, False, True, False)
augs = [noaug, dropout, noise, advtr]

def main():
    global dataset_index
    global meter_scale
    global arch
    cmd_line_params = []
    parser = argparse.ArgumentParser()
    parser.add_argument("--param_selector", type=int, default=-1, help='Index of pair for selecting params')
    args = parser.parse_args()

    for random_state in range(0,num_training_repeats):
        all_results[random_state] = {}
        all_results[random_state][6] = {}
        all_results[random_state][7] = {}
        all_results[random_state][8] = {}
        for ms, di, split, min_sensors in [
            [30, 6, 'random', 15],
            [30, 6, 'grid2', 15],
            [30, 6, 'grid5', 15],
            [30, 6, 'grid10', 15],
            [30, 6, 'grid20', 15],
            [30, 6, 'grid50', 15],
            [30, 8, 'random', 4],
            [30, 8, 'grid2', 4],
            [30, 8, 'grid5', 4],
            [30, 8, 'grid10', 4],
            [30, 8, 'grid20', 4],
            [30, 8, 'grid50', 4],
            [100, 7, 'random', 5],
            [100, 7, 'grid2', 5],
            [100, 7, 'grid5', 5],
            [100, 7, 'grid10', 5],
            [100, 7, 'grid20', 5],
            [100, 7, 'grid50', 5],
        ]:
          all_results[random_state][di][split] = {}
          for loss_func, loss_label in [
            [CoMLoss(), 'com'],
          ]:
            noaug = (False, False, False, False, False)
            dropout = (True, False, False, False, False)
            noise = (False, True, False, False, False)
            advtr = (False, False, False, True, False)
            augs = [noaug, dropout, noise, advtr]
            aug_params = [ 
                [0],
                [min_sensors], #[6,10,15,20],
                [0.4], #[0.4,0.5,0.6,0.7,0.9],
                [0],
            ]
            for aug, params in zip(augs, aug_params):
                for l in params:
                    cmd_line_params.append([ms, di, split, random_state, loss_func, loss_label, min_sensors, arch, aug, l])
    
    if args.param_selector > -1:
        param_list = [cmd_line_params[args.param_selector] ]
    else:
        param_list = cmd_line_params
    
    pickle_index = 16

    for ind, param_set in enumerate(param_list):
        meter_scale, dataset_index, split, random_state, loss_func, loss_label, min_sensors, arch, aug, aug_param = param_set
        sensor_dropout, set_rss_noise, set_power_scaling, adv_train, should_interpolate = aug
        #set_rss_noise = True
        print(param_set)
        params = LocConfig(dataset_index, data_split=split, arch=arch, meter_scale=meter_scale, random_state=random_state, include_elevation_map=include_elevation_map, force_num_tx=force_num_tx, sensor_dropout=sensor_dropout, set_rss_noise=set_rss_noise, set_power_scaling=set_power_scaling, power_limit=0.4, scale_limit=aug_param, min_sensors=min_sensors, device=device, tx_marker_value=0.01, better_epoch_limit=better_epoch_limit, adv_train=adv_train, remove_mobile=False, augmentation=aug_param if should_interpolate else 0)
        params.partial_train = 10

        param_string = f"{params}_{loss_label}"
        model_ending = 'train_val.'
        pickle_filename = 'augment_%s.pkl' % param_string
        worst_filename = 'worst_augment_%s.pkl' % param_string

        all_results[random_state][dataset_index][split][tuple(aug)] = partial_results[pickle_filename]
            

def plot_error_lines(DS, grid, aug_param, vmax=500):
    for key in all_results[random_state][DS][grid][aug_param]['err'].keys():
        err = all_results[random_state][DS][grid][aug_param]['err'][key]
        truth = all_results[random_state][DS][grid][aug_param]['truth'][key].squeeze()[:,1:]
        preds = all_results[random_state][DS][grid][aug_param]['preds'][key].squeeze()[:,:2]
        if 'train_val' in key:
            plt.plot([truth[:,0], preds[:,0]], [truth[:,1], preds[:,1]], alpha=0.2, c='tab:blue', zorder=0)
            plt.scatter(preds[:,0], preds[:,1], c=err, marker='x', vmin=0, vmax=vmax, zorder=9, alpha=0.5)
            plt.scatter(truth[:,0], truth[:,1], c=err, vmin=0, vmax=vmax, zorder=10)
        elif 'train' in key:
            plt.scatter(truth[:,0], truth[:,1], s=2, alpha=0.4, c='red', zorder=1)
        else:
            plt.plot([truth[:,0], preds[:,0]], [truth[:,1], preds[:,1]], alpha=0.2, c='tab:blue', zorder=0)
            plt.scatter(preds[:,0], preds[:,1], c=err, marker='x', cmap='plasma', vmin=0, vmax=vmax, zorder=9, alpha=0.5)
            plt.scatter(truth[:,0], truth[:,1], c=err, cmap='plasma', vmin=0, vmax=vmax, zorder=10)
        print(key, truth.shape, np.median(err), '\t\t',  np.mean(err))
    plt.show()

def plot_basic_means_over_grid_all_ds(key, shadowing=False, include_adv=False, noshift=True, log_scale=False, figsize=(7,6), savefig=False):
    fig, axs = plt.subplots(nrows=3, figsize=figsize, sharex=True)
    labels = ['No Aug', 'Input Dropout', 'RSS Noise', 'AdvTr']
    augs = [noaug, dropout, noise, advtr]
    if not include_adv:
        augs = augs[:-1]
    for ds_ind, (ds, pretty_ds) in enumerate(zip([6,8,7], pretty_datasets)):
        results = {}
        for i, aug in enumerate(augs):
            results[aug] = []
            for gridsize in gridsizes:
                set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key
                means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                results[aug].append( np.array(means) )
            results[aug] = np.array(results[aug])
            shift = 0 if noshift else np.mean(results[noaug], axis=1)[:,None]
            axs[ds_ind].plot(gridpositions, np.mean(results[aug] - shift, axis=1), label=labels[i], marker=markers[i], linestyle=linestyles[i])
            if shadowing:
                axs[ds_ind].fill_between(gridpositions, np.max(results[aug] - shift, axis=1), np.min(results[aug] - shift, axis=1), alpha=0.2)
        #labels = [None, None, None, None]
        axs[ds_ind].set_xscale('log')
        if log_scale:
            axs[ds_ind].set_yscale('log')
        axs[ds_ind].set_xticks(gridpositions, labels=gridlabels, minor=False)
        axs[ds_ind].set_xticks([], minor=True)
        axs[ds_ind].set_ylabel('Mean Error [m]')
        axs[ds_ind].set_title(pretty_ds)
        axs[ds_ind].legend()
    axs[ds_ind].set_xlabel('Grid Size')
    #fig.legend(loc='outside lower center', ncol=min(4, len(augs)))
    plt.tight_layout(pad=0.1)
    #plt.subplots_adjust(bottom=0.15, hspace=0.25)
    if savefig:
        plt.savefig(f'plots/mobicom2024_plots/all_basic_partial10_ds_combined_{key}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/all_basic_partial10_ds_combined_{key}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def plot_basic_means_over_grid(ds, key, shadowing=True, include_adv=False, noshift=True, log_scale=True, figsize=(7,3), savefig=False):
    fig, ax = plt.subplots(figsize=figsize)
    results = {}
    labels = ['No Aug', 'Input Dropout', 'RSS Noise', 'AdvTr']
    augs = [noaug, dropout, noise, advtr]
    if not include_adv:
        labels = labels[:-1]
        augs = augs[:-1]
    for i, aug in enumerate(augs):
        results[aug] = []
        for gridsize in gridsizes:
            set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key
            means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
            results[aug].append( np.array(means) )
        results[aug] = np.array(results[aug])
        shift = 0 if noshift else np.mean(results[noaug], axis=1)[:,None]
        plt.plot(gridpositions, np.mean(results[aug] - shift, axis=1), label=labels[i], marker=markers[i], linestyle=linestyles[i])
        if shadowing:
            plt.fill_between(gridpositions, np.max(results[aug] - shift, axis=1), np.min(results[aug] - shift, axis=1), alpha=0.2)
    plt.xscale('log')
    if log_scale:
        plt.yscale('log')
    plt.xticks(gridpositions, labels=gridlabels, minor=False)
    plt.xticks([], minor=True)
    plt.ylabel('Mean Error [m]')
    plt.xlabel('Grid Size')
    plt.legend()
    plt.tight_layout(pad=0.1)
    if savefig:
        plt.savefig(f'plots/mobicom2024_plots/all_basic_partial10_ds{ds}_{key}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/all_basic_partial10_ds{ds}_{key}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def print_table_basic_augs_over_grid(key, pthreshold=0.05):
    from scipy.stats import ttest_ind
    results = {}
    labels = ['No Aug', 'Input Dropout', 'RSS Noise']
    augs = [noaug, dropout, noise]
    for ds in [6,8,7]:
        results[ds] = {}
        for i, aug in enumerate(augs):
            results[ds][aug] = {'mean':[], 'std':[], 'len':[]}
            for gridsize in gridsizes:
                set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key
                means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                stds = [all_results[rand][ds][gridsize][aug]['err'][set_key].std() for rand in range(5)]
                results[ds][aug]['mean'].append( np.array(means) )
                results[ds][aug]['std'].append( np.array(stds) )
                results[ds][aug]['len'].append( len(all_results[0][ds][gridsize][aug]['err'][set_key]))
            results[ds][aug]['mean'] = np.array(results[ds][aug]['mean'])
            results[ds][aug]['std'] = np.array(results[ds][aug]['std'])
            results[ds][aug]['len'] = np.array(results[ds][aug]['len'])
    print('\\begin{table}[t]')
    print('  \\centering')
    print('  \\caption{PLACE CAPTION}')
    print('  \\begin{tabular}{l|c|c|c|c|c|}')
    print('    & No Aug & \multicolumn{2}{c|}{Dropout} & \multicolumn{2}{c|}{Noise} \\\\')
    print('    Setting & Err[m] & Err[m] & p-val & Err[m] & p-val \\\\ \\hline')
    for ds, pretty_ds in zip([6,8,7], pretty_datasets):
        for i, gridsize in enumerate(gridsizes):
            line = f'    {pretty_ds} {gridlabels[i]} & '
            line += str(int(np.round(results[ds][noaug]['mean'][i].mean()))) + ' & '
            for aug, label in zip(augs, labels):
                if aug == noaug:
                    continue
                tstat, pvalue = ttest_ind(
                    results[ds][noaug]['mean'][i],
                    results[ds][aug]['mean'][i],
                    alternative='greater'
                )
                noaug_mean = results[ds][noaug]['mean'][i].mean()
                aug_mean = results[ds][aug]['mean'][i].mean()
                if noaug_mean/aug_mean > 1.05:
                    line += f'\\textbf{{{str(int(np.round(aug_mean)))}}}  & '
                else:
                    line += str(int(np.round(aug_mean))) + ' & '
                if pvalue < pthreshold:
                    line += f'\\textbf{{{str(np.round(pvalue, 2))}}} & '
                else:
                    line += str(np.round(pvalue, 2)) + ' & '
            line = line[:-2] + ' \\\\ '
            print(line)
        print('    \\hline')
    print('  \\end{tabular}')
    print('  \\label{tab:basic_means}')
    print('\\end{table}')


if __name__ == '__main__':
    main()
    #plot_hi_low_bars('random')

    #plot_basic_means_over_grid_all_ds('test', savefig=True)
    #print_table_basic_augs_over_grid('test')
    embed()

        #import rasterio 
        #outer_origin = np.array([592000, 5671000])
        #tif2 = rasterio.open('test.tif')
        #samples = imd.data[None]
        #origin = samples.origin
        #top_corner = origin + np.array([samples.rectangle_width,  samples.rectangle_height])
        #corners = np.array([origin, top_corner])
        #transform = np.array(tif2.transform).reshape(3,3)
        #transform[:2,2] -= outer_origin
        #inv_transform = np.linalg.inv(transform)
        #a,b = corners.min(axis=0), corners.max(axis=0)
        #a_ind = (inv_transform @ np.array([a[0], a[1], 1]) ).round().astype(int)
        #b_ind = (inv_transform @ np.array([b[0], b[1], 1]) ).round().astype(int)
        #data = tif2.read(1)
        #sub_img = data[b_ind[1]:a_ind[1]+1, a_ind[0]:b_ind[0]+1]
        #sub_img = np.flipud(sub_img)
        #tx_dat = imd.data[imd.train_key].ordered_dataloader.dataset.tensors[1].cpu()
        #tx_dat1 = imd.data[imd.test_keys[0]].ordered_dataloader.dataset.tensors[1].cpu()
        #plt.imshow(sub_img, origin='lower')
        #plt.scatter(150*tx_dat[:,0,1], 150*tx_dat[:,0,2], c='red')
        #plt.scatter(150*tx_dat1[:,0,1], 150*tx_dat1[:,0,2], c='red')
        #rx_dat = imd.data[imd.train_key].ordered_dataloader.dataset.tensors[0].cpu()
        #valid_rx = rx_dat[:,:,0] > 0
        #plt.scatter(150*rx_dat[valid_rx][:,1].flatten(), 150*rx_dat[valid_rx][:,2].flatten(), marker='*', c='yellow')
        #plt.show()
        ##imd.make_elevation_tensors()
        #embed()