import matplotlib
import numpy as np
import scipy
import os
import pickle
import argparse
from IPython import embed
from localization import *
from Splot import *
from dataset import RSSLocDataset
import matplotlib.colors as mcolors
import pandas

plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

tab_colors = list(mcolors.TABLEAU_COLORS.keys())
#tab_colors.remove(tab_colors[4])

noaug = (False, False, False, False, False, False)
dropout = (True, False, False, False, False, False)
noise = (False, True, False, False, False, False)
advtr = (False, False, False, True, False, False)
augs = [noaug, dropout, noise, advtr]
bs_locations = {}
with open('bs_locations.pkl', 'rb') as f:
    bs_locations = pickle.load(f)

markers = ["+", "*", "x", "d", "1", 'v', 6, 7]
pretty_datasets = ['DS1', 'DS2', 'DS3']
linestyles = ['solid', 'dotted', 'dashed', 'dashdot', (0,(1,5)),(0,(5,8)),(0,(3,5,1,5,1,5)),]
gridsizes = ['grid2', 'grid5', 'grid10', 'grid20', 'grid50', 'random']
gridpositions = [2, 5, 10, 20, 50, 200]
gridlabels = [r'$2\times2$',r'$5\times5$',r'$10\times10$',r'$20\times20$',r'$50\times50$','Random']
altgridsizes = ['grid2', 'grid5', 'grid10', 'grid20', 'grid50', 'random', 'alt1_grid2', 'alt6_grid5', 'alt9_grid10']
altgridpositions = [2, 5, 10, 20, 50, 200, 2, 5, 10]
altgridlabels = [r'$2\times2$',r'$5\times5$',r'$10\times10$',r'$20\times20$',r'$50\times50$','Random',r'$2\times2$',r'$5\times5$',r'$10\times10$']
fewgridsizes = ['grid2', 'alt1_grid2', 'grid5', 'grid10', 'random']
fewgridpositions = [2, 2, 5, 10, 100]
fewgridlabels = [r'$2\times2$',r'$2\times2$',r'$5\times5$',r'$10\times10$','Random']

attacks = ['', '_top', '_drop', '_hilo', '_hilotop', '_worst']
pretty_attacks = ['No Attack', 'Top20%', 'Drop20%', 'Hi1Lo5', 'Hi1Lo5+Top1', 'Worst Case']
synthetic_labels = ['No Aug', 'TIREM++', 'CELF', 'RBF', 'TIREM', 'TIREM FS', 'TIREM++ adaptive', 'Non-adaptive']
synthetic_augs = [noaug, 'tirem_nn', 'celf', 'rbf', 'tirem', 'tirem_fs', 'tirem_nn_muted', 'muted']

# Specify params
num_epochs = 2000
arch = 'unet'
force_num_tx = 1
include_elevation_map = True

better_epoch_limit = 75

num_training_repeats = 5

device = torch.device('cuda')
#with open('results/all_basic_results.pkl', 'rb') as f:
#    basic_results = pickle.load(f)
with open('results/all_adv_results.pkl', 'rb') as f:
    adv_results = pickle.load(f)
with open('results/partial_worst_adv_results.pkl', 'rb') as f:
    worst_results = pickle.load(f)
with open('results/worst_adv_details_cpu.pkl', 'rb') as f:
    worst_details = pickle.load(f)
with open('results/partial_basic_results.pkl', 'rb') as f:
    partial_results = pickle.load(f)
with open('results/muted_sensors_results.pkl', 'rb') as f:
    muted_results = pickle.load(f)
with open('results/celf_rbf_with_alt_results.pkl', 'rb') as f:
    celf_rbf_results = pickle.load(f)
with open('results/tiremfs_tiremnn_r2510_results.pkl', 'rb') as f:
    tirem_results = pickle.load(f)
#with open('results/ds2_grid2_results.pkl', 'rb') as f:
#    ds2_results = pickle.load(f)
with open('results/ds2_grid2_adv_results.pkl', 'rb') as f:
    ds2_results = pickle.load(f)
with open('results/ds2_grid2_worst_partial_adv_results.pkl', 'rb') as f:
    ds2_worst_partial_results = pickle.load(f)
with open('results/local_alt_results.pkl', 'rb') as f:
    noaug_alt_results = pickle.load(f)
    for key in noaug_alt_results:
        assert key not in adv_results
        adv_results[key] = noaug_alt_results[key]
with open('results/tiremfs_tiremnn_r2510_noconvert_test_results.pkl', 'rb') as f:
    noconvert_results = pickle.load(f)

for results in [adv_results, worst_results, worst_details, partial_results, muted_results, celf_rbf_results, tirem_results, noconvert_results, ds2_results, ds2_worst_partial_results]:
    keys = list(results.keys())
    for key in keys:
        if 'augment/alt/' in key:
            new_key = key.split('augment/alt/')[1]
        elif 'augment/notestaug/' in key:
            new_key = key.split('augment/notestaug/')[1]
        else:
            new_key = key.split('augment/')[1]
        results[new_key] = results[key]
        del results[key]
muted_sensors = {
    6: { 0: [2, 38, 13, 29, 26, 32, 24, 4],
        1: [24, 4, 36, 12, 39, 25, 31, 10, 34],
        2: [16, 17, 32, 15, 2, 38],
        3: [10, 34, 13, 29, 26, 35, 29],
        4: [7, 15, 23, 19, 25, 31], },
    7: { 0: [4, 9, 15],
        1: [0, 7, 9],
        2: [7, 11, 2],
        3: [12, 9, 4],
        4: [8, 3, 0], },
    8: { 0: [5],
        1: [1],
        2: [2],
        3: [3],
        4: [4], }
}

all_results = {}


def main():
    global dataset_index
    global meter_scale
    global arch
    cmd_line_params = []
    parser = argparse.ArgumentParser()
    parser.add_argument("--param_selector", type=int, default=-1, help='Index of pair for selecting params')
    args = parser.parse_args()

    for random_state in range(0,num_training_repeats):
        all_results[random_state] = {}
        all_results[random_state][6] = {}
        all_results[random_state][7] = {}
        all_results[random_state][8] = {}
        for ms, di, split, min_sensors in [
            [30, 6, 'random', 15],
            [30, 6, 'grid2', 15],
            [30, 6, 'grid5', 15],
            [30, 6, 'grid10', 15],
            [30, 6, 'grid20', 15],
            [30, 6, 'grid50', 15],
            [30, 8, 'random', 4],
            [30, 8, 'grid2', 4],
            [30, 8, 'grid5', 4],
            [30, 8, 'grid10', 4],
            [30, 8, 'grid20', 4],
            [30, 8, 'grid50', 4],
            [100, 7, 'random', 5],
            [100, 7, 'grid2', 5],
            [100, 7, 'grid5', 5],
            [100, 7, 'grid10', 5],
            [100, 7, 'grid20', 5],
            [100, 7, 'grid50', 5],
            [30, 8, 'alt1_grid2', 4],
        ]:
          all_results[random_state][di][split] = {}
          for loss_func, loss_label in [
            [CoMLoss(), 'com'],
          ]:
            noaug = (False, False, False, False, False, False)
            dropout = (True, False, False, False, False, False)
            noise = (False, True, False, False, False, False)
            advtr = (False, False, False, True, False, False)
            synthetic = (False, False, False, False, True, False)
            full_synthetic = (False, False, False, False, True, True)
            augs = [noaug, dropout, noise, advtr, synthetic] #, full_synthetic]
            aug_params = [ 
                [0, 'muted'],
                [min_sensors], #[6,10,15,20],
                [0.4], #[0.4,0.5,0.6,0.7,0.9],
                [0],
                ['tirem_nn', 'celf', 'rbf', 'tirem', 'tirem_nn_muted'],
                #['tirem_fs'],
            ]
            for aug, params in zip(augs, aug_params):
                for l in params:
                    cmd_line_params.append([ms, di, split, random_state, loss_func, loss_label, min_sensors, arch, aug, l])
    
    if args.param_selector > -1:
        param_list = [cmd_line_params[args.param_selector] ]
    else:
        param_list = cmd_line_params
    
    pickle_index = 16

    for ind, param_set in enumerate(param_list):
        meter_scale, dataset_index, split, random_state, loss_func, loss_label, min_sensors, arch, aug, aug_param = param_set
        sensor_dropout, set_rss_noise, set_power_scaling, adv_train, should_interpolate, use_full_synthetic = aug
        #set_rss_noise = True
        #print(param_set)
        aug_string = 'tirem' if aug_param == 'tirem_fs' else ('tirem_nn' if aug_param == 'tirem_nn_muted' else aug_param)
        params = LocConfig(dataset_index, data_split=split, arch=arch, meter_scale=meter_scale, random_state=random_state, include_elevation_map=include_elevation_map, one_tx=True, apply_sensor_dropout=sensor_dropout, apply_rss_noise=set_rss_noise, apply_power_scaling=set_power_scaling, power_limit=0.4, scale_limit=aug_param, min_dropout_inputs=min_sensors, device=device, adv_train=adv_train, remove_mobile=False, should_augment=should_interpolate, augmentation=aug_string if should_interpolate else 0)

        #if dataset_index not in bs_locations and split == 'random':
        #    imd = ImageDataset(params, random_state=random_state)
        #    imd.make_datasets()
        #    sensors = imd.data[imd.train_key].ordered_dataloader.dataset.tensors[0]
        #    sensors = sensors[sensors[:,:,4] > 1]
        #    sensors = sensors[sensors[:,0] > 0]
        #    bs_locations[dataset_index] = torch.unique(sensors[:,1:4],dim=0).cpu().numpy()
        param_string = f"{params}_{loss_label}" + ("_full_synthetic" if use_full_synthetic else "") + ("_muted_sensors" if (aug_param in ['muted', 'tirem_nn_muted']) else "")
        model_ending = 'train_val.'
        pickle_filename = 'augment_%s.pkl' % param_string
        worst_filename = 'worst_augment_%s.pkl' % param_string
        worst_details_filename = 'worst_details_%s.pkl' % param_string

        if (
            pickle_filename not in tirem_results and
            pickle_filename not in celf_rbf_results and
            pickle_filename not in adv_results and 
            pickle_filename not in muted_results
        ): continue
        if aug in [synthetic, full_synthetic] or aug_param == 'muted':
            if 'muted' in aug_param:
                all_results[random_state][dataset_index][split][aug_param] = muted_results[pickle_filename]
            elif 'tirem' in aug_param:
                #all_results[random_state][dataset_index][split][aug_param] = tirem_results[pickle_filename]
                all_results[random_state][dataset_index][split][aug_param] = noconvert_results[pickle_filename]
                #if dataset_index == 8 and split == 'grid2':
                #    all_results[random_state][dataset_index][split][aug_param] = ds2_results[pickle_filename.replace('Aug:0','Aug:None')]
            elif aug_param in ['celf', 'rbf']:
                all_results[random_state][dataset_index][split][aug_param] = celf_rbf_results[pickle_filename]
            continue
        all_results[random_state][dataset_index][split][tuple(aug)] = adv_results[pickle_filename]
        #if dataset_index == 8 and split == 'grid2':
        #    all_results[random_state][dataset_index][split][tuple(aug)] = ds2_results[pickle_filename.replace('Aug:0','Aug:None',)]
        if 'alt' in split:
            continue
        if aug in [noaug, advtr]:
            all_results[random_state][dataset_index][split][tuple(aug)]['worst_details'] = worst_details[worst_details_filename]
            #if dataset_index == 8 and split == 'grid2':
            #    all_results[random_state][dataset_index][split][aug_param] = ds2_worst_partial_results[worst_filename.replace('Aug:0','Aug:None')]
        worst_keys = [key for key in worst_results[worst_filename]['err'] if 'worst' in key]
        for worst_key in worst_keys:
            all_results[random_state][dataset_index][split][tuple(aug)]['err'][worst_key] = worst_results[worst_filename]['err'][worst_key]
            all_results[random_state][dataset_index][split][tuple(aug)]['preds'][worst_key] = worst_results[worst_filename]['preds'][worst_key]
            #if dataset_index == 8 and split == 'grid2':
            #    all_results[random_state][dataset_index][split][tuple(aug)]['err'][worst_key] = ds2_worst_partial_results[worst_filename.replace('Aug:0','Aug:None')]['err'][worst_key]
            #    all_results[random_state][dataset_index][split][tuple(aug)]['preds'][worst_key] = ds2_worst_partial_results[worst_filename.replace('Aug:0','Aug:None')]['preds'][worst_key]
        for worst_key in worst_keys:
            if 'worst_lo' in key:
                continue
            hikey, lokey = worst_key, worst_key.replace('_hi', '_lo')
            all_worst_key = worst_key.replace('_hi','')
            worst_err = np.array([worst_results[worst_filename]['err'][hikey], worst_results[worst_filename]['err'][lokey]]).max(axis=0)
            #if dataset_index == 8 and split == 'grid2':
            #    worst_err = np.array([ds2_worst_partial_results[worst_filename.replace('Aug:0','Aug:None')]['err'][hikey], ds2_worst_partial_results[worst_filename.replace('Aug:0','Aug:None')]['err'][lokey]]).max(axis=0)
            all_results[random_state][dataset_index][split][tuple(aug)]['err'][all_worst_key] = worst_err
            
def get_worst_stats():
    res = plot_adv_means_over_grid_all_ds(attacks=['worst'])
    print('Error increase of worst-case attacks, noaug', ((res[0::4] - res[2::4]) / res[0::4]).mean(axis=1))
    print('Error increase of worst-case attacks, advtr', ((res[1::4] - res[3::4]) / res[1::4]).mean(axis=1))
    print('Error increase from advtr for worst-case attacks', ((res[2::4] - res[3::4]) / res[2::4]).mean(axis=1))

def plot_adv_means_over_grid_all_ds(
    key='test',
    #attacks=['top', 'drop', 'hilo', 'hilotop', 'worst'],
    attacks=['top', 'drop'],
    augs=[noaug, advtr],
    shadowing=False, log_scale=False, figsize=(7,6.5), savefig=False
):
    fig, axs = plt.subplots(nrows=3, figsize=figsize, sharex=True)
    labels = {
        '': 'No Attack',
        'top': 'Top20%',
        'drop': 'Drop20%',
        'hilo': 'HiLo',
        'hilotop': 'HiLo+Top1',
        'worst': 'Worst Case',
    }
    all_means = []
    for ds_ind, (ds, pretty_ds) in enumerate(zip([6,8,7], pretty_datasets)):
        results = {}
        for attack in ['']+attacks:
            attack_ind = list(labels.keys()).index(attack)
            results[attack] = {}
            for i, aug in enumerate(augs):
                label_prefix = '' if aug == noaug else 'AdvTr-'
                if ds_ind > 0:
                    label_prefix = '_'
                results[attack][aug] = []
                for gridsize in gridsizes:
                    set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key + ('_' if len(attack) else '') + attack
                    means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                    results[attack][aug].append( np.array(means) )
                results[attack][aug] = np.array(results[attack][aug])
                axs[ds_ind].plot(gridpositions, np.mean(results[attack][aug] , axis=1), c=tab_colors[attack_ind], label=label_prefix+labels[attack], marker=markers[attack_ind], linestyle=linestyles[i])
                all_means.append(np.mean(results[attack][aug], axis=1))
                print(pretty_ds, label_prefix, labels[attack], all_means[-1])
                if shadowing:
                    axs[ds_ind].fill_between(gridpositions, np.max(results[attack][aug] , axis=1), np.min(results[attack][aug], axis=1), color=tab_colors[attack_ind], alpha=0.2)
        axs[ds_ind].set_xscale('log')
        if log_scale:
            axs[ds_ind].set_yscale('log')
        axs[ds_ind].set_xticks(gridpositions, labels=gridlabels, minor=False)
        axs[ds_ind].set_xticks([], minor=True)
        axs[ds_ind].set_ylabel('Mean Error [m]')
        axs[ds_ind].set_title(pretty_ds)
        #axs[ds_ind].legend()

    all_means = np.array(all_means)
    advtr_diff = (all_means[1::2] - all_means[::2]) / all_means[::2]
    for i, attack in enumerate(['noattack'] + attacks):
        advtr_diff_mean = advtr_diff[i::len(attacks)+1].mean()
        print('Avg increase in error for adv tr', attack,  advtr_diff_mean)
    if 'worst' in attacks:
        advtr_diff_mean = advtr_diff[i::len(attacks)+1].mean()
        print('Avg increase in error for adv tr', attack,  advtr_diff_mean)

    axs[ds_ind].set_xlabel('Grid Size')
    fig.legend(loc='outside lower center', ncol=min(4, len(attacks)+1))
    plt.tight_layout(pad=0.1)
    plt.subplots_adjust(bottom=0.17)
    if savefig:
        plt.savefig(f'plots/mobicom2024_plots/adv_ds_combined_{key}_{"_".join(attacks)}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/adv_ds_combined_{key}_{"_".join(attacks)}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()
    return all_means

def plot_basic_means_over_grid_all_ds(key, shadowing=False, include_adv=False, noshift=True, log_scale=False, figsize=(7,6), savefig=False):
    fig, axs = plt.subplots(nrows=3, figsize=figsize, sharex=True)
    labels = ['No Aug', 'Input Dropout', 'RSS Noise', 'AdvTr']
    augs = [noaug, dropout, noise, advtr]
    if not include_adv:
        augs = augs[:-1]
    for ds_ind, (ds, pretty_ds) in enumerate(zip([6,8,7], pretty_datasets)):
        results = {}
        for i, aug in enumerate(augs):
            results[aug] = []
            for gridsize in gridsizes:
                set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key
                means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                results[aug].append( np.array(means) )
            results[aug] = np.array(results[aug])
            shift = 0 if noshift else np.mean(results[noaug], axis=1)[:,None]
            axs[ds_ind].plot(gridpositions, np.mean(results[aug] - shift, axis=1), label=labels[i], marker=markers[i], linestyle=linestyles[i])
            if shadowing:
                axs[ds_ind].fill_between(gridpositions, np.max(results[aug] - shift, axis=1), np.min(results[aug] - shift, axis=1), alpha=0.2)
        #labels = [None, None, None, None]
        axs[ds_ind].set_xscale('log')
        if log_scale:
            axs[ds_ind].set_yscale('log')
        axs[ds_ind].set_xticks(gridpositions, labels=gridlabels, minor=False)
        axs[ds_ind].set_xticks([], minor=True)
        axs[ds_ind].set_ylabel('Mean Error [m]')
        axs[ds_ind].set_title(pretty_ds)
        axs[ds_ind].legend()
    axs[ds_ind].set_xlabel('Grid Size')
    #fig.legend(loc='outside lower center', ncol=min(4, len(augs)))
    plt.tight_layout(pad=0.1)
    #plt.subplots_adjust(bottom=0.15, hspace=0.25)
    if savefig:
        plt.savefig(f'plots/mobicom2024_plots/all_basic_ds_combined_{key}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/all_basic_ds_combined_{key}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def plot_basic_means_over_grid(ds, key, shadowing=True, include_adv=False, noshift=True, log_scale=True, figsize=(7,3), savefig=False):
    fig, ax = plt.subplots(figsize=figsize)
    results = {}
    labels = ['No Aug', 'Input Dropout', 'RSS Noise', 'AdvTr']
    augs = [noaug, dropout, noise, advtr]
    if not include_adv:
        labels = labels[:-1]
        augs = augs[:-1]
    for i, aug in enumerate(augs):
        results[aug] = []
        for gridsize in gridsizes:
            set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key
            means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
            results[aug].append( np.array(means) )
        results[aug] = np.array(results[aug])
        shift = 0 if noshift else np.mean(results[noaug], axis=1)[:,None]
        plt.plot(gridpositions, np.mean(results[aug] - shift, axis=1), label=labels[i], marker=markers[i], linestyle=linestyles[i])
        if shadowing:
            plt.fill_between(gridpositions, np.max(results[aug] - shift, axis=1), np.min(results[aug] - shift, axis=1), alpha=0.2)
    plt.xscale('log')
    if log_scale:
        plt.yscale('log')
    plt.xticks(gridpositions, labels=gridlabels, minor=False)
    plt.xticks([], minor=True)
    plt.ylabel('Mean Error [m]')
    plt.xlabel('Grid Size')
    plt.legend()
    plt.tight_layout(pad=0.1)
    if savefig:
        plt.savefig(f'plots/mobicom2024_plots/all_basic_ds{ds}_{key}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/all_basic_ds{ds}_{key}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def print_table_basic_augs_over_grid(key, pthreshold=0.05):
    from scipy.stats import ttest_ind
    results = {}
    labels = ['No Aug', 'Input Dropout', 'RSS Noise', 'Adv. Train']
    augs = [noaug, dropout, noise, advtr]
    for ds in [6,8,7]:
        results[ds] = {}
        for i, aug in enumerate(augs):
            results[ds][aug] = {'mean':[], 'std':[], 'len':[]}
            for gridsize in gridsizes:
                set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key
                means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                stds = [all_results[rand][ds][gridsize][aug]['err'][set_key].std() for rand in range(5)]
                results[ds][aug]['mean'].append( np.array(means) )
                results[ds][aug]['std'].append( np.array(stds) )
                results[ds][aug]['len'].append( len(all_results[0][ds][gridsize][aug]['err'][set_key]))
            results[ds][aug]['mean'] = np.array(results[ds][aug]['mean'])
            results[ds][aug]['std'] = np.array(results[ds][aug]['std'])
            results[ds][aug]['len'] = np.array(results[ds][aug]['len'])
    print('\\begin{table}[t]')
    print('  \\centering')
    print('  \\caption{PLACE CAPTION}')
    print('  \\begin{tabular}{l|c|c|c|c|c|}')
    print('    & No Aug & \multicolumn{2}{c|}{Dropout} & \multicolumn{2}{c|}{Noise} \\\\')
    print('    Setting & Err[m] & Err[m] & p-val & Err[m] & p-val \\\\ \\hline')
    for ds, pretty_ds in zip([6,8,7], pretty_datasets):
        for i, gridsize in enumerate(gridsizes):
            line = f'    {pretty_ds} {gridlabels[i]} & '
            line += str(int(np.round(results[ds][noaug]['mean'][i].mean()))) + ' & '
            for aug, label in zip(augs, labels):
                if aug == noaug:
                    continue
                tstat, pvalue = ttest_ind(
                    results[ds][noaug]['mean'][i],
                    results[ds][aug]['mean'][i],
                    alternative='greater'
                )
                noaug_mean = results[ds][noaug]['mean'][i].mean()
                aug_mean = results[ds][aug]['mean'][i].mean()
                if noaug_mean/aug_mean > 1.05:
                    line += f'\\textbf{{{str(int(np.round(aug_mean)))}}}  & '
                else:
                    line += str(int(np.round(aug_mean))) + ' & '
                if pvalue < pthreshold:
                    line += f'\\textbf{{{str(np.round(pvalue, 2))}}} & '
                else:
                    line += str(np.round(pvalue, 2)) + ' & '
            line = line[:-2] + ' \\\\ '
            print(line)
        print('    \\hline')
    print('  \\end{tabular}')
    print('  \\label{tab:basic_means}')
    print('\\end{table}')


def plot_hi_low_bars(gridsize, key='test', figsize=(7,3), savefig=False):
    attack_headers = []
    for head in pretty_attacks:
        attack_headers.append(head)
        attack_headers.append('AdvTr-'+head)
    
    results = {}
    flat_results = {}
    labels = ['No Aug', 'AdvTrain']
    augs = [noaug, advtr]
    for ds, pretty_ds in zip([6,8,7], pretty_datasets):
        results[ds] = {}
        header_index = 0
        for attack_ind, attack in enumerate(attacks):
            for i, aug in enumerate(augs):
                if attack == attacks[0]:
                    results[ds][aug] = {}
                    flat_results[f'{pretty_ds}\n{labels[i]}'] = {}
                set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + key + attack
                means = np.array([all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)])
                results[ds][aug][attack] = means.mean()
                flat_results[f'{pretty_ds}\n{labels[i]}'][pretty_attacks[attack_ind]] = means.mean()
                header_index += 1

    df = pandas.DataFrame.from_dict(flat_results)
    med_data = {key:np.median(error_dict[key], axis=0) for key in error_dict}
    success_data = {key: (error_dict[key] >= success_threshold).sum(axis=0)/error_dict[key].shape[0] for key in error_dict}
    data = med_data if use_median else success_data
    df = pandas.DataFrame.from_dict(data, orient='index', columns=pretty_datasets)
    if has_adv:
        index = ['HiLo', 'Top1+HiLo', 'Drop1+HiLo',
                'AdvTr-HiLo', 'AdvTr-Top1+HiLo', 'AdvTr-Drop1+HiLo'] 
    else: 
        index =['HiLo', 'Top1+HiLo', 'Drop1+HiLo']
    hilow_df = pandas.DataFrame(
        {key:np.array(df.loc[epsilon][[head for head in attack_headers if key in head]]) for key in h1},
        index=index)
    hilow_df.T.plot.bar(rot=0, figsize=figsize) # Plot transpose because it illustrates point better
    if use_median:
        plt.ylabel('Median  Error [m]')
        plt.ylim(top=198)
    else:
        plt.ylabel('Attack Success Rate')
        plt.ylim(top=0.72)
    if has_adv:
        plt.legend(ncol=2, borderpad=0.3, handlelength=1.5, columnspacing=1, framealpha=0.3)
    plt.tight_layout()
    if show_image:
        plt.show()
    else:
        plt.savefig(f'plots/advs/small_hilo_bar_{"med" if use_median else "success"}{"_adv" if has_adv else ""}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/advs/small_hilo_bar_{"med" if use_median else "success"}{"_adv" if has_adv else ""}.png', bbox_inches='tight', dpi=dpi)
    plt.clf()
    plt.close('all')


def partial_worst_case_analysis(ds, gridsize, test=True, plot_scatters=False):
    key = 'test' if test else 'train_val'
    meter_scale = 30 if ds in [6,8] else 100
    results = {
        'preds': {},
        'attacks': {},
        'err': {},
        'adv_tr_preds': {},
        'adv_tr_attacks': {},
        'adv_tr_err': {},
        'truth': {},
        'noattackerr': {},
        'adv_tr_noattackerr': {},
    }
    for rand_state in range(5):
        grid_str = gridsize if 'grid' in gridsize else ''
        rand_res = all_results[rand_state][ds][gridsize][noaug]['worst_details']['worst_attack'][f'0.2testsize{grid_str}_{key}'][1]
        preds = np.array([[rand_res[lohi_ind][i][0].numpy() for i in range(32)] for lohi_ind in range(2)])
        attacks = np.array([[rand_res[lohi_ind][i][1].numpy() for i in range(32)] for lohi_ind in range(2)])
        err = np.array([[rand_res[lohi_ind][i][2].numpy() for i in range(32)] for lohi_ind in range(2)])
        adv_rand_res = all_results[rand_state][ds][gridsize][advtr]['worst_details']['worst_attack'][f'0.2testsize{grid_str}_{key}'][1]
        adv_preds = np.array([[adv_rand_res[lohi_ind][i][0].numpy() for i in range(32)] for lohi_ind in range(2)])
        adv_attacks = np.array([[adv_rand_res[lohi_ind][i][1].numpy() for i in range(32)] for lohi_ind in range(2)])
        adv_err = np.array([[adv_rand_res[lohi_ind][i][2].numpy() for i in range(32)] for lohi_ind in range(2)])
        rss, counts = np.unique(attacks[:,:,:,:,0], return_counts=True)
        results['preds'][rand_state] = preds
        results['attacks'][rand_state] = attacks
        results['err'][rand_state] = err
        results['adv_tr_preds'][rand_state] = adv_preds
        results['adv_tr_attacks'][rand_state] = adv_attacks
        results['adv_tr_err'][rand_state] = adv_err
        results['truth'][rand_state] = all_results[rand_state][ds][gridsize][noaug]['truth'][f'0.2testsize{grid_str}_{key}'][:32]
        results['noattackerr'][rand_state] = all_results[rand_state][ds][gridsize][noaug]['err'][f'0.2testsize{grid_str}_{key}'][:32]
        results['adv_tr_noattackerr'][rand_state] = all_results[rand_state][ds][gridsize][advtr]['err'][f'0.2testsize{grid_str}_{key}'][:32]
    
    all_err = np.zeros((2, 2, 32, 5))
    attack_overwritten = np.zeros((2, 2, 32, 5), dtype=bool)
    attack_distance = np.zeros((2, 2, 32, 5))
    attack_nearby = np.zeros((2, 2, 32, 5), dtype=bool)
    err_diffs = np.zeros((2, 2, 32, 5))
    truth_to_attack_distance = np.zeros((2, 2, 32, 5))
    for j in range(2):
        adv_prefix = '' if j == 0 else 'adv_tr_'
        for i in range(32):
            if plot_scatters:
                fig, axs = plt.subplots(2,5, figsize=(18,10), sharex=True, sharey=True)
            for rand in range(5):
                preds = results[adv_prefix+'preds'][rand]
                attacks = results[adv_prefix+'attacks'][rand]
                err = results[adv_prefix+'err'][rand] * meter_scale
                truths = results['truth'][rand]
                noattackerr = results[adv_prefix+'noattackerr'][rand]
                for lohi_ind in range(2):
                    attack_ind = err[lohi_ind,i].argmax()
                    attack = attacks[lohi_ind,i,attack_ind]
                    attack = attack[attack[:,0] != 0]
                    attack_rss = attack[-1,0]
                    single_attack = attack[-1:]
                    other_attack = attack[:-1]
                    pred = preds[lohi_ind,i,attack_ind]
                    truth = truths[i,0,1:]
                    noer = noattackerr[i]
                    er = err[lohi_ind,i,attack_ind]

                    all_err[j,lohi_ind,i,rand] = er
                    nearby_dist = np.linalg.norm(single_attack[:,1:3].round() - other_attack[:,1:3].round(), axis=1, ord=np.inf)
                    attack_overwritten[j, lohi_ind, i, rand] = any(nearby_dist < 1)
                    attack_distance[j, lohi_ind, i, rand] = nearby_dist.min()
                    attack_nearby[j, lohi_ind, i, rand] = any((nearby_dist > 1) * (nearby_dist <= 2))
                    truth_to_attack_distance[j, lohi_ind, i, rand] = np.linalg.norm(single_attack[0,1:3]*meter_scale - truth)
                    err_diffs[j, lohi_ind, i, rand] = er - noer
                    if plot_scatters:
                        cmap = axs[lohi_ind][rand].scatter(attack[:,1]*meter_scale, attack[:,2]*meter_scale, c=attack[:,0], marker='1')
                        vmin, vmax = cmap.get_clim()
                        axs[lohi_ind][rand].scatter(single_attack[:,1]*meter_scale, single_attack[:,2]*meter_scale, c=single_attack[:,0], marker='+', vmin=vmin, vmax=vmax)
                        axs[lohi_ind][rand].scatter(pred[0]*meter_scale, pred[1]*meter_scale, c='red', marker='x')
                        axs[lohi_ind][rand].scatter(truth[0], truth[1], c='green', marker='X')
                        axs[lohi_ind][rand].set_title('%.1f  (%.1f)' % (er, noer))
            #err_diffs = np.array(err_diffs).reshape(2,5)
            #print(np.mean(err_diffs, axis=1), err_diffs)
            if plot_scatters:
                plt.show()
    
    print('Avg Err of Lo Overwriting attacks before Adv Tr:', all_err[0,0][attack_overwritten[0,0]].mean())
    print('Avg Err of Hi Overwriting attacks before Adv Tr:', all_err[0,1][attack_overwritten[0,1]].mean())
    print('Avg Err of Lo Overwriting attacks after Adv Tr:', all_err[1,0][attack_overwritten[1,0]].mean())
    print('Avg Err of Hi Overwriting attacks after Adv Tr:', all_err[1,1][attack_overwritten[1,1]].mean())
    print('Percentage of Lo/Hi Overwriting attacks before Adv Tr:', attack_overwritten.reshape(2,2,-1).mean(axis=2)[0])
    print('Percentage of Lo/Hi Overwriting attacks after Adv Tr:', attack_overwritten.reshape(2,2,-1).mean(axis=2)[1])
    idx_n = all_err[0].argmax(axis=0)
    I_n, J_n = np.ogrid[:32,:5]
    noaug_max_indexing = idx_n, I_n, J_n
    idx = all_err[1].argmax(axis=0)
    I, J = np.ogrid[:32,:5]
    advtr_max_indexing = idx, I, J
    print('Avg Err of Overall Worst Overwriting attacks before Adv Tr:', all_err[0][idx_n, I_n, J_n].mean())
    print('Avg Err of Overall Worst Overwriting attacks after Adv Tr:', all_err[1][idx, I, J].mean())
    print('Percentage of Overall Worst Overwriting attacks before Adv Tr:', attack_overwritten[0][idx_n, I_n, J_n].mean())
    print('Percentage of Overall Worst Overwriting attacks after Adv Tr:', attack_overwritten[1][idx, I, J].mean())
    print('Percentage of Overall Worst nearby attacks before Adv Tr:', attack_nearby[0][idx_n, I_n, J_n].mean())
    print('Percentage of Overall Worst nearby attacks after Adv Tr:', attack_nearby[1][idx, I, J].mean())
    print('Percentage of Overall Worst attacks using Hi before AdvTr:', idx_n.mean())
    print('Percentage of Overall Worst attacks using Hi after AdvTr:', idx.mean())
    return results, all_err, attack_overwritten, attack_nearby, err_diffs, truth_to_attack_distance, noaug_max_indexing, advtr_max_indexing, attack_distance

def worst_case_analysis(datasets=[6,7,8], grids=gridsizes, savefig=False, figsize=(8,7)):
    complete_results = np.zeros((7,len(datasets),len(grids)))
    distances = np.zeros((2,160,len(datasets),len(grids)))
    truth_to_attack_distances = np.zeros((2,160,len(datasets),len(grids)))
    for i, ds in enumerate(datasets):
        for j, grid in enumerate(grids):
            res = partial_worst_case_analysis(ds, grid)
            results, all_err, attack_overwritten, attack_nearby, err_diffs, truth_to_attack_distance, noaug_max_indexing, advtr_max_indexing, attack_distance = res
            idx_n, I_n, J_n = noaug_max_indexing
            idx, I, J = advtr_max_indexing
            complete_results[0,i,j] = attack_overwritten[0][idx_n, I_n, J_n].mean()
            complete_results[1,i,j] = attack_overwritten[1][idx, I, J].mean()
            complete_results[2,i,j] = attack_nearby[0][idx_n, I_n, J_n].mean()
            complete_results[3,i,j] = attack_nearby[1][idx, I, J].mean()
            complete_results[4,i,j] = idx_n.mean()
            complete_results[5,i,j] = idx.mean()
            distances[0,:,i,j] = attack_distance[0][idx_n, I_n, J_n].flatten()
            distances[1,:,i,j] = attack_distance[1][idx, I, J].flatten()
            truth_to_attack_distances[0,:,i,j] = truth_to_attack_distance[0][idx_n, I_n, J_n].flatten()
            truth_to_attack_distances[1,:,i,j] = truth_to_attack_distance[1][idx, I, J].flatten()
    avg_results = complete_results.reshape(6,-1).mean(axis=1)
    print('Percentage of Overall Worst Overwriting attacks before Adv Tr:', avg_results[0])
    print('Percentage of Overall Worst Overwriting attacks after Adv Tr:', avg_results[1])
    print('Percentage of Overall Worst nearby attacks before Adv Tr:', avg_results[2])
    print('Percentage of Overall Worst nearby attacks after Adv Tr:', avg_results[3])
    print('Percentage of Overall Worst attacks using Hi before AdvTr:', avg_results[4])
    print('Percentage of Overall Worst attacks using Hi after AdvTr:', avg_results[5])
    fig, axs = plt.subplots(nrows=3,ncols=2, sharey='row', figsize=figsize)
    for aug in [0,1]:
      aug_string = 'Original Model' if aug == 0 else 'Adv. Training'
      for i, ds in enumerate([0,2,1]):
        r1 = axs[i,aug].hist(distances[aug,:,ds,:].flatten()*(30 if ds in [0,2] else 100), bins=30, hatch='///', fill=True,label=('_' if ds + aug > 0 else '') + 'Low RSS', linewidth=2)
        r2 = axs[i,aug].hist(distances[aug,:,ds,:].flatten()*(30 if ds in [0,2] else 100),weights=complete_results[4+aug,ds].repeat(160), bins=30,stacked=True, hatch='o',label=('_' if ds + aug > 0 else '') + 'High RSS',linewidth=2)
        if i == 0:
            axs[i,aug].set_title(f'{aug_string}\n {pretty_datasets[i]}')
        else:
            axs[i,aug].set_title(pretty_datasets[i])
    fig.supxlabel('Distance from attack pixel to nearest input [m]', y=0.047)
    fig.supylabel('Count')
    plt.figlegend(ncol=2, loc='lower center', bbox_to_anchor=(0.5,-0.005))
    plt.tight_layout()
    #plt.subplots_adjust(bottom=0.09, left=0.12)
    if savefig:
        plt.show()
        fig.savefig(f'plots/dissertation/worst_case_attack_distance.pdf', bbox_inches='tight')
        fig.savefig(f'plots/dissertation/worst_case_attack_distance.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()
    embed()
    return complete_results


def worst_case_bar_plot():
    pass

def plot_synthetic_means_over_grid(ds, key, grids=fewgridsizes.copy(), shadowing=False, log_scale=False, figsize=(6,3), savefig=False):
    fig = plt.figure(figsize=figsize)
    labels = ['No Aug', 'TIREM++', 'CELF', 'RBF', 'TIREM FS']
    augs = [noaug, 'tirem_nn', 'celf', 'rbf', 'tirem_fs']
    labels = ['No Aug', 'RBF', 'CELF', 'TIREM++', 'TIREM FS']
    augs = [noaug, 'celf', 'rbf', 'tirem_nn', 'tirem_fs']
    tmp_gridsizes = grids.copy()
    tmp_gridsizes.remove('alt1_grid2')
    tmp_gridlabels = [altgridlabels[altgridsizes.index(grid)] for grid in tmp_gridsizes]
    tmp_gridpositions = [altgridpositions[altgridsizes.index(grid)] for grid in tmp_gridsizes]
    pretty_ds = pretty_datasets[[6,8,7].index(ds)]
    results = {}
    for i, aug in enumerate(augs):
        results[aug] = []
        for gridsize in tmp_gridsizes:
            if 'alt' in gridsize: continue
            set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + (key)# if aug == noaug else key+'_synthetic')
            if gridsize == 'grid2' and ds == 8:
                means = [all_results[rand][ds]['alt1_grid2'][aug]['err'][set_key].mean() for rand in range(5)]
            else:
                means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
            results[aug].append( np.array(means) )
        results[aug] = np.array(results[aug])
        plt.plot(tmp_gridpositions, np.mean(results[aug], axis=1), label=labels[i], marker=markers[i], linestyle=linestyles[i])
        if shadowing:
            plt.fill_between(tmp_gridpositions, np.max(results[aug], axis=1), np.min(results[aug], axis=1), alpha=0.2)
    labels = [None, None, None, None,None]
    plt.xscale('log')
    if log_scale:
        plt.yscale('log')
    plt.xticks(tmp_gridpositions, labels=tmp_gridlabels, minor=False)
    plt.xticks([], minor=True)
    #axs[ds_ind].set_ylabel('Mean Error [m]')
    plt.title(pretty_ds)
    #axs[ds_ind].legend()
    plt.xlabel('Grid Size')
    plt.ylabel('Mean Error [m]')
    plt.legend(ncol=2)
    plt.tight_layout(pad=0.1)
    #plt.subplots_adjust(bottom=0.15, hspace=0.25)
    if savefig:
        plt.show()
        fig.savefig(f'plots/mobicom2024_plots/synthetic_means_over_grid_{key}_ds{ds}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/synthetic_means_over_grid_{key}_ds{ds}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def plot_synthetic_means_over_grid_all_ds(key, grids=fewgridsizes, shadowing=False, log_scale=False, figsize=(6,6), savefig=False):
    fig, axs = plt.subplots(nrows=3, figsize=figsize, sharex=True)
    labels = ['No Aug', 'TIREM++', 'CELF', 'RBF', 'TIREM FS']
    augs = [noaug, 'tirem_nn', 'celf', 'rbf', 'tirem_fs']
    grids.remove('alt1_grid2')
    tmp_gridsizes = grids
    tmp_gridlabels = [altgridlabels[altgridsizes.index(grid)] for grid in grids]
    tmp_gridpositions = [altgridpositions[altgridsizes.index(grid)] for grid in grids]
    for ds_ind, (ds, pretty_ds) in enumerate(zip([6,8,7], pretty_datasets)):
        results = {}
        for i, aug in enumerate(augs):
            results[aug] = []
            for gridsize in tmp_gridsizes:
                if 'alt' in gridsize: continue
                set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + (key)# if aug == noaug else key+'_synthetic')
                if gridsize == 'grid2' and ds == 8:
                    means = [all_results[rand][ds]['alt1_grid2'][aug]['err'][set_key].mean() for rand in range(5)]
                else:
                    means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                results[aug].append( np.array(means) )
            results[aug] = np.array(results[aug])
            axs[ds_ind].plot(tmp_gridpositions, np.mean(results[aug], axis=1), label=labels[i], marker=markers[i], linestyle=linestyles[i])
            if shadowing:
                axs[ds_ind].fill_between(tmp_gridpositions, np.max(results[aug], axis=1), np.min(results[aug], axis=1), alpha=0.2)
        labels = [None, None, None, None,None]
        axs[ds_ind].set_xscale('log')
        if log_scale:
            axs[ds_ind].set_yscale('log')
        axs[ds_ind].set_xticks(tmp_gridpositions, labels=tmp_gridlabels, minor=False)
        axs[ds_ind].set_xticks([], minor=True)
        #axs[ds_ind].set_ylabel('Mean Error [m]')
        axs[ds_ind].set_title(pretty_ds)
        #axs[ds_ind].legend()
    axs[ds_ind].set_xlabel('Grid Size')
    fig.supylabel('Mean Error [m]')
    fig.legend(loc='outside lower center', ncol=len(augs))
    plt.tight_layout(pad=0.1)
    #plt.subplots_adjust(bottom=0.15, hspace=0.25)
    if savefig:
        plt.show()
        fig.savefig(f'plots/mobicom2024_plots/synthetic_means_over_grid_{key}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/synthetic_means_over_grid_{key}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()


def print_table_synthetic_augs_over_grid(key, pthreshold=0.05, grids=gridsizes):
    from scipy.stats import ttest_ind
    results = {}
    labels = ['No Aug', 'TIREM++', 'CELF', 'RBF', 'TIREM Synth']
    augs = [noaug, 'tirem_nn', 'celf', 'rbf', 'tirem_fs']
    few_gridlabels = [altgridlabels[altgridsizes.index(grid)] for grid in grids]
    for ds in [6,8,7]:
        results[ds] = {}
        for i, aug in enumerate(augs):
            results[ds][aug] = {'mean':[], 'std':[], 'len':[]}
            for gridsize in grids:
                gridsize_without_alt = gridsize[5:] if 'alt' in gridsize else (gridsize if 'grid' in gridsize else '')
                set_key = '0.2testsize' + gridsize_without_alt + '_' + (key)# if aug == noaug else key+'_synthetic')
                if gridsize not in all_results[0][ds]:
                    results[ds][aug]['mean'].append(np.zeros_like(results[ds][aug]['mean'][-1]))
                    continue
                try:
                    means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                    stds = [all_results[rand][ds][gridsize][aug]['err'][set_key].std() for rand in range(5)]
                except:
                    set_key += '_synthetic'
                    means = [all_results[rand][ds][gridsize][aug]['err'][set_key].mean() for rand in range(5)]
                    stds = [all_results[rand][ds][gridsize][aug]['err'][set_key].std() for rand in range(5)]
                results[ds][aug]['mean'].append( np.array(means) )
                results[ds][aug]['std'].append( np.array(stds) )
                results[ds][aug]['len'].append( len(all_results[0][ds][gridsize][aug]['err'][set_key]))
            results[ds][aug]['mean'] = np.array(results[ds][aug]['mean'])
            results[ds][aug]['std'] = np.array(results[ds][aug]['std'])
            results[ds][aug]['len'] = np.array(results[ds][aug]['len'])
    print('\\begin{table}[t]')
    print('  \\centering')
    print('  \\caption{PLACE CAPTION}')
    print('  \\begin{tabular}{l|c|c|c|c|c|c|c|}')
    print('    & No Aug & \multicolumn{2}{c|}{TIREM++} & \multicolumn{2}{c|}{CELF} & \multicolumn{2}{c|}{RBF} \\\\')
    print('    Setting & Err[m] & Err[m] & p-val & Err[m] & p-val & Err[m] & p-val \\\\ \\hline')
    for ds, pretty_ds in zip([6,8,7], pretty_datasets):
        for i, gridsize in enumerate(grids):
            if gridsize not in all_results[0][ds]: continue
            line = f'    {pretty_ds} {few_gridlabels[i]} & '
            line += str(int(np.round(results[ds][noaug]['mean'][i].mean()))) + ' & '
            for aug, label in zip(augs, labels):
                if aug == noaug:
                    continue
                tstat, pvalue = ttest_ind(
                    results[ds][noaug]['mean'][i],
                    results[ds][aug]['mean'][i],
                    alternative='greater'
                )
                noaug_mean = results[ds][noaug]['mean'][i].mean()
                aug_mean = results[ds][aug]['mean'][i].mean()
                if noaug_mean/aug_mean > 1.05:
                    line += f'\\textbf{{{str(int(np.round(aug_mean)))}}}  & '
                else:
                    line += str(int(np.round(aug_mean))) + ' & '
                if pvalue < pthreshold:
                    line += f'\\textbf{{{str(np.round(pvalue, 2))}}} & '
                else:
                    line += str(np.round(pvalue, 2)) + ' & '
            line = line[:-2] + ' \\\\ '
            print(line)
        print('    \\hline')
    print('  \\end{tabular}')
    print('  \\label{tab:synthetic_means}')
    print('\\end{table}')


def plot_error_scatters(ds, gridsize, rand=None, plot_all=False, aug_params=[noaug, 'tirem_nn'], key='test', figsize=(6,4), savefig=False, xlabel=True, title=True):
    if plot_all:
        fig, axs = plt.subplots(ncols=3, figsize=figsize, sharex=True, sharey=True)
    else:
        fig, ax = plt.subplots(figsize=figsize)
    meter_scale = 30 if ds in [6,8] else 100
    bs_locs = bs_locations[ds] * meter_scale
    if rand is not None and any(['muted' in param for param in aug_params]):
        muted_inds = muted_sensors[ds][rand]
        muted_locs = np.array([bs_loc for bs_loc in bs_locations[ds] if bs_loc[-1] in muted_inds]) * meter_scale
        bs_locs = np.array([bs_loc for bs_loc in bs_locations[ds] if bs_loc[-1] not in muted_inds]) * meter_scale
    few_labels = [synthetic_labels[synthetic_augs.index(aug)] for aug in aug_params]
    both_errs = []
    keys = ['test', 'train_val'] if key == 'both' else [key]
    for i, aug in enumerate(aug_params):
        vmin, vmax = None, None
        tmp_err = []
        truths = []
        for key in keys:
            gridsize_without_alt = gridsize[5:] if 'alt' in gridsize else (gridsize if 'grid' in gridsize else '')
            set_key = '0.2testsize' + gridsize_without_alt + '_' + (key)# if aug in [noaug, 'muted'] else key+'_synthetic')
            rand_range = range(5) if rand is None else [rand]
            try:
                errs = np.array([all_results[rand][ds][gridsize][aug]['err'][set_key] for rand in rand_range])
                truth = all_results[0][ds][gridsize][aug]['truth'][set_key][:,0,1:]
            except:
                set_key += '_synthetic'
                errs = np.array([all_results[rand][ds][gridsize][aug]['err'][set_key] for rand in rand_range])
                truth = all_results[0][ds][gridsize][aug]['truth'][set_key][:,0,1:]
            tmp_err.append(errs)
            truths.append(truth)
            pretty_label = 'Train Val' if 'train_val' == key else 'OOD Test'
            marker = '+' if 'train_val' == key else None
            size = 16 if 'train_val' == key else None
            if plot_all:
                cmap = axs[i].scatter(truth[:,0], truth[:,1], c=errs.mean(axis=0), label=pretty_label, marker=marker, s=size,
                                      norm=mcolors.LogNorm(vmin=vmin, vmax=vmax))
                if key == keys[0]:
                    axs[i].scatter(bs_locs[:,0], bs_locs[:,1], c='b',label='Base Station', marker='v')
                    if 'muted' in aug:
                        axs[i].scatter(muted_locs[:,0], muted_locs[:,1], c='magenta',label='New Base Station', marker='d', s=80)
                vmin, vmax = cmap.get_clim()
                if xlabel:
                    axs[i].set_xlabel('Distance [m]')
                axs[i].set_ylabel('Distance [m]')
        both_errs.append(tmp_err)
        if plot_all:
            axs[i].set_title(few_labels[i] + ' Error')
            plt.colorbar(cmap, label='Error [m]')
            if len(keys) > 1:
                axs[i].legend()
    err_diff = (both_errs[0][0] - both_errs[1][0]).mean(axis=0)
    vmin, vmax = err_diff.min(), err_diff.max()
    linthresh = np.quantile(abs(err_diff), 0.75)
    if plot_all:
        ax = axs[2]
    for i, key in enumerate(keys):
        err_diff = (both_errs[0][i] - both_errs[1][i]).mean(axis=0)
        pretty_label = 'Train Val' if 'train_val' in key else 'OOD Test'
        marker = '+' if 'train_val' in key else None
        size = 16 if 'train_val' in key else None
        cmap = ax.scatter(
            truths[i][:,0], truths[i][:,1],
            c=err_diff, cmap='coolwarm',
            norm=mcolors.CenteredNorm(),
            s=size, marker=marker, label=pretty_label
        )
        if i == 0:
            ax.scatter(bs_locs[:,0], bs_locs[:,1], c='limegreen',label='Base Station', marker='v')
            if 'muted' in aug:
                ax.scatter(muted_locs[:,0], muted_locs[:,1], c='magenta',label='New Base Station', marker='d', s=80)
    #ax.legend(ncol=1 + len(keys), loc='lower right')
    ax.legend()
    if xlabel:
        ax.set_xlabel('Distance [m]')
    ax.set_ylabel('Distance [m]')
    if title:
        ax.set_title(f'Accuracy Improvement from PALM') #{few_labels[1]}')
    plt.colorbar(cmap, label='Decrease in Error [m]')
    plt.tight_layout(pad=0.1)
    aug_str = aug if aug_params[0] == noaug else '_'.join(aug_params)
    if savefig:
        plt.show()
        fig.savefig(f'plots/mobicom2024_plots/aug_improvement_scatter_{ds}_{gridsize}_{aug_str}_{key}{"_single" if plot_all else ""}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/aug_improvement_scatter_{ds}_{gridsize}_{aug_str}_{key}{"_single" if plot_all else ""}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()


def muted_sensors_results(key='test', include_tirem=False):
    results = {}
    augs = ['muted', 'tirem_nn_muted', noaug, 'tirem_nn']
    if not include_tirem:
        augs = augs[:-1]
    for ds in [6,8,7]:
        results[ds] = {}
        for i, aug in enumerate(augs):
            results[ds][aug] = []
            set_key = '0.2testsize_' + key# + ('_synthetic' if 'tirem' in aug else '')
            means = [all_results[rand][ds]['random'][aug]['err'][set_key].mean() for rand in range(5)]
            results[ds][aug].append( np.array(means) )
            results[ds][aug] = np.array(results[ds][aug])
    return results, augs


def plot_muted_bars(key='test', figsize=(6,3), include_tirem=False, savefig=False):
    results, augs = muted_sensors_results(key, include_tirem)
    for key in results:
        for aug in results[key]:
            results[key][aug] = results[key][aug].mean()
    df = pandas.DataFrame.from_dict(results).rename(columns=dict(zip([6,8,7], pretty_datasets)))
    df = df.T.rename(columns={'muted':'Non-adaptive', 'tirem_nn_muted':'PALM adaptive', noaug:'Full training data', 'tirem_nn':'TIREM++ Full data'})
    df.plot.bar(rot=0, figsize=figsize)
    fig = plt.gcf()
    plt.ylabel('Mean Error [m]')
    plt.tight_layout(pad=0.1)
    #plt.yticks([0,50,150,250,350])
    if savefig:
        fig.savefig(f'plots/mobicom2024_plots/muted_bars_{key}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/muted_bars_{key}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()


def print_muted_sensors_table(key='test'):
    results, augs = muted_sensors_results(key)
    print('\\begin{table}[t]')
    print('  \\centering')
    print('  \\caption{MUTED SENSORS TABLE CAPTION}')
    print('  \\begin{tabular}{l|c|c|c||}')
    print('    & Old Data & TIREM++ Augmentation & Full Data  \\\\ \\hline')
    print('    & Err[m] & Err[m] & Err[m] \\\\ \\hline')
    for ds, pretty_ds in zip([6,8,7], pretty_datasets):
        line = f'    {pretty_ds} Random & '
        for aug in augs:
            line += str(int(np.round(results[ds][aug].mean()))) + ' & '
        line = line[:-2] + ' \\\\ '
        print(line)
    print('    \\hline')
    print('  \\end{tabular}')
    print('  \\label{tab:muted_sensors}')
    print('\\end{table}')


def plot_error_lines(ds, gridsize, rand=None, plot_all=False, aug_params=[noaug, 'tirem_nn'], key='test', figsize=(6,4), savefig=False):
    current_obj = None
    def update(event_ax):
        import pdb
        pdb.set_trace()
        event_ax.set_clim(0,1)

    if plot_all:
        fig, axs = plt.subplots(ncols=3, figsize=figsize, sharex=True, sharey=True)
    else:
        fig, ax = plt.subplots(figsize=figsize)
    meter_scale = 30 if ds in [6,8] else 100
    bs_locs = bs_locations[ds] * meter_scale
    few_labels = [synthetic_labels[synthetic_augs.index(aug)] for aug in aug_params]
    both_errs = []
    keys = ['test', 'train_val'] if key == 'both' else [key]
    for i, aug in enumerate(aug_params):
        vmin, vmax = None, None
        tmp_err = []
        truths = []
        preds = []
        for key in keys:
            gridsize_without_alt = gridsize[5:] if 'alt' in gridsize else (gridsize if 'grid' in gridsize else '')
            set_key = '0.2testsize' + gridsize_without_alt + '_' + (key)# if aug == noaug else key+'_synthetic')
            rand_range = range(5) if rand is None else [rand]
            errs = np.array([all_results[rand][ds][gridsize][aug]['err'][set_key] for rand in rand_range])
            truth = all_results[0][ds][gridsize][aug]['truth'][set_key][:,0,1:]
            pred = all_results[0][ds][gridsize][aug]['preds'][set_key][0,:,0:2]
            tmp_err.append(errs)
            truths.append(truth)
            preds.append(pred)
            pretty_label = 'Train Val' if 'train_val' == key else 'OOD Test'
            marker = '+' if 'train_val' == key else None
            size = 16 if 'train_val' == key else None
            if plot_all:
                axs[i].plot([truth[::10,0], pred[::10,0]], [truth[::10,1], pred[::10,1]], c='tab:blue', alpha=0.3)
                cmap = axs[i].scatter(truth[:,0], truth[:,1], c=errs.mean(axis=0), vmin=vmin, vmax=vmax, label=pretty_label, marker=marker, s=size,
                                      norm=mcolors.LogNorm())
                if key == keys[0]:
                    axs[i].scatter(bs_locs[:,0], bs_locs[:,1], c='c',label='Transmitter', marker='v')
                #vmin, vmax = cmap.get_clim()
                axs[i].set_xlabel('Distance [m]')
                axs[i].set_ylabel('Distance [m]')
        both_errs.append(tmp_err)
        if plot_all:
            axs[i].set_title(few_labels[i] + ' Error')
            plt.colorbar(cmap, label='Error [m]')
            if len(keys) > 1:
                axs[i].legend()
    err_diff = (both_errs[0][0] - both_errs[1][0]).mean(axis=0)
    vmin, vmax = err_diff.min(), err_diff.max()
    if plot_all:
        ax = axs[2]
    for i, key in enumerate(keys):
        err_diff = (both_errs[0][i] - both_errs[1][i]).mean(axis=0)
        pretty_label = 'Train Val' if 'train_val' in key else 'OOD Test'
        marker = '+' if 'train_val' in key else None
        size = 16 if 'train_val' in key else None
        cmap = ax.scatter(
            truths[i][:,0], truths[i][:,1],
            c=err_diff, cmap='coolwarm',
            #norm=mcolors.TwoSlopeNorm(vcenter=0, vmin=vmin, vmax=vmax),
            norm=mcolors.SymLogNorm(2),
            s=size, marker=marker, label=pretty_label
        )
        #ax.plot([truths[i][:,0], preds[i][:,0]], [truths[i][:,1], preds[i][:,1]], c='tab:blue', alpha=0.3)
        if i == 0:
            ax.scatter(bs_locs[:,0], bs_locs[:,1], c='limegreen',label='Base Station', marker='v')
    #ax.callbacks.connect('xlim_changed', update)
    #ax.legend(ncol=1 + len(keys), loc='lower right')
    ax.legend()
    ax.set_xlabel('Distance [m]')
    ax.set_ylabel('Distance [m]')
    ax.set_title(f'Accuracy Improvement from {few_labels[1]}')
    plt.colorbar(cmap, label='Decrease in Error [m]')
    plt.tight_layout(pad=0.1)
    aug_str = aug if aug_params[0] == noaug else '_'.join(aug_params)
    if savefig:
        plt.show()
        fig.savefig(f'plots/mobicom2024_plots/aug_improvement_err_lines_{ds}_{gridsize}_{aug_str}_{key}{"_single" if plot_all else ""}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/aug_improvement_err_lines_{ds}_{gridsize}_{aug_str}_{key}{"_single" if plot_all else ""}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()


def plot_error_hists(rand=None, all_gridsizes=False, aug='tirem_nn', key='test', figsize=(10,8), savefig=False):
    fig, axs = plt.subplots(nrows=3, ncols=(len(gridsizes) if all_gridsizes else 4), figsize=figsize)
    labels = ['No Aug', 'TIREM++', 'CELF', 'RBF']
    augs = [noaug, 'tirem_nn', 'celf', 'rbf']
    labels = ['No Aug', 'TIREM++', 'CELF', 'RBF', 'Dropout']
    augs = [noaug, 'tirem_nn', 'celf', 'rbf', dropout]
    aug_params = [noaug, aug]
    few_labels = [labels[augs.index(aug)] for aug in aug_params]
    tmp_gridsizes = gridsizes if all_gridsizes else (gridsizes[:3] + gridsizes[-1:])
    tmp_gridpositions = gridpositions if all_gridsizes else (gridpositions[:3] + [50])
    tmp_gridlabels = gridlabels if all_gridsizes else (gridlabels[:3] + gridlabels[-1:])
    positions = np.array([0,1,3,4,6,7,9,10])
    for ds_ind, ds in enumerate([6,8,7]):
        tmp_err = []
        tmp_labels = []
        tmp_colors = []
        for j, gridsize in enumerate(tmp_gridsizes):
            for i, aug in enumerate(aug_params):
                truths = []
                set_key = '0.2testsize' + (gridsize if 'grid' in gridsize else '') + '_' + (key)# if isinstance(aug,tuple)  else key+'_synthetic')
                rand_range = range(5) if rand is None else [rand]
                errs = np.array([all_results[rand][ds][gridsize][aug]['err'][set_key] for rand in rand_range])
                #tmp_err.append(errs.mean(axis=0))
                tmp_err.append(errs.flatten())
                #tmp_labels.append(few_labels[i])
                #tmp_colors.append(tab_colors[i])
            err_diff = tmp_err[-2] - tmp_err[-1]
            axs[ds_ind,j].hist(err_diff, bins=20, color='c', edgecolor='k', alpha=0.6)
            axs[ds_ind,j].axvline(err_diff.mean(), linestyle='dashed')
            axs[ds_ind,j].set_title(f'{pretty_datasets[ds_ind]} {tmp_gridlabels[j]}')
            min_ylim, max_ylim = axs[ds_ind,j].get_ylim()
            min_xlim, max_xlim = axs[ds_ind,j].get_xlim()
            xshift = max_xlim/8 if max_xlim > (min_xlim * -0.75) else min_xlim/2
            axs[ds_ind,j].text(err_diff.mean()+xshift, max_ylim*0.8, 'Mean:\n {:.1f}'.format(err_diff.mean()))
            print(ds, gridsize, err_diff.mean()/tmp_err[-2].mean())
    fig.supylabel('Count')
    fig.supxlabel(f'Decrease in Error from {few_labels[-1]} Augmentation [m]')
    plt.tight_layout(pad=0.1)
    plt.subplots_adjust(bottom=0.09, left=0.095)
    if savefig:
        plt.savefig(f'plots/mobicom2024_plots/aug_improvement_hists_{few_labels[-1]}_{key}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/aug_improvement_hists_{few_labels[-1]}_{key}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()


def get_separated_accuracy(tx_rx_max_separation=400, test_train_min_separation=200, key='test', grids=fewgridsizes, rand=None, print_all=True):
    labels = ['No Aug', 'TIREM++', 'CELF', 'RBF', 'TIREM-FS']
    augs = [noaug, 'tirem_nn', 'celf', 'rbf', 'tirem_fs']
    labels = ['No Aug', 'TIREM++']
    augs = [noaug, 'tirem_nn']
    few_gridlabels = [altgridlabels[altgridsizes.index(grid)] for grid in grids]
    rand_range = range(5) if rand is None else [rand]
    ordered_labels = []
    ordered_avg = []
    overall_improvement = []
    for ds in [6,8,7]:
        meter_scale = 30 if ds in [6,8] else 100
        bs_locs = bs_locations[ds] * meter_scale
        for gridsize in grids:
            for i, aug in enumerate(augs):
                gridsize_without_alt = gridsize[5:] if 'alt' in gridsize else (gridsize if 'grid' in gridsize else '')
                train_key = '0.2testsize' + gridsize_without_alt + '_' + 'train'
                set_key = '0.2testsize' + gridsize_without_alt + '_' + (key)# if aug == noaug else key+'_synthetic')
                if gridsize not in all_results[0][ds]:
                    continue
                train_truth = all_results[0][ds][gridsize][noaug]['truth'][train_key][:,0,1:]
                truth = all_results[0][ds][gridsize][aug]['truth'][set_key][:,0,1:]
                nearest_train = scipy.spatial.distance.cdist(train_truth, truth).min(axis=0)
                nearest_bs = scipy.spatial.distance.cdist(bs_locs[:,:2], truth).min(axis=0)
                noaug_err = np.array([all_results[rand][ds][gridsize][noaug]['err'][set_key.replace('_synthetic','')] for rand in rand_range]).mean(axis=0)
                err = np.array([all_results[rand][ds][gridsize][aug]['err'][set_key] for rand in rand_range]).mean(axis=0)
                far_inds = (nearest_train > test_train_min_separation) * (nearest_bs < tx_rx_max_separation)
                far_err = err[far_inds]
                far_noaug_err = noaug_err[far_inds]
                if len(far_err) == 0:
                    continue
                if print_all:
                    print(ds, labels[i], gridsize, 'Improvement: %.1f  Percent: %.3f' % ((far_noaug_err - far_err).mean(), (far_noaug_err - far_err).mean()/far_noaug_err.mean()), err.mean(), far_err.mean(), err.shape, far_err.shape)
                if aug != noaug:
                    overall_improvement.append(((far_noaug_err - far_err).mean(), (far_noaug_err - far_err).mean()/far_noaug_err.mean(), len(far_err)))
                ordered_labels.append((ds,labels[i], gridsize))
                ordered_avg.append((err.mean(), far_err.mean()))
                plt.scatter(truth[:,0], truth[:,1])
                plt.scatter(truth[far_inds,0], truth[far_inds,1])
                plt.scatter(train_truth[:,0], train_truth[:,1], s=5, marker='x')
                plt.show()
    overall_improvement = np.array(overall_improvement)
    print(overall_improvement, overall_improvement.mean(axis=0),  (overall_improvement[:,:2] * overall_improvement[:,2:]).sum(axis=0) / overall_improvement[:,2].sum())
    return {lab: res for lab, res in zip(ordered_labels, ordered_avg)}


def get_percentile_accuracy(worst_percentile=0.75, key='test', grids=fewgridsizes, rand=None, print_all=True):
    results = {}
    labels = ['No Aug', 'TIREM++', 'CELF', 'RBF', 'TIREM-FS']
    augs = [noaug, 'tirem_nn', 'celf', 'rbf', 'tirem_fs']
    labels = ['No Aug', 'TIREM++']
    augs = [noaug, 'tirem_nn']
    few_gridlabels = [altgridlabels[altgridsizes.index(grid)] for grid in grids]
    rand_range = range(5) if rand is None else [rand]
    ordered_labels = []
    ordered_avg = []
    overall_improvement = []
    for ds in [6,8,7]:
        meter_scale = 30 if ds in [6,8] else 100
        bs_locs = bs_locations[ds] * meter_scale
        for gridsize in grids:
            for i, aug in enumerate(augs):
                gridsize_without_alt = gridsize[5:] if 'alt' in gridsize else (gridsize if 'grid' in gridsize else '')
                train_key = '0.2testsize' + gridsize_without_alt + '_' + 'train'
                set_key = '0.2testsize' + gridsize_without_alt + '_' + (key)# if aug == noaug else key+'_synthetic')
                if gridsize not in all_results[0][ds]:
                    continue
                train_truth = all_results[0][ds][gridsize][noaug]['truth'][train_key][:,0,1:]
                truth = all_results[0][ds][gridsize][aug]['truth'][set_key][:,0,1:]
                noaug_err = np.array([all_results[rand][ds][gridsize][noaug]['err'][set_key.replace('_synthetic','')] for rand in rand_range]).mean(axis=0)
                err = np.array([all_results[rand][ds][gridsize][aug]['err'][set_key] for rand in rand_range]).mean(axis=0)
                noaug_threshold = np.quantile(noaug_err, worst_percentile)
                far_inds = noaug_err > noaug_threshold
                far_err = err[far_inds]
                far_noaug_err = noaug_err[far_inds]
                if len(far_err) == 0:
                    continue
                if print_all:
                    print(ds, labels[i], gridsize, 'Improvement: %.1f  Percent: %.3f         ' % ((far_noaug_err - far_err).mean(), (far_noaug_err - far_err).mean()/far_noaug_err.mean()), err.mean(), far_err.mean(), err.shape, far_err.shape, err[err > np.quantile(err, worst_percentile)].mean())
                if 'alt' not in gridsize and ds == 8 and 'grid2' in gridsize:
                    continue
                #if 'alt' in gridsize:
                #    overall_improvement.pop(-2)
                #    ordered_labels.pop(-2)
                #    ordered_avg.pop(-2)
                if aug != noaug:
                    overall_improvement.append(((far_noaug_err - far_err).mean(), (far_noaug_err - far_err).mean()/far_noaug_err.mean(), len(far_err)))
                ordered_labels.append((ds,labels[i], gridsize))
                ordered_avg.append((err.mean(), far_err.mean()))
    overall_improvement = np.array(overall_improvement)
    print(overall_improvement, overall_improvement.mean(axis=0),  (overall_improvement[:,:2] * overall_improvement[:,2:]).sum(axis=0) / overall_improvement[:,2].sum())
    return {lab: res for lab, res in zip(ordered_labels, ordered_avg)}


def print_optimistic_table(worst_percentile=0.95, range=200, limit=200):
    res1 = get_separated_accuracy(range, limit)
    res2 = get_percentile_accuracy(worst_percentile)
    print('\\begin{table}[t]')
    print('  \\centering')
    print('  \\caption{OPTIMISTIC RESULTS TABLE CAPTION}')
    print('  \\begin{tabular}{l|c|c|c|c|c|c|}')
    print('    & \multicolumn{3}{c|}{No Aug Avg Err [m]} & \multicolumn{3}{c|}{PALM Avg Err [m]}  \\\\')
    print('    & Avg & 95\%ile & 200m  & Avg & 95\%ile & 200m \\\\ \\hline')
    for label in res2.keys():
        ds, text_name, gridsize = label
        pretty_grid = altgridlabels[altgridsizes.index(gridsize)]
        pretty_ds = pretty_datasets[0 if ds == 6 else (1 if ds == 8 else 2)]
        if 'No Aug' in text_name:
            line = f'    {pretty_ds} {pretty_grid} & '
            line += '%.0f & %.0f & ' % (res2[label][0], res2[label][1])
            if label in res1:
                line += '%.0f & ' % res1[label][1]
            else:
                line += ' -  & '
        else:
            if label in res2:
                line += '%.0f & %.0f & ' % (res2[label][0], res2[label][1])
            else:
                line += ' -  & -  & '
            if label in res1:
                line += '%.0f ' % res1[label][1]
            else:
                line += ' -  '
            line += ' \\\\ \\hline '
            print(line)
    print('  \\end{tabular}')
    print('  \\label{tab:optimistic_results}')
    print('\\end{table}')


if __name__ == '__main__':
    main()
    #plot_hi_low_bars('random')

    #plot_basic_means_over_grid_all_ds('test', savefig=True)
    #plot_adv_means_over_grid_all_ds(attacks=['top', 'drop'], savefig=True)
    #plot_adv_means_over_grid_all_ds(attacks=['hilo', 'hilotop'], savefig=True)
    #plot_adv_means_over_grid_all_ds(attacks=['top', 'hilotop', 'worst'], savefig=True)
    #plot_adv_means_over_grid_all_ds(attacks=['top', 'drop'], log_scale=True, savefig=True)
    #plot_adv_means_over_grid_all_ds(attacks=['hilo', 'hilotop'], log_scale=True, savefig=True)
    #plot_adv_means_over_grid_all_ds(attacks=['top', 'hilotop', 'worst'], log_scale=True, savefig=True)
    #print_table_basic_augs_over_grid('test')
    #plot_error_scatters(6, 'grid2', key='both', figsize=(6,3), savefig=True, xlabel=True, title=False)
    worst_case_analysis()
    embed()

        #import rasterio 
        #outer_origin = np.array([592000, 5671000])
        #tif2 = rasterio.open('test.tif')
        #samples = imd.data[None]
        #origin = samples.origin
        #top_corner = origin + np.array([samples.rectangle_width,  samples.rectangle_height])
        #corners = np.array([origin, top_corner])
        #transform = np.array(tif2.transform).reshape(3,3)
        #transform[:2,2] -= outer_origin
        #inv_transform = np.linalg.inv(transform)
        #a,b = corners.min(axis=0), corners.max(axis=0)
        #a_ind = (inv_transform @ np.array([a[0], a[1], 1]) ).round().astype(int)
        #b_ind = (inv_transform @ np.array([b[0], b[1], 1]) ).round().astype(int)
        #data = tif2.read(1)
        #sub_img = data[b_ind[1]:a_ind[1]+1, a_ind[0]:b_ind[0]+1]
        #sub_img = np.flipud(sub_img)
        #tx_dat = imd.data[imd.train_key].ordered_dataloader.dataset.tensors[1].cpu()
        #tx_dat1 = imd.data[imd.test_keys[0]].ordered_dataloader.dataset.tensors[1].cpu()
        #plt.imshow(sub_img, origin='lower')
        #plt.scatter(150*tx_dat[:,0,1], 150*tx_dat[:,0,2], c='red')
        #plt.scatter(150*tx_dat1[:,0,1], 150*tx_dat1[:,0,2], c='red')
        #rx_dat = imd.data[imd.train_key].ordered_dataloader.dataset.tensors[0].cpu()
        #valid_rx = rx_dat[:,:,0] > 0
        #plt.scatter(150*rx_dat[valid_rx][:,1].flatten(), 150*rx_dat[valid_rx][:,2].flatten(), marker='*', c='yellow')
        #plt.show()
        ##imd.make_elevation_tensors()
        #embed()
no_worst_err = np.array([
    np.array([451.85236,196.32239,137.66245,101.298325,81.65003,62.266163]),
    np.array([514.954,192.34119,149.07062,99.50444,88.183365,65.49123,]),
    np.array([491.68195,215.7825,162.28961,151.61453,114.32725,97.72165]),
    np.array([488.9078,219.00786,165.793,148.36513,114.04071,101.96428]),
    np.array([1120.4508,603.3292,532.8576,385.47794,362.0567,294.1612,]),
    np.array([1104.834,591.1753,521.8014,384.40012,391.1496,296.4142,]),
    np.array([758.4389,411.58505,339.98013,306.82715,262.76718,246.97661]),
    np.array([1036.3816,513.5842,438.5038,416.66397,384.7623,311.68604]),
    np.array([1157.2041,968.74866,677.2638,773.5715,759.68414,779.9964,]),
    np.array([1175.2805,955.4598,641.95575,774.7103,707.57495,701.99615]),
    np.array([2254.9214,1431.5765,1460.949,1290.8167,1275.1868,1278.224,]),
    np.array([2492.869,1511.5793,1555.2849,1396.7616,1433.3702,1366.3793]),
])
no_worst_diff = (no_worst_err[1::2] - no_worst_err[::2]) / no_worst_err[::2]
adv_tr_no = no_worst_diff[:3].mean()
adv_tr_worst = no_worst_diff[3:].mean()
print('Avg increase in error for adv tr, noattack', adv_tr_no)
print('Avg increase in error for adv tr, worst', adv_tr_worst)
