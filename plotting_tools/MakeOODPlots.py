import time
import matplotlib.ticker as tick
import matplotlib.colors as mcolors
import numpy as np
import os
import pickle
import argparse
from IPython import embed
from localization import *
from Splot import *
from dataset import RSSLocDataset
from scipy.stats import pearsonr
import models as DeepNets
import scipy
plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

dpi=300
num_images = 20

def get_data(start=0, stop=100):
    all_data = {}
    all_data[f'tr_images_first{num_images}'] = {}
    all_data[f'tv_images_first{num_images}'] = {}
    all_data[f'te_images_first{num_images}'] = {}
    all_data['tr_images_higherr'] = {}
    all_data['tv_images_higherr'] = {}
    all_data['te_images_higherr'] = {}
    all_data['tr_images_lowconf'] = {}
    all_data['tv_images_lowconf'] = {}
    all_data['te_images_lowconf'] = {}
    all_data['tr_images_highspread'] = {}
    all_data['tv_images_highspread'] = {}
    all_data['te_images_highspread'] = {}
    all_data[f'tr_inputs_first{num_images}'] = {}
    all_data[f'tv_inputs_first{num_images}'] = {}
    all_data[f'te_inputs_first{num_images}'] = {}
    all_data['tr_inputs_higherr'] = {}
    all_data['tv_inputs_higherr'] = {}
    all_data['te_inputs_higherr'] = {}
    all_data['tr_inputs_lowconf'] = {}
    all_data['tv_inputs_lowconf'] = {}
    all_data['te_inputs_lowconf'] = {}
    all_data['tr_inputs_highspread'] = {}
    all_data['tv_inputs_highspread'] = {}
    all_data['te_inputs_highspread'] = {}
    all_data['tr_truth'] = {}
    all_data['tv_truth'] = {}
    all_data['te_truth'] = {}
    all_data['tr_preds'] = {}
    all_data['tv_preds'] = {}
    all_data['te_preds'] = {}
    all_data['tr_ensemble_preds'] = {}
    all_data['tv_ensemble_preds'] = {}
    all_data['te_ensemble_preds'] = {}
    all_data['tr_splot_preds'] = {}
    all_data['tv_splot_preds'] = {}
    all_data['te_splot_preds'] = {}
    all_data['tr_err'] = {}
    all_data['tv_err'] = {}
    all_data['te_err'] = {}
    all_data['tr_splot_err'] = {}
    all_data['tv_splot_err'] = {}
    all_data['te_splot_err'] = {}
    all_data['tr_max_conf'] = {}
    all_data['tv_max_conf'] = {}
    all_data['te_max_conf'] = {}
    all_data['tr_all_conf'] = {}
    all_data['tv_all_conf'] = {}
    all_data['te_all_conf'] = {}
    all_data['tr_max_mean_conf'] = {}
    all_data['tv_max_mean_conf'] = {}
    all_data['te_max_mean_conf'] = {}
    all_data['tr_spread_sum'] = {}
    all_data['tv_spread_sum'] = {}
    all_data['te_spread_sum'] = {}
    all_data['tr_spread_max'] = {}
    all_data['tv_spread_max'] = {}
    all_data['te_spread_max'] = {}
    all_data['tr_dist'] = {}

    for i in range(start,stop):
      for prefix in ['results/ood_seasonal_results_fixed', 'results/ood_grid_results_fixed']:
      #for prefix in ['ood_seasonal_results_fixed']:
        filename =f'{prefix}_{i}.pkl'
        if not os.path.exists(filename):
            continue
        with open(filename, 'rb') as f:
            data = pickle.load(f)
        for key in data:
            meter_scale, dataset_index, split_str, random_state = key[:4]
            loss_str = key[5]
            (train_images, tv_images, test_images), (tr_truth, tv_truth, te_truth), (tr_preds, tv_preds, te_preds), (tr_splot_preds, tv_splot_preds, te_splot_preds), (tr_err, tv_err, te_err) = data[key]
            #if 'limited' not in split_str: continue
            if 'grid' in split_str:
                grid_size = int(''.join(c for c in split_str if c.isdigit()))
                new_key = (meter_scale, dataset_index, grid_size, random_state, loss_str)
                if grid_size in [15,20]: continue
            else:
                new_key = (meter_scale, dataset_index, split_str, random_state, loss_str)
                #if meter_scale not in [30,100,150]: continue
            print(key)

            all_data['tr_truth'][new_key] = tr_truth.copy()
            all_data['tv_truth'][new_key] = tv_truth.copy()
            all_data['te_truth'][new_key] = te_truth.copy()
            all_data['tr_preds'][new_key] = tr_preds.copy()
            all_data['tv_preds'][new_key] = tv_preds.copy()
            all_data['te_preds'][new_key] = te_preds.copy()
            all_data['tr_splot_preds'][new_key] = tr_splot_preds.copy()
            all_data['tv_splot_preds'][new_key] = tv_splot_preds.copy()
            all_data['te_splot_preds'][new_key] = te_splot_preds.copy()
            all_data['tr_err'][new_key] = tr_err.copy()
            all_data['tv_err'][new_key] = tv_err.copy()
            all_data['te_err'][new_key] = te_err.copy()
            all_data['tr_max_conf'][new_key] = train_images[0].reshape(len(train_images[0]), -1).max(axis=1)
            all_data['tv_max_conf'][new_key] = tv_images[0].reshape(len(tv_images[0]), -1).max(axis=1)
            all_data['te_max_conf'][new_key] = test_images[0].reshape(len(test_images[0]), -1).max(axis=1)
            all_data['tr_all_conf'][new_key] = train_images[0].reshape(len(train_images[0]), 5, -1).max(axis=2)
            all_data['tv_all_conf'][new_key] = tv_images[0].reshape(len(tv_images[0]), 5, -1).max(axis=2)
            all_data['te_all_conf'][new_key] = test_images[0].reshape(len(test_images[0]), 5, -1).max(axis=2)
            all_data['tr_max_mean_conf'][new_key] = train_images[0].mean(axis=1).reshape(len(train_images[0]), -1).max(axis=1)
            all_data['tv_max_mean_conf'][new_key] = tv_images[0].mean(axis=1).reshape(len(tv_images[0]), -1).max(axis=1)
            all_data['te_max_mean_conf'][new_key] = test_images[0].mean(axis=1).reshape(len(test_images[0]), -1).max(axis=1)
            all_data['tr_splot_err'][new_key] = np.linalg.norm(tr_splot_preds - tr_truth, axis=1)
            all_data['tv_splot_err'][new_key] = np.linalg.norm(tv_splot_preds - tv_truth, axis=1)
            all_data['te_splot_err'][new_key] = np.linalg.norm(te_splot_preds - te_truth, axis=1)

            dist = np.zeros_like(train_images[0][0][0])
            for i in range(len(dist)):
                for j in range(len(dist[0])):
                    coord = np.array([i,j]) * meter_scale
                    dist[i,j] = min(np.linalg.norm(coord - tr_truth, axis=1))
            all_data['tr_dist'][new_key] = dist.flatten()

            if random_state in [0,1,2,3]:
                all_data[f'tr_images_first{num_images}'][new_key] = train_images[0][:num_images].copy()
                all_data[f'tv_images_first{num_images}'][new_key] = tv_images[0][:num_images].copy()
                all_data[f'te_images_first{num_images}'][new_key] = test_images[0][:num_images].copy()
                all_data['tr_images_higherr'][new_key] = train_images[0][np.argpartition(tr_err, -num_images)[-num_images:]].copy()
                all_data['tv_images_higherr'][new_key] = tv_images[0][np.argpartition(tv_err, -num_images)[-num_images:]].copy()
                all_data['te_images_higherr'][new_key] = test_images[0][np.argpartition(te_err, -num_images)[-num_images:]].copy()
                all_data['tr_images_lowconf'][new_key] = train_images[0][np.argpartition(all_data['tr_max_mean_conf'][new_key], num_images)[:num_images]].copy()
                all_data['tv_images_lowconf'][new_key] = tv_images[0][np.argpartition(all_data['tv_max_mean_conf'][new_key], num_images)[:num_images]].copy()
                all_data['te_images_lowconf'][new_key] = test_images[0][np.argpartition(all_data['te_max_mean_conf'][new_key], num_images)[:num_images]].copy()
                all_data[f'tr_inputs_first{num_images}'][new_key] = train_images[1][:,1][:num_images].copy()
                all_data[f'tv_inputs_first{num_images}'][new_key] = tv_images[1][:,1][:num_images].copy()
                all_data[f'te_inputs_first{num_images}'][new_key] = test_images[1][:,1][:num_images].copy()
                all_data['tr_inputs_higherr'][new_key] = train_images[1][:,1][np.argpartition(tr_err, -num_images)[-num_images:]].copy()
                all_data['tv_inputs_higherr'][new_key] = tv_images[1][:,1][np.argpartition(tv_err, -num_images)[-num_images:]].copy()
                all_data['te_inputs_higherr'][new_key] = test_images[1][:,1][np.argpartition(te_err, -num_images)[-num_images:]].copy()
                all_data['tr_inputs_lowconf'][new_key] = train_images[1][:,1][np.argpartition(all_data['tr_max_mean_conf'][new_key], num_images)[:num_images]].copy()
                all_data['tv_inputs_lowconf'][new_key] = tv_images[1][:,1][np.argpartition(all_data['tv_max_mean_conf'][new_key], num_images)[:num_images]].copy()
                all_data['te_inputs_lowconf'][new_key] = test_images[1][:,1][np.argpartition(all_data['te_max_mean_conf'][new_key], num_images)[:num_images]].copy()

            maxes, sums = [], []
            ensemble_peaks = []
            for preds in [train_images[0], tv_images[0], test_images[0]]:
                peak_locs = np.argmax( preds.reshape((preds.shape[0], 5, -1)), axis=-1)
                peak_locs = np.flip(np.array(np.unravel_index(peak_locs, preds.shape[2:])).T, axis=2)
                max_distances = np.zeros((5, len(peak_locs[0])))
                sum_distances = np.zeros((5, len(peak_locs[0])))
                for i, loc in enumerate(peak_locs):
                    max_distances[i] = np.linalg.norm(peak_locs - loc[None,:], axis=2).max(axis=0)
                    sum_distances[i] = np.linalg.norm(peak_locs - loc[None,:], axis=2).sum(axis=0)
                ensemble_peaks.append(peak_locs * meter_scale)
                maxes.append(max_distances.max(axis=0))
                sums.append(sum_distances.max(axis=0))
            all_data['tr_spread_sum'][new_key], all_data['tv_spread_sum'][new_key], all_data['te_spread_sum'][new_key] = sums
            all_data['tr_spread_max'][new_key], all_data['tv_spread_max'][new_key], all_data['te_spread_max'][new_key] = maxes
            all_data['tr_ensemble_preds'][new_key], all_data['tv_ensemble_preds'][new_key], all_data['te_ensemble_preds'][new_key] = ensemble_peaks

            if random_state == 0:
                all_data['tr_images_highspread'][new_key] = train_images[0][np.argpartition(all_data['tr_spread_sum'][new_key], -num_images)[-num_images:]].copy()
                all_data['tv_images_highspread'][new_key] = tv_images[0][np.argpartition(all_data['tv_spread_sum'][new_key], -num_images)[-num_images:]].copy()
                all_data['te_images_highspread'][new_key] = test_images[0][np.argpartition(all_data['te_spread_sum'][new_key], -num_images)[-num_images:]].copy()
                all_data['tr_inputs_highspread'][new_key] = train_images[1][:,1][np.argpartition(all_data['tr_spread_sum'][new_key], -num_images)[-num_images:]].copy()
                all_data['tv_inputs_highspread'][new_key] = tv_images[1][:,1][np.argpartition(all_data['tv_spread_sum'][new_key], -num_images)[-num_images:]].copy()
                all_data['te_inputs_highspread'][new_key] = test_images[1][:,1][np.argpartition(all_data['te_spread_sum'][new_key], -num_images)[-num_images:]].copy()
    return all_data

def get_dataset_plots_and_table():
    options = [
        [6, 'nov_selftest_july_limited', 30, 0],
        [6, 'random', 30, 0],
        [6, 'grid2', 30, 0],
        [6, 'grid5', 30, 0],
    ]
    for dataset_index, split, meter_scale, random_state in options:
        params = LocConfig(
            dataset_index,
            data_split=split,
            arch='unet_ensemble',
            batch_size=1,
            meter_scale=meter_scale,
            random_state=random_state,
            min_sensors=6,
            force_num_tx=1,
            device=torch.device('cpu'),
        )

        imd = RSSLocDataset(params, random_state=random_state)
        train_key, test_key = imd.make_datasets(make_val=True)
        if 'selftest' in split:
            imd.plot_separation('july', 'nov', labels=['July', 'November'], plot_rx=True)
        else:
            imd.plot_separation(train_key, test_key, labels=['Train', 'Test'], plot_rx=True)
        imd.print_dataset_stats()

def plot_tx_loc_maps(meter_scale=30, dataset_index=6, random_state=0, separations=['random', 2, 5, 'july_selftest_nov_limited'], figsize=(3,2.5)):
    size = 2
    for separation in separations:
        fig = plt.figure(figsize=figsize)
        if isinstance(separation, str) and 'selftest' in separation:
            jkey = (meter_scale, dataset_index, 'july_selftest_nov_limited', random_state, 'emd_p1')
            nkey = (meter_scale, dataset_index, 'nov_selftest_july_limited', random_state, 'emd_p1')
            j_tr = all_data['tr_truth'][jkey]
            n_tr = all_data['tr_truth'][nkey]
            j_te = all_data['te_truth'][jkey]
            n_te = all_data['tv_truth'][nkey]
            plt.scatter(j_tr[:,0], j_tr[:,1], alpha=0.8, c='tab:blue', s=size, marker='.', label='July')
            plt.scatter(j_te[:,0], j_te[:,1], alpha=0.8, c='tab:blue', s=size, marker='.')
            plt.scatter(n_tr[:,0], n_tr[:,1], alpha=0.8, c='tab:orange', s=size, marker='v', label='Nov.')
            plt.scatter(n_te[:,0], n_te[:,1], alpha=0.8, c='tab:orange', s=size, marker='v')
        else:
            new_key = (meter_scale, dataset_index, separation, random_state, 'emd_p1')
            tr_truth = all_data['tr_truth'][new_key]
            tv_truth = all_data['tv_truth'][new_key]
            te_truth = all_data['te_truth'][new_key]
            plt.scatter(tr_truth[:,0], tr_truth[:,1], alpha=0.8, c='tab:blue', s=size, marker='.', label='Train')
            plt.scatter(tv_truth[:,0], tv_truth[:,1], alpha=0.8, c='tab:blue', s=size, marker='.')
            plt.scatter(te_truth[:,0], te_truth[:,1], alpha=0.8, c='tab:orange', s=size, marker='v', label='Test')
        leg = plt.legend(loc='lower right', framealpha=0.9, borderpad=0.3, labelspacing=0.2, handlelength=0.4, handletextpad=0.5, markerscale=4.0)
        plt.tight_layout()
        plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
        plt.savefig(f'plots/dyspan2024_plots/dataset_tx_map_{separation}_{dataset_index}.png',dpi=dpi, bbox_inches='tight')
        plt.savefig(f'plots/dyspan2024_plots/dataset_tx_map_{separation}_{dataset_index}.pdf', bbox_inches='tight')
        plt.clf()
        plt.close()

def save_outputs_from_losses(meter_scale=30, dataset_index=6, random_state=0, separation='random', figsize=(3,2.5)):
    for loss in ['emd_p1', 'com', 'mse']:
        new_key = (meter_scale, dataset_index, separation, random_state, loss)
        fig = plt.figure(figsize=figsize)
        plt.imshow(all_data[f'tv_images_first{num_images}'][new_key][0].mean(axis=0), origin='lower')
        plt.axis('off')
        fig.savefig(f'plots/dyspan2024_plots/images/output_loss_{loss}.png',dpi=dpi, bbox_inches='tight', pad_inches=0)
        fig.savefig(f'plots/dyspan2024_plots/images/output_loss_{loss}.pdf', bbox_inches='tight', pad_inches=0)
        plt.cla()
        plt.close()


def plot_predictions_within_training_area(meter_scale, dataset_index, grid_size, random_state, figsize=(5.5,3.75), use_weighted=True):
    new_key = (meter_scale, dataset_index, grid_size, random_state, 'emd_p1')
    tr_truth = all_data['tr_truth'][new_key]
    te_truth = all_data['te_truth'][new_key]
    if use_weighted:
        te_preds = all_data['te_preds'][new_key]
    else:
        te_preds = all_data['te_ensemble_preds'][new_key].reshape(-1,2)
    fig = plt.figure(figsize=figsize)
    plt.scatter(tr_truth[:,0], tr_truth[:,1], alpha=0.1, label='Train Locations')
    plt.scatter(te_truth[:,0], te_truth[:,1], alpha=0.1, c='grey', marker='P', label='OOD Locations')
    plt.scatter(te_preds[:,0], te_preds[:,1], alpha=0.7, marker='x', c='tab:red', label='OOD Predictions')
    if meter_scale == 30 and dataset_index == 6 and grid_size == 2:
        box1 = np.array([
            [117, 1200, 1200, 117, 117],
            [1280, 1280, 415, 415, 1280]])
        plt.plot(box1[0], box1[1], c='tab:green', alpha=0.7, label='OOD Area')
    elif meter_scale == 30 and dataset_index == 6 and grid_size == 3:
        box1 = np.array([
            [117, 848, 848, 117, 117],
            [1000, 1000, 415, 415, 1000]])
        box2 = np.array([
            [1565, 1565, 850, 850, 1565],
            [2176, 1612, 1612, 2176, 2176]])
        plt.plot(box1[0], box1[1], c='tab:green', alpha=0.7, label='OOD Area')
        plt.plot(box2[0], box2[1], c='tab:green', alpha=0.7)
    elif meter_scale == 30 and dataset_index == 6 and grid_size == 5:
        box1 = np.array([
            [127, 568, 568, 127, 127],
            [1120, 1120, 785, 785, 1120]])
        box2 = np.array([
            [1435, 1435, 1000, 1000, 1435],
            [2176, 1832, 1832, 2176, 2176]])
        box3 = np.array([
            [568, 568, 990, 990, 568],
            [775, 455, 455, 775, 775]])
        box4 = np.array([
            [1435, 1435, 1000, 1000, 1435],
            [1130, 1490, 1490, 1130, 1130]])
        plt.plot(box1[0], box1[1], c='tab:green', alpha=0.7, label='OOD Area')
        plt.plot(box2[0], box2[1], c='tab:green', alpha=0.7)
        plt.plot(box3[0], box3[1], c='tab:green', alpha=0.7)
        plt.plot(box4[0], box4[1], c='tab:green', alpha=0.7)
    elif meter_scale == 150 and dataset_index == 7 and grid_size == 3:
        box1 = np.array([
            [1997, 3839, 3839, 1997, 1997],
            [3605, 3605, 5216, 5216, 3605]])
        box2 = np.array([
            [215, 1980, 1980, 215, 215],
            [153, 153, 1832, 1832, 153]])
        plt.plot(box1[0], box1[1], c='tab:green', alpha=0.7, label='OOD Area')
        plt.plot(box2[0], box2[1], c='tab:green', alpha=0.7)
    leg = plt.legend(loc='lower right', framealpha=0.9)
    leg = plt.legend(loc='lower right', framealpha=0.9, borderpad=0.3, handlelength=1.5, handletextpad=0.5)
    for lh in leg.legendHandles:
        lh.set_alpha(0.8)
    plt.tight_layout()
    plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
    plt.savefig(f'plots/dyspan2024_plots/predictions_within_training_area_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.png',dpi=dpi, bbox_inches='tight')
    plt.savefig(f'plots/dyspan2024_plots/predictions_within_training_area_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.pdf', bbox_inches='tight')
    plt.clf()
    plt.close()

def plot_filters_within_training_area(meter_scale, dataset_index, grid_size, random_state, filter_size=0.1, figsize=(6,4)):
    new_key = (meter_scale, dataset_index, grid_size, random_state, 'emd_p1')
    tr_truth = all_data['tr_truth'][new_key]
    te_truth = all_data['te_truth'][new_key]
    fig = plt.figure(figsize=figsize)
    thresholds = get_separation(meter_scale,dataset_index, grid_size, random_state, ['max_mean_conf', 'spread_sum'], filter_size, upper_threshold=[True, False], two_inputs=True)[0]
    comb_preds, comb_err, filter_inds = get_combined_preds_and_err(
        ml_preds=all_data['te_preds'][new_key],
        range_preds=all_data['te_splot_preds'][new_key],
        truth=all_data['te_truth'][new_key],
        conf=all_data['te_max_mean_conf'][new_key],
        spread=all_data['te_spread_sum'][new_key],
        conf_thresh=thresholds[0],
        spread_thresh=thresholds[1]
    )
    plt.scatter(tr_truth[:,0], tr_truth[:,1], alpha=0.2, c='grey', s=2, label='Train Tx Locations')
    plt.scatter(te_truth[filter_inds,0], te_truth[filter_inds,1], alpha=0.6, marker='o', s=20, c='tab:red', label='Filtered OOD Locations')
    plt.scatter(te_truth[~filter_inds,0], te_truth[~filter_inds,1], alpha=0.6, marker='o', s=20, c='tab:green', label='Non-filtered OOD Locations')
    if meter_scale == 30 and dataset_index == 6 and grid_size == 3:
        box1 = np.array([
            [117, 848, 848, 117, 117],
            [1000, 1000, 415, 415, 1000]])
        box2 = np.array([
            [1565, 1565, 850, 850, 1565],
            [2176, 1612, 1612, 2176, 2176]])
        plt.plot(box1[0], box1[1], c='tab:green', alpha=0.7, label='OOD Area')
        plt.plot(box2[0], box2[1], c='tab:green', alpha=0.7)
    elif meter_scale == 150 and dataset_index == 7 and grid_size == 3:
        box1 = np.array([
            [1997, 3839, 3839, 1997, 1997],
            [3605, 3605, 5216, 5216, 3605]])
        box2 = np.array([
            [215, 1980, 1980, 215, 215],
            [153, 153, 1832, 1832, 153]])
        plt.plot(box1[0], box1[1], c='tab:green', alpha=0.7, label='OOD Area')
        plt.plot(box2[0], box2[1], c='tab:green', alpha=0.7)
    plt.legend(loc='lower right')
    plt.tight_layout()
    plt.tick_params(axis='both', which='both', bottom=False, top=False, left=False, right=False, labelbottom=False, labelleft=False)
    plt.savefig(f'plots/dyspan2024_plots/filtered_truth_within_training_area_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.png',dpi=dpi, bbox_inches='tight')
    plt.savefig(f'plots/dyspan2024_plots/filtered_truth_within_training_area_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.pdf', bbox_inches='tight')
    plt.clf()
    plt.close()

def plot_data_error_scatter(meter_scale, dataset_index, grid_size, random_state, threshold=0.1, data_key='max_mean_conf', xlabel='Model Confidence', figsize=(6,5)):
    new_key = (meter_scale, dataset_index, grid_size, random_state, 'emd_p1')
    tv_conf = all_data['tv_'+data_key][new_key]
    te_conf = all_data['te_'+data_key][new_key]
    tv_err = all_data['tv_err'][new_key]
    te_err = all_data['te_err'][new_key]
    threshold, (trf, tvf, tef) = get_separation(meter_scale,dataset_index, grid_size, random_state, 'max_mean_conf', threshold, upper_threshold=True)
    fig, axs = plt.subplots(2,1, sharex=True, sharey=True, figsize=figsize)
    axs[0].scatter(tv_conf, tv_err, label='Validation', c='tab:blue', s=2)
    axs[0].plot([threshold, threshold], [2,1000], c='tab:blue')
    axs[0].set_ylabel('Error [m]')
    axs[0].set_xlabel(xlabel)
    axs[0].set_title('Validation Set')
    axs[1].scatter(te_conf, te_err, label='Test', c='tab:orange', s=2)
    axs[1].plot([threshold, threshold], [2,1000], c='tab:blue')
    axs[1].set_ylabel('Error [m]')
    axs[1].set_xlabel(xlabel)
    axs[1].set_title('OOD Test Set')
    axs[0].set_yscale('log')
    axs[0].set_yticks([10,100,1000])
    print(f'Tv Corr: {spearmanr(tv_conf, tv_err)}\tTe Corr: {spearmanr(te_conf, te_err)}')
    print(f'trf: {trf}\ttvf: {tvf}\ttef: {tef}')
    plt.tight_layout()
    fig.savefig(f'plots/dyspan2024_plots/{data_key}_err_scatter_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.png',dpi=dpi, bbox_inches='tight')
    fig.savefig(f'plots/dyspan2024_plots/{data_key}_err_scatter_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.pdf', bbox_inches='tight')
    plt.cla()
    plt.close()

def plot_lowconf_id(meter_scale, dataset_index, grid_size, random_state=0, dataset='tr', with_pred=False, figsize=(3,3),single_image=False):
    new_key = (meter_scale, dataset_index, grid_size, random_state, 'emd_p1')
    lowconf_inds = np.argpartition(all_data[dataset+'_max_mean_conf'][new_key], num_images)[:num_images]
    truth = all_data[dataset+'_truth'][new_key][lowconf_inds]
    preds = all_data[dataset+'_preds'][new_key][lowconf_inds][:,:2]
    for i in range(num_images):
        fig = plt.figure(figsize=figsize)
        plt.imshow(all_data[dataset+'_inputs_lowconf'][new_key][i], origin='lower')
        if with_pred:
            plt.scatter(preds[i,0]/meter_scale, preds[i,1]/meter_scale, marker='x', c='red', label='Prediction', alpha=0.7)
        plt.scatter(truth[i,0]/meter_scale, truth[i,1]/meter_scale, marker='^', c='white', label='Truth', alpha=0.7)
        preds_str = 'with_preds_' if with_pred else ''
        #plt.title('Max RSS: %.3f' % (all_data[dataset+'_inputs_lowconf'][new_key][i].max()))
        single_str = ''
        if single_image:
            plt.legend(loc='lower right')
            plt.ylim(13,68)
            plt.xlim(0,55)
            single_str = '_single_img'
        plt.axis('off')
        plt.tight_layout()
        fig.savefig(f'plots/dyspan2024_plots/images/{dataset}_lowconf_input_{preds_str}{single_image}{i}_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.png',dpi=dpi, bbox_inches='tight', pad_inches=0)
        fig.savefig(f'plots/dyspan2024_plots/images/{dataset}_lowconf_input_{preds_str}{single_image}{i}_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.pdf', bbox_inches='tight', pad_inches=0)
        plt.cla()
        plt.close()

def plot_custom_dissertation_lowconf_id(meter_scale, dataset_index, grid_size, random_state=0, dataset='tr', with_pred=False, figsize=(5,3),single_image=False):
    new_key = (meter_scale, dataset_index, grid_size, random_state, 'emd_p1')
    lowconf_inds = np.argpartition(all_data[dataset+'_max_mean_conf'][new_key], num_images)[:num_images]
    truth = all_data[dataset+'_truth'][new_key][lowconf_inds]
    preds = all_data[dataset+'_preds'][new_key][lowconf_inds][:,:2]
    for i in range(num_images):
        fig = plt.figure(figsize=figsize)
        cmap = plt.imshow(all_data[dataset+'_inputs_lowconf'][new_key][i] * (5 + 112) - 112, origin='lower', interpolation='none')
        plt.colorbar(cmap, label='RSS [dB]')
        #if with_pred:
        #    plt.scatter(preds[i,0]/meter_scale, preds[i,1]/meter_scale, marker='x', c='red', label='Prediction', alpha=0.7)
        plt.scatter(truth[i,0]/meter_scale, truth[i,1]/meter_scale, marker='^', c='white', label='Tx Location', alpha=0.7)
        preds_str = 'with_preds_' if with_pred else ''
        #plt.title('Max RSS: %.3f' % (all_data[dataset+'_inputs_lowconf'][new_key][i].max()))
        single_str = ''
        if single_image:
            plt.legend(loc='lower right')
            plt.ylim(13,68)
            plt.xlim(0,55)
            single_str = '_single_img'
        plt.axis('off')
        plt.tight_layout()
        fig.savefig(f'plots/dissertation/{dataset}_lowconf_input_{preds_str}{single_image}{i}_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.png',dpi=dpi, bbox_inches='tight', pad_inches=0)
        fig.savefig(f'plots/dissertation/{dataset}_lowconf_input_{preds_str}{single_image}{i}_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.pdf', bbox_inches='tight', pad_inches=0)
        plt.cla()
        plt.close()

def plot_higherror_id(meter_scale, dataset_index, grid_size, random_state=0, dataset='tr', with_pred=False, figsize=(3,3), single_image=False):
    new_key = (meter_scale, dataset_index, grid_size, random_state, 'emd_p1')
    higherror_inds = np.argpartition(all_data[dataset+'_err'][new_key], -num_images)[-num_images:]
    truth = all_data[dataset+'_truth'][new_key][higherror_inds]
    preds = all_data[dataset+'_preds'][new_key][higherror_inds][:,:2]
    for i in range(num_images):
        fig = plt.figure(figsize=figsize)
        plt.imshow(all_data[dataset+'_inputs_higherr'][new_key][i], origin='lower')
        if with_pred:
            plt.scatter(preds[i,0]/meter_scale, preds[i,1]/meter_scale, marker='x', c='red', label='Prediction', alpha=0.7)
        plt.scatter(truth[i,0]/meter_scale, truth[i,1]/meter_scale, marker='^', c='white', label='Truth', alpha=0.7)
        preds_str = 'with_preds_' if with_pred else ''
        #plt.title('Max RSS: %.3f' % (all_data[dataset+'_inputs_higherr'][new_key][i].max()))
        single_str = ''
        if single_image:
            plt.legend(loc='lower right')
            plt.ylim(13,68)
            plt.xlim(12,67)
            single_str = '_single_img'
        plt.axis('off')
        plt.tight_layout()
        fig.savefig(f'plots/dyspan2024_plots/images/{dataset}_higherr_input_{preds_str}{single_image}{i}_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.png',dpi=dpi, bbox_inches='tight', pad_inches=0)
        fig.savefig(f'plots/dyspan2024_plots/images/{dataset}_higherr_input_{preds_str}{single_image}{i}_{meter_scale}_{dataset_index}_{grid_size}_{random_state}.pdf', bbox_inches='tight', pad_inches=0)
        plt.cla()
        plt.close()

def plot_specific_higherr(i=5):
    key = (30, 6, 5, 0, 'emd_p1')
    fig = plt.figure(figsize=(3,3))
    higher_inds = np.argpartition(all_data['te_err'][key], -num_images)[-num_images:]
    plt.imshow(all_data['te_inputs_higherr'][key][16], origin='lower')
    #plt.scatter(663/30, 1832/30, c='red', marker='x', label='Prediction')
    #plt.scatter(1155/30, 2072/30, c='white', marker='^', label='Truth')
    plt.axis('off')
    plt.tight_layout()
    plt.legend(loc='lower right')
    plt.ylim(19,74)
    plt.xlim(5,60)
    fig.savefig(f'plots/dyspan2024_plots/images/specific_te_higherr_input.png',dpi=dpi, bbox_inches='tight', pad_inches=0)
    fig.savefig(f'plots/dyspan2024_plots/images/specific_te_higherr_input.pdf', bbox_inches='tight', pad_inches=0)
    plt.cla()
    plt.close()


def get_separation(meter_scale, dataset_index, grid_size, random_state, data_key, percentile=.1, use_tv_for_thresh=True, upper_threshold=True, two_inputs=False):
    new_key = (meter_scale, dataset_index, grid_size, random_state, 'emd_p1')
    if not two_inputs:
        data_key, upper_threshold = [data_key], [upper_threshold]
    tr_inds = np.zeros_like(all_data['tr_'+data_key[0]][new_key]).astype(bool)
    tv_inds = np.zeros_like(all_data['tv_'+data_key[0]][new_key]).astype(bool)
    te_inds = np.zeros_like(all_data['te_'+data_key[0]][new_key]).astype(bool)
    thresholds = []
    for key, upper in zip(data_key, upper_threshold):
        tr_conf = all_data['tr_'+key][new_key]
        tv_conf = all_data['tv_'+key][new_key]
        te_conf = all_data['te_'+key][new_key]
        threshold = np.quantile(tv_conf, percentile if upper else 1 - percentile) if use_tv_for_thresh else np.quantile(tr_conf, percentile if upper else 1 - percentile)
        if upper:
            tr_inds += (tr_conf < threshold)
            tv_inds += (tv_conf < threshold)
            te_inds += (te_conf < threshold)
        else:
            tr_inds += (tr_conf > threshold)
            tv_inds += (tv_conf > threshold)
            te_inds += (te_conf > threshold)
        tr_percent_filtered = tr_inds.sum() / len(tr_inds)
        tv_percent_filtered = tv_inds.sum() / len(tv_inds)
        te_percent_filtered = te_inds.sum() / len(te_inds)
        thresholds.append(threshold)
    return thresholds, (tr_percent_filtered, tv_percent_filtered, te_percent_filtered)

def plot_separation_knees(gridsizes=2, meter_scale=30, dataset_index=6, figsize=(6,3.5)):
    num_points = 300
    if isinstance(gridsizes, int):
        gridsizes = [gridsizes]
    fig = plt.figure(figsize=figsize)
    for gridsize in gridsizes:
        both_filtered = np.zeros((5, num_points))
        conf_filtered = np.zeros((5, num_points))
        spread_filtered = np.zeros((5, num_points))
        for randstate in range(5):
            te_filtered = [get_separation(meter_scale,dataset_index, gridsize, randstate, ['max_mean_conf', 'spread_sum'], i, upper_threshold=[True, False], two_inputs=True)[1][2] for i in np.linspace(0,0.5,num_points)]
            both_filtered[randstate] = te_filtered
            te_filtered = [get_separation(meter_scale,dataset_index, gridsize, randstate, 'max_mean_conf', i, upper_threshold=True)[1][2] for i in np.linspace(0,0.5,num_points)]
            conf_filtered[randstate] = te_filtered
            te_filtered = [get_separation(meter_scale,dataset_index, gridsize, randstate, 'spread_sum', i, upper_threshold=False)[1][2] for i in np.linspace(0,0.5,num_points)]
            spread_filtered[randstate] = te_filtered
        plt.plot(np.linspace(0,0.5,num_points)*100, 100*conf_filtered.mean(axis=0), linestyle='--', label='Confidence Filtering')
        plt.plot(np.linspace(0,0.5,num_points)*100, 100*spread_filtered.mean(axis=0), linestyle=':', label='Spread Filtering')
        plt.plot(np.linspace(0,0.5,num_points)*100, 100*both_filtered.mean(axis=0), linestyle='-', label='Double Filtering')
    plt.xlabel('Percent of Validation Set Removed')
    plt.ylabel('Percent of OOD Set Removed')
    plt.legend()
    plt.tight_layout()
    fig.savefig(f'plots/dyspan2024_plots/filtering_knee_{meter_scale}_{dataset_index}_{gridsize}.png',dpi=dpi, bbox_inches='tight')
    fig.savefig(f'plots/dyspan2024_plots/filtering_knee_{meter_scale}_{dataset_index}_{gridsize}.pdf', bbox_inches='tight')
    plt.cla()
    plt.close()
    return both_filtered, conf_filtered, spread_filtered

def plot_filter_comparison_knees(gridsizes=2, meter_scale=30, dataset_index=6, figsize=(6,2.7)):
    num_points = 300
    if isinstance(gridsizes, int):
        gridsizes = [gridsizes]
    fig, ax = plt.subplots(1,1, figsize=figsize)
    for gridsize in gridsizes:
        max_conf_filtered = np.zeros((5, num_points))
        mean_conf_filtered = np.zeros((5, num_points))
        max_spread_filtered = np.zeros((5, num_points))
        sum_spread_filtered = np.zeros((5, num_points))
        for randstate in range(5):
            te_filtered = [get_separation(meter_scale,dataset_index, gridsize, randstate, 'max_conf', i, upper_threshold=True)[1][2] for i in np.linspace(0,0.5,num_points)]
            max_conf_filtered[randstate] = te_filtered
            te_filtered = [get_separation(meter_scale,dataset_index, gridsize, randstate, 'max_mean_conf', i, upper_threshold=True)[1][2] for i in np.linspace(0,0.5,num_points)]
            mean_conf_filtered[randstate] = te_filtered
            te_filtered = [get_separation(meter_scale,dataset_index, gridsize, randstate, 'spread_max', i, upper_threshold=False)[1][2] for i in np.linspace(0,0.5,num_points)]
            max_spread_filtered[randstate] = te_filtered
            te_filtered = [get_separation(meter_scale,dataset_index, gridsize, randstate, 'spread_sum', i, upper_threshold=False)[1][2] for i in np.linspace(0,0.5,num_points)]
            sum_spread_filtered[randstate] = te_filtered
        plt.plot(np.linspace(0,0.5,num_points), max_conf_filtered.mean(axis=0), linestyle='-.', label='Max Pred')
        plt.plot(np.linspace(0,0.5,num_points), mean_conf_filtered.mean(axis=0), linestyle='-', label='Max Mean Pred')
        plt.plot(np.linspace(0,0.5,num_points), max_spread_filtered.mean(axis=0), linestyle='--', label='Max Spread')
        plt.plot(np.linspace(0,0.5,num_points), sum_spread_filtered.mean(axis=0), linestyle=':', label='Sum Spread')
    plt.xlabel('Low Conf. Rate (Validation)')
    ax.yaxis.set_major_formatter(tick.PercentFormatter(xmax=1,decimals=0))
    plt.ylabel('Low Conf. Rate (OOD)')
    ax.xaxis.set_major_formatter(tick.PercentFormatter(xmax=1,decimals=0))
    plt.legend()
    plt.tight_layout()
    fig.savefig(f'plots/dyspan2024_plots/filtering_comparison_knee_{meter_scale}_{dataset_index}_{gridsize}.png',dpi=dpi, bbox_inches='tight')
    fig.savefig(f'plots/dyspan2024_plots/filtering_comparison_knee_{meter_scale}_{dataset_index}_{gridsize}.pdf', bbox_inches='tight')
    plt.cla()
    plt.close()

def plot_separation_grid_sizes(meter_scale=30, dataset_index=6, filter_size=0.1, figsize=(6,3.5)):
    fig = plt.figure(figsize=figsize)
    gridsizes = [2,3,4,5,7,10,15]
    both_filtered = np.zeros((5, len(gridsizes)))
    conf_filtered = np.zeros((5, len(gridsizes)))
    spread_filtered = np.zeros((5, len(gridsizes)))
    for i, gridsize in enumerate(gridsizes):
        for randstate in range(5):
            both_filtered[randstate, i] = get_separation(meter_scale,dataset_index, gridsize, randstate, ['max_mean_conf', 'spread_sum'], filter_size, upper_threshold=[True, False], two_inputs=True)[1][2] 
            conf_filtered[randstate, i] = get_separation(meter_scale,dataset_index, gridsize, randstate, 'max_mean_conf', filter_size, upper_threshold=True)[1][2]
            spread_filtered[randstate, i] = get_separation(meter_scale,dataset_index, gridsize, randstate, 'spread_sum', filter_size, upper_threshold=False)[1][2]
    plt.plot(gridsizes, 100*conf_filtered.mean(axis=0), label='Confidence Filtering', linestyle='--')
    plt.plot(gridsizes, 100*spread_filtered.mean(axis=0), label='Spread Filtering', linestyle=':')
    plt.plot(gridsizes, 100*both_filtered.mean(axis=0), label='Double Filtering', linestyle='-')
    plt.xlabel('Grid Size')
    plt.ylabel('Percent of OOD Set Removed')
    plt.legend()
    plt.tight_layout()
    fig.savefig(f'plots/dyspan2024_plots/filtering_gridsize_{meter_scale}_{dataset_index}_{filter_size}.png',dpi=dpi, bbox_inches='tight')
    fig.savefig(f'plots/dyspan2024_plots/filtering_gridsize_{meter_scale}_{dataset_index}_{filter_size}.pdf', bbox_inches='tight')
    plt.cla()
    plt.close()
    return both_filtered, conf_filtered, spread_filtered

def plot_combined_error(meter_scale=30, dataset_index=6, filter_size=0.1, filtering='max_mean_conf', filtered_only=False, figsize=(6,4.6)):
    assert filtering in ['both', 'max_mean_conf', 'spread_sum']
    fig, axs = plt.subplots(2,1, figsize=figsize)
    thresholds = []
    gridsizes = [2,3,4,5,7,10]
    cutl_med = np.zeros((5,len(gridsizes)))
    range_med = np.zeros((5,len(gridsizes)))
    comb_med = np.zeros((5,len(gridsizes)))
    tv_cutl_med = np.zeros((5,len(gridsizes)))
    tv_range_med = np.zeros((5,len(gridsizes)))
    tv_comb_med = np.zeros((5,len(gridsizes)))
    for i, gridsize in enumerate(gridsizes):
        for randstate in range(5):
            new_key = (meter_scale, dataset_index, gridsize, randstate, 'emd_p1')
            if filtering == 'both':
                thresholds.append(get_separation(meter_scale,dataset_index, gridsize, randstate, ['max_mean_conf', 'spread_sum'], filter_size, upper_threshold=[True, False], two_inputs=True)[0])
                comb_tv_preds, comb_tv_err, tv_filter_inds = get_combined_preds_and_err(
                    ml_preds=all_data['tv_preds'][new_key],
                    range_preds=all_data['tv_splot_preds'][new_key],
                    truth=all_data['tv_truth'][new_key],
                    conf=all_data['tv_max_mean_conf'][new_key],
                    spread=all_data['tv_spread_sum'][new_key],
                    conf_thresh=thresholds[-1][0],
                    spread_thresh=thresholds[-1][1]
                )
                comb_te_preds, comb_te_err, te_filter_inds = get_combined_preds_and_err(
                    ml_preds=all_data['te_preds'][new_key],
                    range_preds=all_data['te_splot_preds'][new_key],
                    truth=all_data['te_truth'][new_key],
                    conf=all_data['te_max_mean_conf'][new_key],
                    spread=all_data['te_spread_sum'][new_key],
                    conf_thresh=thresholds[-1][0],
                    spread_thresh=thresholds[-1][1]
                )
            elif filtering == 'max_mean_conf':
                thresholds.append(get_separation(meter_scale,dataset_index, gridsize, randstate, 'max_mean_conf', filter_size, upper_threshold=True)[0])
                comb_tv_preds, comb_tv_err, tv_filter_inds = get_combined_preds_and_err(
                    ml_preds=all_data['tv_preds'][new_key],
                    range_preds=all_data['tv_splot_preds'][new_key],
                    truth=all_data['tv_truth'][new_key],
                    conf=all_data['tv_max_mean_conf'][new_key],
                    conf_thresh=thresholds[-1][0],
                )
                comb_te_preds, comb_te_err, te_filter_inds = get_combined_preds_and_err(
                    ml_preds=all_data['te_preds'][new_key],
                    range_preds=all_data['te_splot_preds'][new_key],
                    truth=all_data['te_truth'][new_key],
                    conf=all_data['te_max_mean_conf'][new_key],
                    conf_thresh=thresholds[-1][0],
                )
            elif filtering == 'spread_sum':
                thresholds.append(get_separation(meter_scale,dataset_index, gridsize, randstate, 'spread_sum', filter_size, upper_threshold=False)[0])
                comb_tv_preds, comb_tv_err, tv_filter_inds = get_combined_preds_and_err(
                    ml_preds=all_data['tv_preds'][new_key],
                    range_preds=all_data['tv_splot_preds'][new_key],
                    truth=all_data['tv_truth'][new_key],
                    spread=all_data['tv_spread_sum'][new_key],
                    spread_thresh=thresholds[-1][1]
                )
                comb_te_preds, comb_te_err, te_filter_inds = get_combined_preds_and_err(
                    ml_preds=all_data['te_preds'][new_key],
                    range_preds=all_data['te_splot_preds'][new_key],
                    truth=all_data['te_truth'][new_key],
                    spread=all_data['te_spread_sum'][new_key],
                    spread_thresh=thresholds[-1][1]
                )
            filter_inds = filter_inds if filtered_only else np.ones(len(comb_te_err)).astype(bool)
            cutl_med[randstate,i] = np.median(all_data['te_err'][new_key][filter_inds])
            range_med[randstate,i] = np.median(all_data['te_splot_err'][new_key][filter_inds])
            comb_med[randstate,i] = np.median(comb_te_err[filter_inds])
            tv_cutl_med[randstate,i] = np.median(all_data['tv_err'][new_key])
            tv_range_med[randstate,i] = np.median(all_data['tv_splot_err'][new_key])
            tv_comb_med[randstate,i] = np.median(comb_tv_err)

    axs[0].plot(gridsizes, tv_cutl_med.mean(axis=0), label='CNN', linestyle='--')
    axs[0].plot(gridsizes, tv_range_med.mean(axis=0), label='Ranging', linestyle=':')
    axs[0].plot(gridsizes, tv_comb_med.mean(axis=0), label='Hybrid', linestyle='-')
    axs[1].plot(gridsizes, cutl_med.mean(axis=0), label='CNN', linestyle='--')
    axs[1].plot(gridsizes, range_med.mean(axis=0), label='Ranging', linestyle=':')
    axs[1].plot(gridsizes, comb_med.mean(axis=0), label='Hybrid', linestyle='-')
    axs[0].set_xlabel('Grid Size [$N\\times N$]')
    axs[0].set_ylabel('Val. Med. Error [m]')
    axs[1].set_xlabel('Grid Size [$N\\times N$]')
    axs[1].set_ylabel('OOD Med. Error [m]')
    axs[0].legend(loc='upper right')
    axs[1].legend(loc='upper right')
    plt.tight_layout()
    low_conf = 'low_conf_' if filtered_only else ''
    fig.savefig(f'plots/dyspan2024_plots/combined_accuracy_lines_{low_conf}{meter_scale}_{dataset_index}_{filter_size}.png',dpi=dpi, bbox_inches='tight')
    fig.savefig(f'plots/dyspan2024_plots/combined_accuracy_lines_{low_conf}{meter_scale}_{dataset_index}_{filter_size}.pdf', bbox_inches='tight')
    plt.cla()
    plt.close()

def get_combined_preds_and_err(ml_preds, range_preds, truth, conf=None, spread=None, conf_thresh=None, spread_thresh=None):
    combined_preds = ml_preds[:,:2].copy()
    assert (conf is not None and conf_thresh is not None) or (spread is not None and spread_thresh is not None)
    filtered_inds = np.zeros(len(combined_preds)).astype(bool)
    if conf is not None:
        assert conf_thresh is not None
        filtered_inds += conf < conf_thresh
    if spread is not None:
        assert spread_thresh is not None
        filtered_inds += spread > spread_thresh
    combined_preds[filtered_inds] = range_preds[filtered_inds, :2]
    err = np.linalg.norm(combined_preds - truth, axis=1)
    return combined_preds, err, filtered_inds

def get_dist_report(meter_scale=30, dataset_index=6, use_median=True, filter_size=0.1, figsize=(6,3)):
    gridsizes = [2,3,4,5,7,10,15]
    use_range = np.zeros((5, len(gridsizes))).astype(bool)
    for i, gridsize in enumerate(gridsizes):
        for randstate in range(5):
            new_key = (meter_scale, dataset_index, gridsize, randstate, 'emd_p1')
            dist = all_data['tr_dist'][new_key]
            tr_range_err = all_data['tr_splot_err'][new_key]
            tr_err = all_data['tr_err'][new_key]
            te_err = all_data['te_err'][new_key]
            thresholds = get_separation(meter_scale,dataset_index, gridsize, randstate, ['max_mean_conf', 'spread_sum'], filter_size, upper_threshold=[True, False], two_inputs=True)[0]
            comb_preds, comb_err, filter_inds = get_combined_preds_and_err(
                ml_preds=all_data['te_preds'][new_key],
                range_preds=all_data['te_splot_preds'][new_key],
                truth=all_data['te_truth'][new_key],
                conf=all_data['te_max_mean_conf'][new_key],
                spread=all_data['te_spread_sum'][new_key],
                conf_thresh=thresholds[0],
                spread_thresh=thresholds[1]
            )
            print(new_key, 'Med', np.median(dist)+np.median(tr_err), np.median(tr_range_err))
            print('(xx, x, xx, x)', np.mean(te_err), np.mean(comb_err))
            if use_median:
                use_range[randstate, i] = np.median(dist) > np.median(tr_range_err)
            else:
                use_range[randstate, i] = np.mean(dist) > np.mean(tr_range_err)
    return use_range

def get_seasonal_stats(loss='emd_p1', num_samples=4):
    jkeys = [(30,6,'july_selftest_nov_limited',i, loss) for i in range(num_samples)]
    nkeys = [(30,6,'nov_selftest_july_limited',i, loss) for i in range(num_samples)]
    jakeys = [(30,6,'july_selftest_april_limited',i, loss) for i in range(num_samples)]
    nakeys = [(30,6,'nov_selftest_april_limited',i, loss) for i in range(num_samples)]
    j_jerrs = [all_data['te_err'][k] for k in jkeys ]
    n_jerrs = [all_data['te_err'][k] for k in nkeys ]
    j_nerrs = [all_data['tv_err'][k] for k in jkeys ]
    n_nerrs = [all_data['tv_err'][k] for k in nkeys ]
    j_aerrs = [all_data['te_err'][k] for k in jakeys ]
    n_aerrs = [all_data['te_err'][k] for k in nakeys ]
    j_jconfs = [all_data['te_max_mean_conf'][k] for k in jkeys ]
    n_jconfs = [all_data['te_max_mean_conf'][k] for k in nkeys ]
    j_nconfs = [all_data['tv_max_mean_conf'][k] for k in jkeys ]
    n_nconfs = [all_data['tv_max_mean_conf'][k] for k in nkeys ]
    j_aconfs = [all_data['te_max_mean_conf'][k] for k in jakeys ]
    n_aconfs = [all_data['te_max_mean_conf'][k] for k in nakeys ]

    #fig, axs = plt.subplots(2,4)
    #for i, key in enumerate(jkeys+nkeys):
    #    axs.flat[i].imshow(all_data['te_images_first20'][key][0].mean(axis=0))
    #    axs.flat[i].set_title(key[2])
    #plt.show( 
    for i in range(num_samples):
        print('\n\n', i)
        jthreshold = np.quantile(j_jconfs[i], 0.1)
        print('July Model, Nov % Filtered', (j_nconfs[i] < jthreshold).sum() / len(j_nconfs[i]))
        nthreshold = np.quantile(n_nconfs[i], 0.1)
        print('Nov Model, July % Filtered', (n_jconfs[i] < nthreshold).sum() / len(n_jconfs[i]))
        print('J_J mean', np.mean(j_jerrs[i]))
        print('J_J med', np.median(j_jerrs[i]))
        print('N_N mean', np.mean(n_nerrs[i]))
        print('N_N med', np.median(n_nerrs[i]))
        print('J_N mean', np.mean(j_nerrs[i]))
        print('J_N med', np.median(j_nerrs[i]))
        print('N_J mean', np.mean(n_jerrs[i]))
        print('N_J med', np.median(n_jerrs[i]))
        print('J_A mean', np.mean(j_aerrs[i]))
        print('J_A med', np.median(j_aerrs[i]))
        print('N_A mean', np.mean(n_aerrs[i]))
        print('N_A med', np.median(n_aerrs[i]))
        print('J_N lowconf mean', j_nerrs[i][j_nconfs[i] < jthreshold].mean())
        print('J_N higconf mean', j_nerrs[i][j_nconfs[i] > jthreshold].mean())
        print('J_N lowconf med', np.median(j_nerrs[i][j_nconfs[i] < jthreshold]))
        print('J_N higconf med', np.median(j_nerrs[i][j_nconfs[i] > jthreshold]))
        print('N_J lowconf mean', n_jerrs[i][n_jconfs[i] < nthreshold].mean())
        print('N_J higconf mean', n_jerrs[i][n_jconfs[i] > nthreshold].mean())
        print('N_J lowconf med', np.median(n_jerrs[i][n_jconfs[i] < nthreshold]))
        print('N_J higconf med', np.median(n_jerrs[i][n_jconfs[i] > nthreshold]))
        print('Hybrid')
        ncomb_errs = j_nerrs[i].copy()
        jcomb_errs = j_jerrs[i].copy()
        acomb_errs = j_aerrs[i].copy()
        n_nnorm_conf = n_nconfs[i] / nthreshold
        j_nnorm_conf = j_nconfs[i] / jthreshold
        n_jnorm_conf = n_jconfs[i] / nthreshold
        j_jnorm_conf = j_jconfs[i] / jthreshold
        n_anorm_conf = n_aconfs[i] / nthreshold
        j_anorm_conf = j_aconfs[i] / jthreshold
        ncomb_errs[n_nnorm_conf > j_nnorm_conf] = n_nerrs[i][n_nnorm_conf > j_nnorm_conf]
        jcomb_errs[n_jnorm_conf > j_jnorm_conf] = n_jerrs[i][n_jnorm_conf > j_jnorm_conf]
        acomb_errs[n_anorm_conf > j_anorm_conf] = n_aerrs[i][n_anorm_conf > j_anorm_conf]
        print(f'Hybrid nov_mean', np.mean(ncomb_errs))
        print(f'Hybrid nov_med', np.median(ncomb_errs))
        print(f'Hybrid july_mean', np.mean(jcomb_errs))
        print(f'Hybrid july_med', np.median(jcomb_errs))
        print(f'Hybrid april_mean', np.mean(acomb_errs))
        print(f'Hybrid april_med', np.median(acomb_errs))
        print()

def get_grid_stats():
    global all_data
    for grid_size in [2,5]:
        print(f'\nGridsize: {grid_size}')
        keys = [(30,6,grid_size,i, 'emd_p1') for i in range(5)]
        errs = {}
        confs = {}
        range_errs = {}
        errs['tr'] = [all_data['tr_err'][k] for k in keys ]
        errs['tv'] = [all_data['tv_err'][k] for k in keys ]
        errs['te'] = [all_data['te_err'][k] for k in keys ]
        confs['tr'] = [all_data['tr_max_mean_conf'][k] for k in keys ]
        confs['tv'] = [all_data['tv_max_mean_conf'][k] for k in keys ]
        confs['te'] = [all_data['te_max_mean_conf'][k] for k in keys ]
        range_errs['tr'] = [all_data['tr_splot_err'][k] for k in keys ]
        range_errs['tv'] = [all_data['tv_splot_err'][k] for k in keys ]
        range_errs['te'] = [all_data['te_splot_err'][k] for k in keys ]

        thresholds = [np.quantile(confs['tv'][i], 0.1) for i in range(5)]
        for key in ['tr', 'tv', 'te']:
            print(f'CUTL {key}_mean', [np.mean(errs[key][i]) for i in range(5)])
            print(f'CUTL {key}_med', [np.median(errs[key][i]) for i in range(5)])
            print(f'Range {key}_mean', [np.mean(range_errs[key][i]) for i in range(5)])
            print(f'Range {key}_med', [np.median(range_errs[key][i]) for i in range(5)])
            comb_errs = [err.copy() for err in errs[key]]
            for i in range(5):
                comb_errs[i][confs[key][i] < thresholds[i]] = range_errs[key][i][confs[key][i] < thresholds[i]]
            print(f'Hybrid {key}_mean', [np.mean(comb_errs[i]) for i in range(5)])
            print(f'Hybrid {key}_med', [np.median(comb_errs[i]) for i in range(5)])
            print()

def make_dyspan_loss_plot(figsize=(6,3)):
    with open('dyspanlog.txt', 'r') as f:
        lines = f.readlines()
    start_inds = [i for i in range(len(lines)) if lines[i].startswith('Ep1 Train Loss')]
    start_inds.append(-1)
    groups = []
    for i, start_ind in enumerate(start_inds[:-1]):
        tmp_lines = lines[start_ind:start_inds[i+1]]
        train, test, train_val = [], [], [] 
        for line in tmp_lines:
            if line[:2] != 'Ep': continue
            split_line = line.split()
            if split_line[1] == 'Train':
                current_train = float(split_line[5])
            else:
                train.append(current_train)
                test.append(float(split_line[3][5:]))
                train_val.append(float(split_line[11][5:]))
        groups.append(np.stack((train, test, train_val)))
    tab_colors = list(mcolors.TABLEAU_COLORS.keys())
    markers = ["+", "*", "x", "d", "1", 'v', 6, 7]
    linestyles = ['solid', 'dotted', 'dashed', 'dashdot', (0,(1,5)),(0,(5,8)),(0,(3,5,1,5,1,5)),]
    com = groups[0+3]
    emd = groups[2+3]
    fig = plt.figure(figsize=figsize)
    plt.plot(com[0], label='CoM Train', linestyle=linestyles[4])
    plt.plot(com[2], label='CoM Validation', linestyle=linestyles[2])
    plt.plot(com[1], label='CoM OOD Test', linestyle=linestyles[1])
    plt.plot(emd[0], label='EMD Train', linestyle=linestyles[3])
    plt.plot(emd[2], label='EMD Validation', linestyle=linestyles[5])
    plt.plot(emd[1], label='EMD OOD Test', linestyle=linestyles[0])
    plt.xlabel('Training Epoch')
    plt.ylabel('Mean Error [m]')
    plt.legend(ncol=2)
    plt.tight_layout(pad=0.1)
    #plt.yscale('log')
    plt.show()
    fig.savefig('plots/dyspan2024_plots/training_error_plot.pdf', bbox_inches='tight')
    #embed()

all_data = get_data()
#plot_filter_comparison_knees()
#plot_combined_error()
#get_seasonal_terminal()
#get_dataset_plots_and_table()
#plot_predictions_within_training_area(30, 6, 3, 0)
#plot_predictions_within_training_area(150, 7, 3, 0)
#plot_data_error_scatter(30, 6, 3, 0, data_key='max_mean_conf', xlabel='Max Confidence')
#plot_data_error_scatter(30, 6, 5, 0, data_key='max_mean_conf', xlabel='Max Confidence')
#plot_data_error_scatter(150, 7, 3, 0, data_key='max_mean_conf', xlabel='Max Confidence')
#plot_data_error_scatter(150, 7, 5, 0, data_key='max_mean_conf', xlabel='Max Confidence')
#plot_data_error_scatter(30, 6, 3, 0, data_key='spread_sum', xlabel='Spread')
#plot_data_error_scatter(30, 6, 5, 0, data_key='spread_sum', xlabel='Spread')
#plot_data_error_scatter(150, 7, 3, 0, data_key='spread_sum', xlabel='Spread')
#plot_data_error_scatter(150, 7, 5, 0, data_key='spread_sum', xlabel='Spread')
#plot_lowconf_id(30, 6, 5, 0)
#plot_lowconf_id(150, 7, 5, 0)
#plot_higherror_id(30, 6, 5, 0)
#plot_higherror_id(150, 7, 5, 0)
#botha, confa, spreada = plot_separation_knees(2, 30, 6)
#bothb, confb, spreadb = plot_separation_knees(2, 150, 7)
#both, conf, spread = plot_separation_grid_sizes(30, 6, 0.1)
#both2, conf2, spread2 = plot_separation_grid_sizes(150, 7, 0.1)
#print(both[:,0].mean())
#print(both2[:,0].mean())
#plot_combined_error()
#plot_combined_error(150,7)
#plot_filters_within_training_area(30, 6, 2, 0)
#plot_filters_within_training_area(30, 6, 5, 0)
#plot_filters_within_training_area(150, 7, 2, 0)
#plot_filters_within_training_area(150, 7, 5, 0)

embed()