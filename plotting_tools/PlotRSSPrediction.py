import numpy as np
import torch
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import os
from locconfig import LocConfig
from dataset import ImageDataset
from tirem_helper import load_tirem_alt_params, load_tirem_prop_map

plt.rcParams.update({'font.size': 14})
plt.rc('legend',fontsize=12) # using a size in points
plt.rc('text', usetex=True)
plt.rc('font', family='serif')

tab_colors = list(mcolors.TABLEAU_COLORS.keys())
markers = ["P", "*", "x", "d", 4, 5, 6, 7]
methods = ['linear', 'nearest', 'rbf', 'tirem_nn_rbf', 'celf', 'tirem', 'tirem_nn']
pretty_methods = ['Linear', 'Nearest', 'RBF', 'RBF/TIREM++', 'CELF', 'TIREM', 'TIREM++']
pretty_datasets = ['DS1', 'DS2', 'DS3']
linestyles = ['solid', 'dotted', 'dashed', 'dashdot']

def plot_loss_fields(grid_size=10):
    #for ds, min_sensors in [(6,15),(7,5),(8,4)]:
    for ds, min_sensors in [(6,15)]:
        ms = 25 if ds in [6,8] else 100
        params = LocConfig(ds, force_num_tx=1, min_sensors=min_sensors, data_split=f'grid{grid_size}', meter_scale=ms)
        imd = ImageDataset(params, random_state=0)
        imd.make_datasets(make_val=True)
        maps = imd.get_prop_maps('celf')
        for label in imd.celf:
            celf = imd.celf[label]
            nrows, ncols = len(celf.yVals), len(celf.xVals)
            ratio_field2chm = ms//np.array(imd.elevation_map.res)
            contour_x, contour_y = np.arange(0,ncols)*ratio_field2chm[0], np.arange(0,nrows)*ratio_field2chm[1]
            mesh_x, mesh_y = np.meshgrid(contour_x, contour_y)
            fig = plt.figure(figsize=(5,4))
            cmap = plt.contourf(mesh_x, mesh_y, celf.LossField.reshape(nrows, ncols), cmap='viridis')
            plt.colorbar(cmap, label='dB')
            if ds == 6:
                plt.ylim(bottom=700)
            plt.yticks([],[])
            plt.xticks([],[]) 
            plt.tight_layout(pad=0.1)
            plt.savefig(f'plots/mobicom2024_plots/rss_prediction/celf/celf_loss_field_{ds}_grid{grid_size}_{label}.pdf', bbox_inches='tight')
            plt.savefig(f'plots/mobicom2024_plots/rss_prediction/celf/celf_loss_field_{ds}_grid{grid_size}_{label}.png', bbox_inches='tight', dpi=300)
            plt.clf()


def get_all_pred_maps(grid_size=10, combined=True, figsize=(12,2.5), savefig=False):
    #make_methods = ['linear', 'rbf', 'tirem_nn', 'celf']
    make_methods = ['rbf', 'tirem', 'tirem_nn', 'celf']
    import time
    for ds, min_sensors in [(8,4)]:
    #for ds, min_sensors in [(6,15),(7,5),(8,4)]:
        ms = 3 if ds in [6,8] else 10
        params = LocConfig(ds, force_num_tx=1, min_sensors=min_sensors, data_split=f'grid{grid_size}', meter_scale=ms)
        imd = ImageDataset(params, random_state=0)
        imd.make_datasets(make_val=True)
        images = {}
        for method in make_methods:
            if method not in images:
                images[method] = {}
            print(method)
            if method == 'celf':
                meter_scale = 12 if ds in [6,8] else 100
                params = LocConfig(ds, force_num_tx=1, min_sensors=min_sensors, data_split=f'grid{grid_size}', meter_scale=meter_scale)
                imd = ImageDataset(params, random_state=0)
                imd.make_datasets(make_val=True)
            start = time.time()
            prop_results = imd.get_prop_maps(method)
            end = time.time()
            print('Total Time: ', end - start)
            prop_maps, keys, rx_locs, rx_types = prop_results[:4]
            rx_data = imd.get_rx_data_by_tx_location(imd.train_key, combine_sensors=True, required_limit=100)
            for prop_map, key, name in zip(prop_maps, keys, rx_data.keys()):
                key = key[0]
                min_rss, max_rss = imd.get_min_max_rss_from_key(key)
                max_rss = min(max_rss, -4)
                result = prop_map * (max_rss - min_rss) + min_rss
                images[method][name] = result
                if not combined:
                    fig = plt.figure(figsize=(5.5,4))
                    cmap = plt.imshow(result, origin='lower', vmin=min_rss, vmax=max_rss)
                    #if ds == 6:
                    #    plt.ylim(bottom=700/(ms*2))
                    plt.yticks([],[])
                    plt.xticks([],[]) 
                    plt.tight_layout(pad=0.1)
                    if savefig:
                        plt.show()
                        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/all_preds/pred_map_{ds}_grid{grid_size}_{key}_{name}_{method}.pdf', bbox_inches='tight')
                        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/all_preds/pred_map_{ds}_grid{grid_size}_{key}_{name}_{method}.png', bbox_inches='tight', dpi=300)
                        #plt.colorbar(cmap)
                        plt.clf()
                        plt.close()
                    else:
                        plt.show()
        if combined:
            ordered_methods = ['rbf', 'celf', 'tirem', 'tirem_nn']
            #ordered_methods = ['linear', 'rbf', 'celf', 'tirem_nn']
            for name in images[ordered_methods[0]]:
                fig, axs = plt.subplots(ncols=len(ordered_methods), figsize=figsize)
                minrss, maxrss = 0,-100
                for method in ordered_methods:
                    image = images[method][name]
                    minrss = min(minrss, image.min())
                    maxrss = max(maxrss, image.max())
                for i, method in enumerate(ordered_methods):
                    image = images[method][name]
                    if method == 'celf':
                        image = image[:-1,:-1]
                    cmap = axs[i].imshow(image, origin='lower', vmin=minrss, vmax=maxrss)
                    axs[i].set_yticks([],[])
                    axs[i].set_xticks([],[]) 
                plt.tight_layout(pad=0.1)
                cbar = fig.colorbar(cmap, ax=axs.ravel().tolist(), label='Pred RSS [dB]')
                if savefig:
                    plt.show()
                    fig.savefig(f'plots/mobicom2024_plots/rss_prediction/all_preds/pred_map_{ds}_grid{grid_size}_{name}_combined3.pdf', bbox_inches='tight')
                    fig.savefig(f'plots/mobicom2024_plots/rss_prediction/all_preds/pred_map_{ds}_grid{grid_size}_{name}_combined3.png', bbox_inches='tight', dpi=300)
                    #plt.colorbar(cmap)
                    plt.clf()
                    plt.close()
                else:
                    plt.show()


def plot_tirem_nn_features_preds(grid_size=10, sensors=None):
    for ds, min_sensors in [(6,15),(7,5),(8,4)]:
        ms = 3 if ds in [6,8] else 10
        params = LocConfig(ds, force_num_tx=1, min_sensors=min_sensors, data_split=f'grid{grid_size}', meter_scale=ms, include_elevation_map=True, augmentation='tirem_nn')
        imd = ImageDataset(params, random_state=0)
        imd.make_datasets(make_val=True)
        tirem_results = imd.get_prop_maps('tirem')
        prop_results = imd.get_prop_maps('tirem_nn')
        tirem_prop_maps = tirem_results[0]
        rx_data = imd.get_rx_data_by_tx_location(imd.train_key, combine_sensors=True, required_limit=100)
        prop_maps, keys, rx_locs, rx_types = prop_results[:4]
        for tirem_map, prop_map, key, name in zip(tirem_prop_maps, prop_maps, keys, rx_data.keys()):
            key = key[0]
            tirem_pred = load_tirem_prop_map(ds, name, tuple(imd.corners.flatten()))[::ms,::ms]
            tirem_features = load_tirem_alt_params(ds, name, tuple(imd.corners.flatten()))[::ms,::ms]
            min_rss, max_rss = imd.get_min_max_rss_from_key(key)
            for result, label in zip(
            [
                tirem_map * (max_rss - min_rss) + min_rss,
                prop_map * (max_rss - min_rss) + min_rss,
                tirem_pred
            ] + [tirem_features[:,:,ind] for ind in range(14)], (
                'tirem_pred', 'tirem_nn_pred', 'direct_tirem_pred', 
                'log_dist', 'diff_param', 'diff_param_obstacle', 
                'diff_angles1', 'diff_angles2', 'elev_angles_NLOS1',
                'elev_angles_NLOS2', 'elev_angles_txrx1', 'elev_angles_txrx2',
                'LOS', 'num_knife_edges', 'num_obstacles', 'shadowing_angles1',
                'shadowing_angles2'
            )):
                fig = plt.figure(figsize=(5.5,4))
                cmap = plt.imshow(result, origin='lower')
                plt.colorbar(cmap)
                if ds == 6:
                    plt.ylim(bottom=700/(ms*2))
                plt.yticks([],[])
                plt.xticks([],[]) 
                plt.tight_layout(pad=0.1)
                plt.savefig(f'plots/mobicom2024_plots/rss_prediction/tirem_nn_pred_{ds}_grid{grid_size}_{label}_{key}_{name}.pdf', bbox_inches='tight')
                plt.savefig(f'plots/mobicom2024_plots/rss_prediction/tirem_nn_pred_{ds}_grid{grid_size}_{label}_{key}_{name}.png', bbox_inches='tight', dpi=300)
                plt.clf()
                plt.close()
            
def plot_elevation_and_scatters(grid_sizes=[2,5,10]):
    for ds, min_sensors in [(6,15),(7,5),(8,4)]:
    #for ds, min_sensors in [(7,5)]:
        ms = 3 if ds in [6,8] else 10
        params = LocConfig(ds, force_num_tx=1, min_sensors=min_sensors, data_split=f'grid2', meter_scale=ms, include_elevation_map=True)
        imd = ImageDataset(params, random_state=0)
        imd.make_datasets(make_val=True)
        imd.make_elevation_tensors()
        fig = plt.figure(figsize=(5.5,4))
        if ds in [6,8]:
            vmin, vmax = (2, 45)
        else:
            vmin, vmax = (0.05, 0.5)
        if ds in [6,8]:
            cmap = plt.imshow(imd.building_tensors[0].cpu(), origin='lower', cmap='Greys', vmin=vmin, vmax=vmax)
        else:
            cmap = plt.imshow(imd.elevation_tensors[0].cpu(), origin='lower', cmap='Greys', norm=mcolors.LogNorm(vmin=vmin, vmax=vmax))
        if ds == 6:
            plt.ylim(bottom=700/(ms*2))
        elif ds == 8:
            plt.ylim(0, 456)
        plt.yticks([],[])
        plt.xticks([],[]) 
        plt.tight_layout(pad=0.1)
        plt.savefig(f'plots/mobicom2024_plots/rss_prediction/elevation_{ds}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/rss_prediction/elevation_{ds}.png', bbox_inches='tight', dpi=300)
        train_data = imd.data[imd.train_key]
        test_data = imd.data[imd.test_keys[0]]
        tr_tx_vecs = np.array(train_data.tx_vecs)[:,0]
        te_tx_vecs = np.array(test_data.tx_vecs)[:,0]
        tr_rx_vecs = np.array(train_data.ordered_dataloader.dataset.tensors[0].cpu())
        ds_limit = 1 if ds in [6,7] else -1
        tr_rx_vecs = np.unique(tr_rx_vecs[tr_rx_vecs[:,:,4] > ds_limit][:,1:3], axis=0)
        if ds == 7:
            tr_tx_vecs -= 140
            te_tx_vecs -= 140
            tr_rx_vecs -= 140/ms
        ylim = plt.ylim()
        xlim = plt.xlim()
        plt.scatter(tr_tx_vecs[:,0]/ms, tr_tx_vecs[:,1]/ms, s=4, alpha=0.4, color='tab:blue', label='Rx' if ds == 8 else 'Tx' )
        plt.scatter(te_tx_vecs[:,0]/ms, te_tx_vecs[:,1]/ms, s=4, alpha=0.4, color='tab:blue')
        plt.scatter(tr_rx_vecs[:,0], tr_rx_vecs[:,1], s=25, color='tab:green', marker='^', label='Tx' if ds == 8 else 'Rx')
        plt.ylim(ylim[0], ylim[1])
        plt.xlim(xlim[0], xlim[1])
        lgnd = plt.legend()
        for handle in lgnd.legend_handles:
            handle.set_sizes([30.0])
        plt.savefig(f'plots/mobicom2024_plots/rss_prediction/elevation_scatter_{ds}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/rss_prediction/elevation_scatter_{ds}.png', bbox_inches='tight', dpi=300)
        plt.clf()
        plt.close()
        for grid_size in grid_sizes:
            params = LocConfig(ds, force_num_tx=1, min_sensors=min_sensors, data_split=f'grid{grid_size}', meter_scale=ms, include_elevation_map=True)
            imd = ImageDataset(params, random_state=0)
            imd.make_datasets(make_val=True)
            imd.make_elevation_tensors()
            fig = plt.figure(figsize=(5.5,4))
            if ds in [6,8]:
                cmap = plt.imshow(imd.building_tensors[0].cpu(), origin='lower', cmap='Greys', vmin=vmin, vmax=vmax)
            else:
                cmap = plt.imshow(imd.elevation_tensors[0].cpu(), origin='lower', cmap='Greys', norm=mcolors.LogNorm(vmin=vmin, vmax=vmax))
            if ds == 6:
                plt.ylim(bottom=700/(ms*2))
            plt.yticks([],[])
            plt.xticks([],[]) 
            train_data = imd.data[imd.train_key]
            test_data = imd.data[imd.test_keys[0]]
            tr_tx_vecs = np.array(train_data.tx_vecs)[:,0]
            te_tx_vecs = np.array(test_data.tx_vecs)[:,0]
            ylim = plt.ylim()
            xlim = plt.xlim()
            if ds == 7:
                tr_tx_vecs -= 140
                te_tx_vecs -= 140
            plt.scatter(tr_tx_vecs[:,0]/ms, tr_tx_vecs[:,1]/ms, s=4, alpha=0.4, color='tab:blue', label='Train')
            plt.scatter(te_tx_vecs[:,0]/ms, te_tx_vecs[:,1]/ms, s=4, alpha=0.4, color='tab:orange', label='OOD Test', marker='x')
            plt.scatter(tr_rx_vecs[:,0], tr_rx_vecs[:,1], s=35, color='tab:green', marker='^', label='Tx' if ds == 8 else 'Rx')
            plt.ylim(ylim[0], ylim[1])
            plt.xlim(xlim[0], xlim[1])
            lgnd = plt.legend()
            for handle in lgnd.legend_handles:
                handle.set_sizes([30.0])
            plt.tight_layout(pad=0.1)
            plt.savefig(f'plots/mobicom2024_plots/rss_prediction/elevation_scatter_split_{ds}_grid{grid_size}.pdf', bbox_inches='tight')
            plt.savefig(f'plots/mobicom2024_plots/rss_prediction/elevation_scatter_split_{ds}_grid{grid_size}.png', bbox_inches='tight', dpi=300)
            plt.clf()
            plt.close()


filenames = os.listdir('synth_evaluation/')
all_results = {}
for filename in filenames:
    if not 'tirem_nn_rbf' in filename:
        continue
    file = os.path.join('synth_evaluation', filename)
    results = np.load(file, allow_pickle=True)
    rx_locs = results['rx_locs']
    results = results['results'].ravel()[0]
    DS = int(file[file.find('DS:')+3])
    grid = int(file[file.find('grid') + 4: file.find('grid')+4+3].strip('_'))
    if DS not in all_results:
        all_results[DS] = {}
    all_results[DS][grid] = results
    all_results[DS][grid]['rx_locs'] = rx_locs
    num_results = len(all_results[DS][grid]['train_coords'])
    num_methods = len(methods)
    num_sensors = num_results // num_methods
    all_results[DS][grid]['train_coords'] = all_results[DS][grid]['train_coords'][:num_sensors]
    all_results[DS][grid]['val_coords'] = all_results[DS][grid]['val_coords'][:num_sensors]
    all_results[DS][grid]['test_coords'] = all_results[DS][grid]['test_coords'][:num_sensors]
    all_results[DS][grid]['train_err'] = 1 * (all_results[DS][grid]['train_pred'] - all_results[DS][grid]['train_rss']) 
    all_results[DS][grid]['val_err'] = 1 * (all_results[DS][grid]['val_pred'] - all_results[DS][grid]['val_rss']) 
    all_results[DS][grid]['test_err'] = 1 * (all_results[DS][grid]['test_pred'] - all_results[DS][grid]['test_rss']) 
    all_results[DS][grid]['train_rel_err'] = (all_results[DS][grid]['train_pred'] - all_results[DS][grid]['train_rss'])  / all_results[DS][grid]['train_rss']
    all_results[DS][grid]['val_rel_err'] = (all_results[DS][grid]['val_pred'] - all_results[DS][grid]['val_rss'])  / all_results[DS][grid]['val_rss']
    all_results[DS][grid]['test_rel_err'] = (all_results[DS][grid]['test_pred'] - all_results[DS][grid]['test_rss']) / all_results[DS][grid]['test_rss']
    for key in [
        'train_rss', 'val_rss', 'test_rss',
        'train_pred', 'val_pred', 'test_pred',
        'train_err', 'val_err', 'test_err',
        'train_rel_err', 'val_rel_err', 'test_rel_err',
    ]:
        new_dict = {}
        for i in range(num_sensors):
            new_dict[i] = np.stack(all_results[DS][grid][key][i::num_sensors])
        all_results[DS][grid][key] = new_dict
    for key in ['train_coords', 'val_coords', 'test_coords']:
        new_dict = {}
        for i in range(num_sensors):
            new_dict[i] = np.linalg.norm(all_results[DS][grid][key][i] - all_results[DS][grid]['rx_locs'][i], axis=1)
        all_results[DS][grid][key.split('_')[0]+'_dist'] = new_dict
    for key in [
        'train_', 'val_', 'test_',
        'train_rel_', 'val_rel_', 'test_rel_',
    ]:
        means = []
        stds = []
        for i in range(num_methods):
            means.append( [np.mean(abs(all_results[DS][grid][key+'err'][j][i])) for j in range(num_sensors)] )
            stds.append( [np.std(abs(all_results[DS][grid][key+'err'][j][i])) for j in range(num_sensors)] )
        all_results[DS][grid][key+'mean_err'] = np.array(means)
        all_results[DS][grid][key+'stds_err'] = np.array(stds)


def plot_individual_sensors_all_ds(grid, key='test', method_inds=[2,4,5,6], errorbar=False, figsize=(6,5), savefig=False):
    fig, axs = plt.subplots(nrows=3, figsize=figsize)
    for ds_ind, ds in enumerate([6,8,7]):
        res = all_results[ds][grid]
        length = len(res[key+'_mean_err'][0])
        for i, ind in enumerate(method_inds):
            label = pretty_methods[ind] if ds_ind == 0 else None
            if errorbar:
                axs[ds_ind].errorbar(np.arange(length)+i/20, res[key+'_mean_err'][ind], yerr=res[key+'_stds_err'][ind], label=label, marker=markers[i], c=tab_colors[i], capsize=3, linestyle=linestyles[i])
                #axs[ds_ind].set_ylabel('Mean Error [dB] (Std. Dev.)')
            else:
                axs[ds_ind].plot(np.arange(length)+i/100, res[key+'_mean_err'][ind], label=label, marker=markers[i], c=tab_colors[i], linestyle=linestyles[i])
                #axs[ds_ind].set_ylabel('Mean Error [dB]')
        axs[ds_ind].set_title(pretty_datasets[ds_ind])
        axs[ds_ind].set_xticks(ticks=range(length), labels=np.arange(length, dtype=int)+1, fontsize=10)
    plt.xlabel('Stationary Device ID')
    fig.supylabel('Mean Error [dB]')
    fig.legend(loc='outside lower center', ncol=min(4, len(method_inds)))
    plt.subplots_adjust(left=0.11, right=0.99, top=0.95, bottom=0.16, hspace=0.5)
    if savefig:
        plt.show()
        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/all_ds_grid_{key}_{grid}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/all_ds_grid_{key}_{grid}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def plot_individual_sensors(ds, grid, key='test', method_inds=[2,4,5,6], errorbar=False, figsize=(6,2), savefig=False):
    fig = plt.figure(figsize=figsize)
    res = all_results[ds][grid]
    ds_ind = 0 if ds == 6 else (1 if ds == 8 else 2)
    length = len(res[key+'_mean_err'][0])
    for i, ind in enumerate(method_inds):
        label = pretty_methods[ind]
        if errorbar:
            plt.errorbar(np.arange(length)+i/20, res[key+'_mean_err'][ind], yerr=res[key+'_stds_err'][ind], label=label, marker=markers[i], c=tab_colors[i], capsize=3, linestyle=linestyles[i])
            plt.ylabel('Mean Error [dB] (Std. Dev.)')
        else:
            plt.plot(np.arange(length)+i/100, res[key+'_mean_err'][ind], label=label, marker=markers[i], c=tab_colors[i], linestyle=linestyles[i])
            plt.ylabel('Mean Error [dB]')
    plt.title(pretty_datasets[ds_ind])
    plt.xticks(ticks=range(length), labels=np.arange(length, dtype=int)+1, fontsize=10)
    plt.xlabel('Stationary Device ID')
    plt.legend(ncol=min(2, len(method_inds)))
    plt.tight_layout(pad=0.1)
    if savefig:
        plt.show()
        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/ds_{ds}_grid_{key}_{grid}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/ds_{ds}_grid_{key}_{grid}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def plot_grid_size_val_test(DS, method_inds=[2,3,6], errorbar=False):
    key = 'val'
    grids = np.array([2,5,10])
    fig, axs = plt.subplots(nrows=2, sharex=True)
    for i, ind in enumerate(method_inds):
        means = [all_results[DS][grid][key+'_mean_err'][ind].mean() for grid in grids]
        if errorbar:
            stds = [all_results[DS][grid][key+'_stds_err'][ind].mean() for grid in grids]
            axs[0].errorbar(grids+i/20, means, yerr=stds, label=pretty_methods[ind], marker=markers[i], c=tab_colors[i], linestyle='--')
        else:
            axs[0].plot(grids, means, label=pretty_methods[ind], marker=markers[i], c=tab_colors[i])
    key = 'test'
    for i, ind in enumerate(method_inds):
        means = [all_results[DS][grid][key+'_mean_err'][ind].mean() for grid in grids]
        if errorbar:
            stds = [all_results[DS][grid][key+'_stds_err'][ind].mean() for grid in grids]
            axs[1].errorbar(grids-(i+1)/20, means, yerr=stds, marker=markers[i], c=tab_colors[i])
        else:
            axs[1].plot(grids, means, marker=markers[i], c=tab_colors[i])
    plt.xlabel('Grid Size')
    if errorbar:
        axs[0].set_ylabel('Mean Error [dB] (Std. Dev.)')
        axs[1].set_ylabel('Mean Error [dB] (Std. Dev.)')
    else:
        axs[0].set_ylabel('Mean Error [dB]')
        axs[1].set_ylabel('Mean Error [dB]')
    axs[0].set_title('Valdiation')
    axs[1].set_title('OOD Test')
    fig.legend(loc='outside lower center', ncol=min(4, len(method_inds)))
    plt.tight_layout(pad=0.1)
    plt.show()

def plot_grid_size_all_ds(key='test', method_inds=[2,4,5,6], errorbar=False, figsize=(6,5), savefig=False, drop_outlier=False):
    grids = np.array([2,5,10])
    fig, axs = plt.subplots(nrows=3, figsize=figsize)
    for ds_ind, ds in enumerate([6,8,7]):
        for i, ind in enumerate(method_inds):
            if drop_outlier and ds == 7:
                means = [all_results[ds][grid][key+'_mean_err'][ind][:-1].mean() for grid in grids]
            else:
                means = [all_results[ds][grid][key+'_mean_err'][ind].mean() for grid in grids]
            label = pretty_methods[ind] if ds_ind == 0 else None
            if errorbar:
                stds = [all_results[ds][grid][key+'_stds_err'][ind].mean() for grid in grids]
                axs[ds_ind].errorbar(grids+i/20, means, yerr=stds, label=label, marker=markers[i], c=tab_colors[i], linestyle=linestyles[i])
                #axs[ds_ind].set_ylabel('Mean Error [dB] (Std. Dev.)')
            else:
                axs[ds_ind].plot(grids, means, label=label, marker=markers[i], c=tab_colors[i], linestyle=linestyles[i])
                #axs[ds_ind].set_ylabel('Mean Error [dB]')
            axs[ds_ind].set_title(pretty_datasets[ds_ind])
            axs[ds_ind].set_xticks(ticks=[2,5,10], labels=[r'$2\times2$',r'$5\times5$',r'$10\times10$',])
    plt.xlabel('Grid Size')
    fig.legend(loc='outside lower center', ncol=min(4, len(method_inds)))
    fig.supylabel('Mean Error [dB]')
    plt.tight_layout(pad=0.1)
    plt.subplots_adjust(left=0.14, right=0.99, top=0.95, bottom=0.18, hspace=0.5)
    outlier_str = '_drop_outlier' if drop_outlier else ''
    if savefig:
        plt.savefig(f'plots/mobicom2024_plots/rss_prediction/all_ds_grid_size_{key}{outlier_str}.pdf', bbox_inches='tight')
        plt.savefig(f'plots/mobicom2024_plots/rss_prediction/all_ds_grid_size_{key}{outlier_str}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()

def get_wins_per_method(method_inds=[2,4,5,6], key='test'):
    wins = {}
    for ds_ind, ds in enumerate([6,8,7]):
        wins[ds] = {}
        for grid in [2,5,10]:
            num_sensors = len(all_results[ds][grid][key+'_err'])
            wins[ds][grid] = {
                'mean': np.zeros((len(method_inds))),
                'sum': np.zeros((len(method_inds))),
            }
            sensor_wins_mean = np.zeros((num_sensors, len(method_inds)))
            sensor_wins_sum = np.zeros((num_sensors, len(method_inds)))
            for sensor in all_results[ds][grid][key+'_err']:
                num_results = len(all_results[ds][grid][key+'_err'][sensor][0])
                little_errs = np.zeros((len(method_inds), num_results))
                for i, ind in enumerate(method_inds):
                    little_errs[i] = abs(all_results[ds][grid][key+'_err'][sensor][ind])
                inds, counts = np.unique(little_errs.argmin(axis=0), return_counts=True)
                sensor_wins_sum[sensor, inds] = counts
                sensor_wins_mean[sensor, inds] = counts / num_results
            wins[ds][grid]['mean'] = sensor_wins_mean.mean(axis=0)
            wins[ds][grid]['sum'] = sensor_wins_sum.sum(axis=0)
    return wins


def plot_scatters(ds, grid, sensor=14, method_inds=[2,6], figsize=(6,3), hide_val=False, relative_err=False, savefig=False):
    fig, axs = plt.subplots(nrows=1, ncols=len(method_inds), sharex=True, sharey=True, figsize=figsize)
    keys = ['test', 'val']
    if relative_err:
        keys = [key+'_rel' for key in keys]
    vmin, vmax = 0, 0
    res = all_results[ds][grid]
    for i, ind in enumerate(method_inds):
        for key in keys:
            if hide_val and 'val' in key: continue
            err = (res[key+'_err'][sensor][ind])
            vmin = min(vmin, err.min())
            vmax = max(vmax, err.max())
    vmin = round(vmin-1)
    vmax = round(vmax+1)
    for i, ind in enumerate(method_inds):
        label2 = 'Sensor'
        for key in keys:
            if hide_val and 'val' in key:
                continue
            coords = res[key.replace('_rel','')+'_coords'][sensor] * (25 if ds in [6,8] else 100)
            err = (res[key+'_err'][sensor][ind])
            rx_locs = res['rx_locs']* (25 if ds in [6,8] else 100)
            cmap = axs[i].scatter(coords[:,0], coords[:,1], 
                                  c=err, 
                                  s=2 if 'val' in key else 10,
                                  cmap='coolwarm', 
                                  norm=mcolors.CenteredNorm(halfrange=max(vmax, abs(vmin))),
                                  marker='*' if 'val' in key else None,
                                  label='Valdiation' if 'val' in key else 'OOD Test')
            axs[i].scatter(rx_locs[sensor,0], rx_locs[sensor,1], c='tab:green', marker='v', label=label2)
            label2 = None
        axs[i].set_title(pretty_methods[ind])
        axs[i].set_xlabel('Distance [m]')
        axs[i].legend(labelspacing=0.1,handlelength=0.1)
        axs[i].set_aspect('equal', adjustable='box')
        if i == 0:
            axs[i].set_ylabel('Distance [m]')
    plt.tight_layout(pad=0.15)
    cbar = plt.colorbar(cmap, ax=axs.ravel().tolist(), label='Pred Error [dB]', ticks=[vmin, 0, vmax])
    if savefig:
        plt.show()
        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/ds_{ds}_{key}_grid_{grid}_scatter{sensor}.pdf', bbox_inches='tight')
        fig.savefig(f'plots/mobicom2024_plots/rss_prediction/ds_{ds}_{key}_grid_{grid}_scatter{sensor}.png', bbox_inches='tight', dpi=300)
        plt.close()
    else:
        plt.show()


def plot_hists(DS, grid, sensors=range(5), method_inds=[2,6]):
    fig, axs = plt.subplots(nrows=len(method_inds), ncols=len(sensors), sharex=True)
    for i, ind in enumerate(method_inds):
        for j, sensor in enumerate(sensors):
            res = all_results[DS][grid]
            for key in ['val', 'test']:
                coords = res[key+'_coords'][sensor]
                err = (res[key+'_err'][sensor][ind])
                axs[i,j].hist(err, bins=50, alpha=0.8)
        axs[i,0].set_title(pretty_methods[ind])
    plt.tight_layout()
    plt.show()

def plot_violins(DS, grid, method_inds=[2,4,5,6]):
    fig = plt.figure(figsize=(2+len(method_inds), 4))
    val_err, test_err = {}, {}
    spacing = 2.4
    separation = 0.4
    width = 0.7
    labels = [pretty_methods[ind] for ind in method_inds]
    for ind in method_inds:
        res = all_results[DS][grid]
        verr = [res['val_err'][sensor][ind] for sensor in res['val_err']]
        val_err[methods[ind]] = abs(np.concatenate(verr))
        terr = [res['test_err'][sensor][ind] for sensor in res['test_err']]
        test_err[methods[ind]] = abs(np.concatenate(terr))
    plt.violinplot(val_err.values(), positions=np.arange(0,len(labels))*spacing-separation, showmeans=True, widths=width)
    plt.violinplot(test_err.values(), positions=np.arange(0,len(labels))*spacing+separation, showmeans=True, widths=width)
    plt.xticks(ticks=np.arange(0,len(labels))*spacing, labels=labels)
    plt.plot([], c=tab_colors[0], label='Validation')
    plt.plot([], c=tab_colors[1], label='OOD Test')
    plt.ylabel('Prediction Error [dB]')
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.show()

def plot_individual_violins(DS, grid, method_inds=[2,4,5,6]):
    fig = plt.figure(figsize=(2+len(method_inds), 4))
    val_err, test_err = {}, {}
    spacing = 2.4
    separation = 0.4
    width = 0.7
    labels = [pretty_methods[ind] for ind in method_inds]
    for ind in method_inds:
        res = all_results[DS][grid]
        verr = [res['val_err'][sensor][ind] for sensor in res['val_err']]
        val_err[methods[ind]] = verr
        terr = [res['test_err'][sensor][ind] for sensor in res['test_err']]
        test_err[methods[ind]] = terr
    plt.violinplot(val_err.values(), positions=np.arange(0,len(labels))*spacing-separation, showmeans=True, widths=width)
    plt.violinplot(test_err.values(), positions=np.arange(0,len(labels))*spacing+separation, showmeans=True, widths=width)
    plt.xticks(ticks=np.arange(0,len(labels))*spacing, labels=labels)
    plt.plot([], c=tab_colors[0], label='Validation')
    plt.plot([], c=tab_colors[1], label='OOD Test')
    plt.ylabel('Prediction Error [dB]')
    plt.legend(loc='upper left')
    plt.tight_layout()
    plt.show()

#plot_loss_fields(2)
#plot_loss_fields(5)
#plot_loss_fields(10)
#get_all_pred_maps(10)
#plot_tirem_nn_features_preds()
#plot_elevation_and_scatters()
#plot_grid_size_all_ds(method_inds=[2,4,5,6], savefig=True)
#plot_grid_size_all_ds('val', method_inds=[2,4,5,6], savefig=True)
#plot_grid_size_all_ds(method_inds=[2,4,5,6], savefig=True, drop_outlier=True)
#plot_individual_sensors_all_ds(2, figsize=(6,5), savefig=True)
#plot_individual_sensors_all_ds(2, key='val', figsize=(6,5), savefig=True)
#plot_individual_sensors_all_ds(5, figsize=(6,5), savefig=True)
#plot_individual_sensors_all_ds(5, key='val', figsize=(6,5), savefig=True)
#plot_individual_sensors_all_ds(10, figsize=(6,5), savefig=True)
#plot_individual_sensors_all_ds(10, key='val', figsize=(6,5), savefig=True)
#plot_individual_sensors(7,10, figsize=(6,2), savefig=True)
#res = get_wins_per_method()
#get_all_pred_maps()
from IPython import embed
embed()