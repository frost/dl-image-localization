"""NRDZ TIREM Propagation Model."""

import numpy as np
import pandas as pd
import scipy.io as sio
from time import time
from numba import jit
from ctypes import *
from common import *
from tirem_params import *
import math
from datetime import date
import concurrent.futures
import multiprocessing as mp
from multiprocessing import shared_memory
import rasterio
import utm

tirem_dll = None

def main():
    """Entry point into tirem code."""
    load_tirem_lib()
    bs_endpoint_name = "USTAR"
    bs_is_tx = 0
    txheight = 1.5
    rxheight = 3
    bs_lon = -111.84167
    bs_lat = 40.76895
    freq = 462.7
    polarz = 'H'
    run_date = tirem_pred(bs_endpoint_name, bs_is_tx, txheight, rxheight, bs_lon, bs_lat, freq, polarz, generate_features=True)
    #f = np.load(bs_endpoint_name + '_tirem_rssi_' + str(freq) + run_date+".npz")
    #f = np.load(bs_endpoint_name + '_diff_parameter_' + str(freq) + run_date+".npy")
    #f = np.load(bs_endpoint_name + '_diff_param_obstacle_with_max_h_over_r_' + str(freq) + run_date+".npy")
    #f = np.load(bs_endpoint_name + '_elevation_angles_NLOS_' + str(freq) + run_date+".npy")
    #f = np.load(bs_endpoint_name + '_elevation_angles_tx_rx_' + str(freq) + run_date+".npy")
    #f = np.load(bs_endpoint_name + '_LOS_NLOS_' + str(freq) + run_date+".npy")
    #f = np.load(bs_endpoint_name + '_number_knife_edges_' + str(freq) + run_date+".npy")
    #f = np.load(bs_endpoint_name + '_number_obstacles_' + str(freq) + run_date+".npy")
    #f = np.load(bs_endpoint_name + '_shadowing_angles_' + str(freq) + run_date + ".npy")


def tirem_pred(bs_endpoint_name, bs_is_tx, txheight, rxheight, bs_lon, bs_lat, freq,
               polarz, generate_features=1, map_type="corrected", map_filedir="../corrected_dsm.tif", gain=0, first_call=0,
               extsn=0,
               refrac=450, conduc=50, permit=81, humid=80, side_len=0.5, sampling_interval=0.5):
    """
    Code for running TIREM and generating features for augmented modeling. Adapted from SLC_TIREM_v3_Python.

    :param bs_endpoint_name: basestation endpoint name e.g. "cbrssdr1-ustar-comp"
    :param bs_is_tx: boolean, is the basestation a transmitter (1) or a receiver (0)
    :param txheight: transmitter height (m) -DO NOT INCLUDE THE BUILDING HEIGHT OR
    ELEVATION, JUST THE TX HEIGHT W.R.TO THE SURFACE IT STAYS ON
    :param rxheight: receiver height (m) -DO NOT INCLUDE THE BUILDING HEIGHT OR
    ELEVATION, JUST THE RX HEIGHT W.R.TO THE SURFACE IT STAYS ON
    :param bs_lon: basestation longitude
    :param bs_lat: basestation latitude
    :param freq: transmit frequency in MHz
    :param polarz: polarization of the wave ('H' for horizontal, 'V' for vertical)
            produced by the Tx antenna
    :param generate_features: boolean, Generate only TIREM predictions (0) / also generate
                       features (1)
    :param map_type: Fusion map ("fusion") or lidar DSM map ("lidar"). The fusion map
              struct has to have the fields "dem" for digital elevation map and
              "hybrid_bldg" for building heights. The lidar map has to have the field
              "data" having combined information of elevations and building heights.
    :param map_filedir: map file directory including the filename and extension
    gain: antenna gain in dB
    :param gain: antenna gain in dB
    :param first_call: boolean, 1 loads the TIREM library for people using it for
               the first time.
    :param extsn: boolean, 0 is false; anything else is true. False = new profile,
           true = extension of last profile terrain
    :param refrac: surface refractivity, range: (200 to 450.0) "N-Units"
    :param conduc: conductivity, range: (0.00001 to 100.0) S/m
    :param permit: relative permittivity of earth surface, range: (1 to 1000)
    :param humid: humidity, units g/m^3, range: (0 to 110.0)
    :param side_len: grid_cell side length (m)
    :param sampling_interval: sampling interval along the tx-rx link. e.g. 0.5
                       means a given grid cell is sampled twice. Side length
                       of 0.5 and sampling interval of 0.5 mean that the
                       Tx-Rx horizontal array step size 0.5*0.5 = 0.25 m.
    :return: run_date, the date the script completed its run.
    Saves tirem rssi and generated features to the current folder.
    Serhat Tadik, Aashish Gottipati, Michael A. Varner, Gregory D. Durgin
    """
    if map_filedir.endswith('mat'):
        # load the map
        map_struct = sio.loadmat(map_filedir)['SLC']

        # Define a new struct named SLC
        SLC = map_struct[0][0]
        column_map = dict(zip([name for name in SLC.dtype.names], [i for i in range(len(SLC.dtype.names))]))

        bs_x, bs_y, idx = lon_lat_to_grid_xy(np.array([bs_lon]), np.array([bs_lat]), SLC,
                                             column_map)  # Longitude - latitude

    elif map_filedir.endswith('tif'):
        alt_coords = pd.read_csv('../alt_coords.csv', comment='#')
        alt_lat = np.array(alt_coords['Latitude (deg N)'])
        alt_lon = np.array(alt_coords['Longitude (deg E)'])

        dsm_corners = np.array([
            [1447.88347094,  492.35956159],
            [3847.88347094, 2712.35956159]
        ])		
        dsm_object = rasterio.open(map_filedir)
        dsm_object.origin = np.array(dsm_object.transform)[:6].reshape(2,3) @ np.array([0, dsm_object.shape[0], 1])
        dsm_map = dsm_object.read(1)     # a np.array containing elevation values
        utm_transform = np.array(dsm_object.transform).reshape((3,3))
        utm_transform[:2,2] -= dsm_object.origin
        inv_transform = np.linalg.inv(utm_transform)
        a_ind = (inv_transform @ np.array([dsm_corners[0,0], dsm_corners[0,1], 1])).round().astype(int)
        b_ind = (inv_transform @ np.array([dsm_corners[1,0], dsm_corners[1,1], 1])).round().astype(int)

        def lat_lon_to_xy(alt_lat, alt_lon, origin=np.array([a_ind[0], b_ind[1]])):
            alt_idx = np.vstack(utm.from_latlon(alt_lat, alt_lon)[:2]) - dsm_object.origin[:,None]
            alt_idx = inv_transform @ np.vstack((alt_idx, np.ones(len(alt_lat))))
            xy = (alt_idx[:2] - np.array([a_ind[0], b_ind[1]])[:,None]).T
            return xy
        
        sub_img = np.flipud(dsm_map[b_ind[1]:a_ind[1]+1, a_ind[0]:b_ind[0]+1])
        bs_coords = lat_lon_to_xy(alt_lat, alt_lon)
        bs_coords[:,1] = len(sub_img) - bs_coords[:,1]

    # generate the map
    if map_type == "corrected":
        slc_map = sub_img.copy().astype(np.float64)
        nrows, ncols = slc_map.shape
    else:
        if map_type == "fusion":
            slc_map = SLC[column_map['dem']] + 0.3048 * SLC[column_map['hybrid_bldg']]
        elif map_type == "lidar":
            slc_map = SLC[column_map['data']]
        nrows = int(SLC[column_map['nrows']])
        ncols = int(SLC[column_map['ncols']])
    shape = slc_map.shape
    shm = shared_memory.SharedMemory(create=True, size=slc_map.nbytes, name='tirem_np')
    shared_slc_map = np.ndarray(slc_map.shape, dtype=slc_map.dtype, buffer=shm.buf)
    shared_slc_map[:] = slc_map[:]
    if map_type == "corrected":
        loop_options = zip(alt_coords['Station'], bs_coords, alt_coords['Height (m)'], alt_coords['Type'])
    else:
        loop_options = [[bs_endpoint_name, [bs_x, bs_y], rxheight, 'Rooftop_old']]
    for name, coords, height, station_type in loop_options:
        print('Producing TIREM map for ', name)
        bs_endpoint_name = name
        bs_x, bs_y = np.rint(coords).astype(int)
        rxheight = height
        if station_type == 'Rooftop':
            rxheight = 3
        elif name == 'EBC-DD':
            rxheight = 3
        # parameters
        input_params = {'bs_endpoint_name': bs_endpoint_name, 'bs_is_tx': bs_is_tx, 'txheight': txheight,
                        'rxheight': rxheight,
                        'bs_lon': bs_lon, 'bs_lat': bs_lat, 'bs_x': bs_x, 'bs_y': bs_y, 'freq': freq,
                        'polarz': polarz + '   ', 'generate_features': generate_features,
                        'map_type': map_type, 'map_filedir': map_filedir, 'gain': gain, 'first_call': first_call,
                        'extsn': extsn,
                        'refrac': refrac, 'conduc': conduc, 'permit': permit, 'humid': humid, 'side_len': side_len,
                        'sampling_interval': sampling_interval}

        params = Params(**input_params)

        ## Get tirem loss for all points

        # determine the tx/rx grid (raster) coordinates
        if bs_is_tx:
            tx = np.array([params.bs_x, params.bs_y])
        else:
            rx = np.array([params.bs_x, params.bs_y])

        # the gain
        EIRP = params.gain

        ## Run TIREM
        t = time()

        # Initialize variables
        wavelength = 300 / params.freq
        tirem_rssi = np.ones((nrows, ncols)) * np.NaN
        distances = np.ones((nrows, ncols)) * np.NaN

        t0 = time()
        if generate_features:
            # line-of-sight / non-line-of-sight
            LOS = np.ones((nrows, ncols))

            # number of blocking obstructions
            number_obstacles = np.zeros((nrows, ncols))

            # number of knife edges
            number_knife_edges = np.zeros((nrows, ncols))

            # elevation angle between Tx & Rx with no consideration of obstacles in between
            elevation_angles_tx_rx = np.zeros((nrows, ncols))

            # elevation angles considering the obstacles in between Tx & Rx
            elevation_angles_NLOS = np.zeros((nrows, ncols, 2))

            # shadowing angles for the first and last knife-edges
            first_last_shadowing_angles = np.zeros((nrows, ncols, 2))

            # diffraction angles for the first and last knife-edges
            first_last_diffraction_angles = np.zeros((nrows, ncols, 2))

            # Fresnel-Kirchhoff diffraction parameter
            diff_parameter = np.zeros((nrows, ncols, 2))

            # Fresnel-Kirchhoff diffraction parameter for the obstacle with the
            # largest h (height of the obstacle above the tx-rx link) /r (fresnel
            # zone at that point) ratio
            diff_param_obstacle_with_max_h_over_r = np.zeros((nrows, ncols))

        # Determine tx / rx height from the map
        if bs_is_tx:
            tx_elevation = slc_map[tx[1], tx[0]]
        else:
            rx_elevation = slc_map[rx[1], rx[0]]
        # Loop through the map pixels

        grid = np.array(np.meshgrid(range(ncols), range(nrows))).T.reshape(-1,2)
        if bs_is_tx:
            rx_vec = grid + 1
            vec = rx_vec
            tx_vec = tx
            #rx_elevation = slc_map[grid[:,1], grid[:,0]]
        else:
            tx_vec = grid + 1
            vec = tx_vec
            rx_vec = rx
            #tx_elevation = slc_map[grid[:,1], grid[:,0]]
        distances[grid[:,1], grid[:,0]] = np.linalg.norm(rx_vec - tx_vec, 2, axis=1) * side_len

        finished_elements = 0
        t0 = time()
        #tirem_parallel(157, tx if bs_is_tx else rx, bs_is_tx, side_len, shape, sampling_interval, EIRP, params, generate_features)
        with concurrent.futures.ProcessPoolExecutor() as executor:
            futures = {executor.submit(tirem_parallel, a, tx if bs_is_tx else rx, bs_is_tx, side_len, shape, sampling_interval, EIRP, params, generate_features): a for a in range(ncols)}
            for future in concurrent.futures.as_completed(futures):
                a = futures[future]
                try:
                    row, result = future.result()
                    #tirem_rssi[:,row] = result
                    LOS[:,row], number_obstacles[:,row], number_knife_edges[:,row], elevation_angles_tx_rx[:,row], elevation_angles_NLOS[:,row], first_last_shadowing_angles[:,row], first_last_diffraction_angles[:,row], diff_parameter[:,row], diff_param_obstacle_with_max_h_over_r[:,row] = result
                    #LOS[:,row], number_obstacles[:,row], first_last_shadowing_angles[:,row], diff_param_obstacle_with_max_h_over_r[:,row] = result
                    finished_elements += 1
                    print(f"{(time() - t0)/finished_elements:.4f}    {round(finished_elements/ncols, 4)*100:.2f}   {finished_elements}/{ncols}    ", end='\r')
                except Exception as exc:
                    print('%i generated an exception %s' % (a, exc))

        #for a in range(ncols):

        #    # Display progress
        #    if np.mod(a, 50) == 0:
        #        print("Progress: ")
        #        print(str(100 * a / ncols) + '   %')
        #    print('Row Time', time() - trow, tinit, tbuild, ttirem, twrap)
        #    trow = time()
        #    tinit = 0.0
        #    tbuild = 0.0
        #    ttirem = 0.0
        #    twrap = 0.0

        #    #for b in range(nrows):

        elapsed = time() - t
        print("Elapsed time is:" + str(elapsed))
        today = date.today()

        #np.savez(bs_endpoint_name + '_tirem_rssi_' + str(freq) + today.strftime("%b-%d-%Y") + '.npz', tirem_rssi, distances, params)

        if generate_features:
            np.save(bs_endpoint_name + '_LOS_NLOS_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy', LOS)
            np.save(bs_endpoint_name + '_number_obstacles_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy',
                    number_obstacles)
            np.save(bs_endpoint_name + '_number_knife_edges_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy',
                    number_knife_edges)
            np.save(bs_endpoint_name + '_elevation_angles_tx_rx_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy',
                    elevation_angles_tx_rx)
            np.save(bs_endpoint_name + '_elevation_angles_NLOS_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy',
                    elevation_angles_NLOS)
            np.save(bs_endpoint_name + '_shadowing_angles_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy',
                    first_last_shadowing_angles)
            np.save(bs_endpoint_name + '_diffraction_angles_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy',
                    first_last_diffraction_angles)
            np.save(bs_endpoint_name + '_diff_parameter_' + str(freq) + today.strftime("%b-%d-%Y") + '.npy', diff_parameter)
            np.save(bs_endpoint_name + '_diff_param_obstacle_with_max_h_over_r_' + str(freq) + today.strftime(
                "%b-%d-%Y") + '.npy', diff_param_obstacle_with_max_h_over_r)

    return today.strftime("%b-%d-%Y")

def tirem_parallel(a, x_val, bs_is_tx, side_len, shape, sampling_interval, EIRP, params, generate_features):
    existing_shm = shared_memory.SharedMemory(name='tirem_np')
    slc_map = np.ndarray(shape, dtype=np.float64, buffer=existing_shm.buf)
    nrows = len(slc_map)
    tmp_res = np.ones(nrows) * np.NaN
    LOS = np.ones((nrows))
    number_obstacles = np.zeros((nrows))
    number_knife_edges = np.zeros((nrows))
    elevation_angles_tx_rx = np.zeros((nrows))
    elevation_angles_NLOS = np.zeros((nrows, 2))
    first_last_shadowing_angles = np.zeros((nrows, 2))
    first_last_diffraction_angles = np.zeros((nrows, 2))
    diff_parameter = np.zeros((nrows, 2))
    diff_param_obstacle_with_max_h_over_r = np.zeros((nrows))

    wavelength = 300 / params.freq
    if bs_is_tx:
        tx_elevation = slc_map[x_val[1], x_val[0]]
    else:
        rx_elevation = slc_map[x_val[1], x_val[0]]
    
    time1 = 0
    time1a = 0
    time2 = 0
    time3 = 0
    time3a = 0
    time4 = 0
    time5 = 0

    for b in range(nrows):
            flag = 0
            cntr = 0
            if bs_is_tx:
                rx = np.array([a, b]) + np.array([1, 1])
                tx = x_val
            else:
                tx = np.array([a, b]) + np.array([1, 1])
                rx = x_val
            if (rx == tx).all() == 0:
                # build arrays and calculate TIREM's predictions
                t0 = time()
                [d_array, e_array] = build_arrays(side_len, sampling_interval, tx, rx, slc_map)
                #tmp_res[b] = EIRP - get_tirem_loss(d_array, e_array, params)

                if generate_features:

                    # Get
                    if bs_is_tx:
                        rx_elevation = slc_map[b, a]
                    else:
                        tx_elevation = slc_map[b, a]
                    # Get the nonzero elements of the distance and elevation arrays

                    nonzero_e_array_indices = [e_array != 0]
                    d_array_nonzero = d_array[tuple(nonzero_e_array_indices)]
                    e_array_nonzero = e_array[tuple(nonzero_e_array_indices)]

                    # Calculate the total Rx and Tx heights and the slope between them
                    rx_total_height = rx_elevation + params.rxheight
                    tx_total_height = tx_elevation + params.txheight
                    slope = (rx_total_height - tx_total_height) / max(d_array_nonzero)

                    # Elevation angle
                    elevation_angles_tx_rx[b] = 90 - math.degrees(math.atan(slope))

                    # Blocking obstruction information, bo_info, initialization
                    test1 = (d_array_nonzero * slope + tx_total_height) < (e_array_nonzero)
                    ind = test1.argmax()
                    ind = ind if test1[ind] else -1
                    inds = []
                    while ind >= 0:
                        inds.append(ind)
                        test1[ind:ind+201] = 0
                        ind = test1.argmax()
                        ind = ind if test1[ind] else -1
                    inds = np.array(inds).astype(int)
                    h_arr = e_array_nonzero - (d_array_nonzero * slope + tx_total_height)
                    r_arr = np.sqrt(wavelength * d_array_nonzero[inds] * (d_array_nonzero[-1] - d_array_nonzero[inds])) * np.sqrt(1 + slope ** 2) / d_array_nonzero[-1]
                    bo_info = np.vstack((d_array_nonzero[inds], h_arr[inds], r_arr)).T
                    number_obstacles[b] = len(inds)
                    if number_obstacles[b]:
                        LOS[b] = 0
                    #d1 = max(np.sqrt((d_array_nonzero[inds[0]] * slope) ** 2 + d_array_nonzero[inds[0]] ** 2), 0.25)
                    #d2 = max(np.sqrt(((d_array_nonzero[-1] - d_array_nonzero[inds[0]]) * slope) ** 2 + (d_array_nonzero[-1] - d_array_nonzero[inds[0]]) ** 2), 0.25)
                    #diff_parameter[b, 0] = h[inds[0]] * np.sqrt(2 * (d1 + d2) / (wavelength * d1 * d2))
                    

                    # find the obstacle with largest h / r ratio and calculate the diffraction parameter for it
                    if (bo_info == np.array([[0, 0, 0]])).all() == 0:
                        idxx = (bo_info[:, 1] / bo_info[:, 2]) == max(bo_info[:, 1] / bo_info[:, 2])
                        d1_ = max(bo_info[idxx, 0], 0.25)
                        h_ = bo_info[idxx, 1]
                        diff_param_obstacle_with_max_h_over_r[b] = h_ * np.sqrt(2 * (d_array_nonzero[-1] * (np.sqrt(1 + slope ** 2))) / (wavelength * d1_ * (d_array_nonzero[-1] - d1_) * (1 + slope ** 2)))

                    # knife edge information, ke_info, initialization
                    d_ke = 0
                    i = 0
                    ke_ind = (((d_array_nonzero - d_ke) * slope + tx_total_height) < (e_array_nonzero)).argmax()
                    first_last_shadowing_angles[b, 0] = math.degrees(math.atan(
                                    (e_array_nonzero[ke_ind] - tx_total_height) / d_array_nonzero[ke_ind])) - math.degrees(
                                    math.atan(slope))
                    saved_inds = []
                    while ke_ind > 0:
                    #while len(ke_inds):
                        saved_inds.append(ke_ind)
                        d_ke = d_array_nonzero[ke_ind]
                        tx_total_height = e_array_nonzero[ke_ind]
                        slope = (rx_total_height - tx_total_height) / (max(d_array_nonzero) - d_array_nonzero[ke_ind])
                        obstacles = ((d_array_nonzero - d_ke) * slope + tx_total_height) < (e_array_nonzero)
                        obstacles[:ke_ind+201] = 0
                        ke_ind = obstacles.argmax()
                    ke_inds = np.array(saved_inds).astype(int)
                    ke_info = np.array([d_array_nonzero[ke_inds], e_array_nonzero[ke_inds]]).T
                    number_knife_edges[b] = len(ke_inds)
                    #ke_info = np.array([[0, 0]])
                    #flag = 0
                    #cntr = 0
                    #d_ke = 0
                    #tx_total_height = tx_elevation + params.txheight
                    #slope = (rx_total_height - tx_total_height) / max(d_array_nonzero)
                    #for i in range(len(e_array_nonzero) - 1):
                    #    cntr = cntr + 1
                    #    # different from blocking obstacle, change the slope each time you come across an obstacle
                    #    if ((d_array_nonzero[i] - d_ke) * slope + tx_total_height) < (e_array_nonzero[i]):
                    #        # calculate number of knife edges
                    #        if flag == 0 or cntr > 200:
                    #            #number_knife_edges[b, a] += 1
                    #            # if it's the first ke, calculate the shadowing angle
                    #            if flag == 0:
                    #                first_last_shadowing_angles[b, 0] = math.degrees(math.atan(
                    #                    (e_array_nonzero[i] - tx_total_height) / d_array_nonzero[i])) - math.degrees(
                    #                    math.atan(slope))

                    #            # update ke_info with distance and elevation information
                    #            ke_info = np.append(ke_info, [[d_array_nonzero[i], e_array_nonzero[i]]], 0)
                    #            if flag == 0:
                    #                ke_info = np.delete(ke_info, 0, 0)
                    #            # update parameters
                    #            d_ke = d_array_nonzero[i]
                    #            tx_total_height = e_array_nonzero[i]
                    #            slope = (rx_total_height - tx_total_height) / (max(d_array_nonzero) - d_array_nonzero[i])

                    #            cntr = 0
                    #            flag = 1

                    # redefine the original slope in case it changes in the previous loop
                    rx_total_height = rx_elevation + params.rxheight
                    tx_total_height = tx_elevation + params.txheight
                    slope = (rx_total_height - tx_total_height) / max(d_array_nonzero)

                    # if there is at least one ke, calculate NLOS elevation angles, shadowing angle for the
                    # last ke, and diffraction angles.
                    if (ke_info == np.array([[0, 0]])).all() == 0:

                        h = ke_info[-1, 1] - ((ke_info[-1, 0] - d_array_nonzero[-1]) * slope + rx_total_height)
                        d1 = max(np.sqrt(((ke_info[-1, 0] - d_array_nonzero[-1]) * slope) ** 2 + (ke_info[-1, 0] - d_array_nonzero[-1]) ** 2), 0.25)
                        d2 = max(np.sqrt(((d_array_nonzero[0] - ke_info[-1, 0]) * slope) ** 2 + (d_array_nonzero[0] - ke_info[-1, 0]) ** 2), 0.25)

                        diff_parameter[b, 1] = h * np.sqrt(2 * (d1 + d2) / (wavelength * d1 * d2))
                        first_last_shadowing_angles[b, 1] = math.degrees(math.atan((ke_info[-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[-1, 0]))) + math.degrees(math.atan(slope))
                        elevation_angles_NLOS[b, 0] = 90 - math.degrees(math.atan((ke_info[0, 1] - tx_total_height) / max((ke_info[0, 0] - d_array_nonzero[0]),0.25)))
                        elevation_angles_NLOS[b, 1] = 90 - math.degrees(math.atan((ke_info[-1, 1] - rx_total_height) / max((d_array_nonzero[-1] - ke_info[-1, 0]),0.25)))

                        if ke_info.shape[0] == 1:
                            slope_ke_to_rx = (ke_info[0, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[0, 0])
                            first_last_diffraction_angles[b, 0] = math.degrees(math.atan((ke_info[0, 1] - tx_total_height) / ke_info[0, 0])) + math.degrees(math.atan(slope_ke_to_rx))
                            first_last_diffraction_angles[b, 1] = first_last_diffraction_angles[b, 0]

                        if ke_info.shape[0] != 1:

                            slope_1 = (ke_info[0, 1] - tx_total_height) / ke_info[0, 0]
                            slope_2 = (ke_info[0, 1] - ke_info[1, 1]) / (ke_info[1, 0] - ke_info[0, 0])
                            first_last_diffraction_angles[b, 0] = math.degrees(math.atan(slope_1)) + math.degrees(math.atan(slope_2))

                            # if the encountered obstacle isn't actually an obstacle that the waves would diffract from
                            # (e.g.the first encountered obstacle doesn't block the link between the Tx and the second
                            # obstacle), correct the variables
                            cnt_ke = 0
                            cnt2 = 0
                            while cnt_ke < ke_info.shape[0]:
                                cnt2 = cnt2 + 1
                                cnt_ke = cnt_ke + 1
                                if cnt_ke == 1:
                                    slope_1 = (ke_info[cnt_ke-1, 1] - tx_total_height) / ke_info[cnt_ke-1, 0]
                                else:
                                    slope_1 = (ke_info[cnt_ke-1, 1] - ke_info[cnt_ke - 2, 1]) / (ke_info[cnt_ke-1, 0] - ke_info[cnt_ke - 2, 0])

                                if cnt_ke == ke_info.shape[0]:
                                    slope_2 = (ke_info[cnt_ke-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[cnt_ke-1, 0])
                                else:
                                    slope_2 = (ke_info[cnt_ke-1, 1] - ke_info[cnt_ke, 1]) / (ke_info[cnt_ke , 0] - ke_info[cnt_ke-1, 0])
                                cnt = cnt_ke
                                idx = 0
                                while math.degrees(math.atan(slope_1)) < -1 * math.degrees(math.atan(slope_2)):
                                    idx = idx + 1
                                    if ke_info.shape[0] == cnt + 1:

                                        if ke_info.shape[0]- 1 - idx == 0:
                                            slope_1 = (ke_info[-1, 1] - tx_total_height) / ke_info[-1, 0]
                                        else:
                                            slope_1 = (ke_info[-1, 1] - ke_info[-2 - idx, 1]) / (ke_info[-1, 0] - ke_info[- 2 - idx, 0])

                                        slope_2 = (ke_info[-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[-1, 0])
                                    else:
                                        if cnt2 == 1:
                                            slope_1 = (ke_info[cnt, 1] - tx_total_height) / ke_info[cnt, 0]
                                        else:
                                            slope_1 = (ke_info[cnt, 1] - ke_info[cnt_ke - idx - 1, 1]) / (ke_info[cnt, 0] - ke_info[cnt_ke - idx - 1, 0])

                                        slope_2 = (ke_info[cnt, 1] - ke_info[cnt + 1, 1]) / (
                                                    ke_info[cnt + 1, 0] - ke_info[cnt , 0])

                                    if cnt2 == 1:
                                        h__ = ke_info[cnt, 1] - (ke_info[cnt, 0] * slope + tx_total_height)
                                        d1__ = math.sqrt((ke_info[cnt, 0] * slope) ** 2 + ke_info[cnt, 0] ** 2)
                                        d2__ = math.sqrt(((d_array_nonzero[-1] - ke_info[cnt, 0]) * slope) ** 2 + (d_array_nonzero[-1] - ke_info[cnt , 0]) ** 2)
                                        diff_parameter[b, 0] = h__ * math.sqrt(2 * (d1__ + d2__) / (wavelength * d1__ * d2__))

                                        first_last_diffraction_angles[b, 0] = math.degrees(math.atan(slope_1)) + math.degrees(math.atan(slope_2))
                                        first_last_shadowing_angles[b, 0] = math.degrees(math.atan(slope_1))

                                    cnt = cnt + 1
                                    cnt_ke = cnt_ke + 1
                                    number_knife_edges[b] = number_knife_edges[b] - 1
                                    #if number_knife_edges[b, a] < 0:
                                    #    print(number_knife_edges[b, a])

                            #if first_last_diffraction_angles[b, a, 0] < 0:
                            #    print(first_last_diffraction_angles[b, a, 0])


                            slope_last_ke_to_rx = (ke_info[-1, 1] - rx_total_height) / (d_array_nonzero[-1] - ke_info[-1, 0])
                            slope_ke_beforelast_to_ke_last = (ke_info[-2, 1] - ke_info[-1, 1]) / (ke_info[-1, 0] - ke_info[-2, 0])
                            first_last_diffraction_angles[b, 1] = math.degrees(math.atan(slope_last_ke_to_rx)) - math.degrees(math.atan(slope_ke_beforelast_to_ke_last))
    return a, (LOS, number_obstacles, number_knife_edges, elevation_angles_tx_rx, elevation_angles_NLOS, first_last_shadowing_angles, first_last_diffraction_angles, diff_parameter, diff_param_obstacle_with_max_h_over_r)
    return a, (tmp_res, LOS, number_obstacles, first_last_shadowing_angles, diff_param_obstacle_with_max_h_over_r)


def load_tirem_lib():
    """Loads tirem DLL"""
    global tirem_dll
    return

    try:
        tirem_dll = CDLL("./TIREM320DLL.dll")
    except OSError:
        try:
            tirem_dll = WinDLL("./TIREM320DLL.dll")
        except OSError:
            print('ERROR! Failed to load TIREM DLL')


def call_tirem_loss(d_array, e_array, params):
    """Sets up data for Tirem DLL call."""
    # Load DLL
    if tirem_dll is None:
        load_tirem_lib()

    # initialize the pointer and data for each argument
    # inputs just set to some number in their valid range
    TANTHT = pointer(c_float(params.txheight))  # 0 - 30, 000m
    RANTHT = pointer(c_float(params.rxheight))
    PROPFQ = pointer(c_float(params.freq))  # 1 to 20, 000 MHz

    # next three values characterize the shape of terrain
    NPRFL = pointer(c_int32(d_array.shape[0]))  # number of points in array MAYBE TGUS

    HPRFL = e_array.astype(np.float32).ctypes.data_as(POINTER(c_float))  # array of above (mean) sea level heights
    XPRFL = d_array.astype(np.float32).ctypes.data_as(POINTER(c_float))  # array of great circles distances between
    # points and start

    EXTNSN = pointer(c_int32(params.extsn))  # boolean, 0 is false
    # anything else is true. False = new profile, true = extension of last profile terrain
    # Haven't been able to figure out what the extension flag actually does

    REFRAC = pointer(c_float(params.refrac))  # Surface refractivity  200 to 450.0 "N-Units"
    CONDUC = pointer(c_float(params.conduc))  # 0.00001 to 100.0 S/m
    PERMIT = pointer(c_float(params.permit))  # Relative permittivity of earth surface  1 to 1000
    HUMID = pointer(c_float(params.humid))  # Units g/m^3   0 to 110.0
    polar_ascii = np.array([ord(c) for c in params.polarz])
    POLARZ = polar_ascii.astype(np.uint8).ctypes.data_as(POINTER(c_void_p))

    # output starts here, I just intialize them all to 0
    VRSION = np.array([0, 0, 0, 0, 0, 0, 0, 0], dtype=np.uint8).ctypes.data_as(POINTER(c_void_p))
    MODE = np.array([0, 0, 0, 0], dtype=np.uint8).ctypes.data_as(POINTER(c_void_p))
    LOSS = pointer(c_float(0))
    FSPLSS = pointer(c_float(0))

    tirem_dll.CalcTiremLoss(TANTHT, RANTHT, PROPFQ, NPRFL, HPRFL, XPRFL, EXTNSN, REFRAC, CONDUC,
                            PERMIT, HUMID, POLARZ, VRSION, MODE, LOSS, FSPLSS)
    return LOSS.contents.value


def get_tirem_loss(d_array, e_array, params):
    """Returns TIREM loss.

    Usage: loss = get_tirem_loss(d_array, e_array, params)

    inputs: d_array - distance array
            e_array - elevation array
            params - transmitter parameters

    output:   loss - estimated propagation loss
    """
    return call_tirem_loss(d_array, e_array, params)


if __name__ == '__main__':
    main()
