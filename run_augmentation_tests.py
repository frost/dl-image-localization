import numpy as np
import os
import torch
import pickle
import argparse
from localization import DLLocalization
from locconfig import LocConfig
from dataset import RSSLocDataset
from models import CoMLoss, SlicedEarthMoversDistance
from attacker import batch_wrapper, get_all_attack_preds_without_grad

should_train = True
should_load_model = True
should_run_results = True
restart_optimizer = False
save_all_results = True
force_april_test = True

# Specify params
max_num_epochs = 2000
include_elevation_map = True

batch_size = 64 if should_train else 64

num_training_repeats = 5

device = torch.device('cuda')
all_results = {}

muted_sensors = {
    6: {
        0: [2, 38, 13, 29, 26, 32, 24, 4],
        1: [24, 4, 36, 12, 39, 25, 31, 10, 34],
        2: [16, 17, 32, 15, 2, 38],
        3: [10, 34, 13, 29, 26, 35, 29],
        4: [7, 15, 23, 19, 25, 31],
    },
    7: {
        0: [4, 9, 15],
        1: [0, 7, 9],
        2: [7, 11, 2],
        3: [12, 9, 4],
        4: [8, 3, 0],
    },
    8: {
        0: [5],
        1: [1],
        2: [2],
        3: [3],
        4: [4],
    }
}

def main():
    global dataset_index
    cmd_line_params = []
    parser = argparse.ArgumentParser()
    parser.add_argument("--param_selector", type=int, default=-1, help='Index of pair for selecting params')
    parser.add_argument("--random_ind", type=int, default=-1, help='Random Int for selecting set of params')
    args = parser.parse_args()

    for random_state in range(0,num_training_repeats):
        for di in [8]:#6,7,8]:
            for split in ['grid2']:#'random', 'grid2', 'grid5', 'grid10']:
                for loss_func in [ CoMLoss()]:#, torch.nn.MSELoss(), SlicedEarthMoversDistance(num_projections=100, scaling=0.01, p=1)]:
                    augs = [
                        #[False, False, False, False],
                        #[True, False, False, False],
                        #[False, True, False, False],
                        #[False, False, True, False],
                        [False, False, False, True],
                        #[True, True],
                    ]
                    aug_params = [ 
                        #[0],
                        #[0],
                        #[0],
                        #[0],
                        ['tirem_nn'],
                        #['tirem'],
                    ]
                    for aug, params in zip(augs, aug_params):
                        for l in params:
                            cmd_line_params.append([di, split, random_state, loss_func,  aug, l])
    
    if args.param_selector > -1:
        param_list = [cmd_line_params[args.param_selector] ]
    elif args.random_ind > -1:
        param_list = [param for param in cmd_line_params if param[3] == args.random_ind]
    else:
        param_list = cmd_line_params
    
    for ind, param_set in enumerate(param_list):
        dataset_index, split, random_state, loss_func, aug, aug_param = param_set
        apply_rss_noise, apply_sensor_dropout, adv_train, should_augment = aug
        #should_augment, full_synthetic = aug
        full_synthetic = False
        print(param_set)
        dict_params = {
            "dataset": dataset_index,
            "data_split": split,
            "batch_size":batch_size,
            "random_state":random_state,
            "include_elevation_map":include_elevation_map,
            "apply_rss_noise":apply_rss_noise,
            "apply_sensor_dropout":apply_sensor_dropout,
            "adv_train":adv_train,
            "should_augment": should_augment,
            "augmentation": aug_param if should_augment else 0,
            "meter_scale": 30,
        }
        params = LocConfig(**dict_params)

        loss_label = 'com' if isinstance(loss_func, CoMLoss) else 'mse' if isinstance(loss_func, torch.nn.MSELoss) else 'emd' if isinstance(loss_func, SlicedEarthMoversDistance) else 'unknown'
        param_string = f"{params}_{loss_label}{'_full_synthetic' if full_synthetic else ''}"
        PATH = 'models/alt/%s__model.pt' % param_string
        model_ending = 'train_val.'
        global all_results
        pickle_filename = 'results/augment/alt/augment_%s.pkl' % param_string
        #pickle_filename = 'results/augment/alt/worst_augment_%s.pkl' % param_string
        model_filename = PATH.replace('model.', 'model_' + model_ending)

        if should_run_results:
            if os.path.exists(pickle_filename): # partial partial partial
                with open(pickle_filename, 'rb') as f:
                    results = pickle.load(f)

            else:
                rldataset = RSSLocDataset(params)
                #rldataset.make_partial_data(10)

                rldataset.print_dataset_stats()

                dlloc = DLLocalization(rldataset, loss_object=loss_func)

                if should_train:# and not os.path.exists(model_filename):
                    print('Training on dataset', rldataset.train_key, 'for', max_num_epochs, 'epochs')
                    print('Training', PATH)
                    dlloc.train_model(max_num_epochs, save_model_file=PATH, load_model=should_load_model, restart_optimizer=restart_optimizer, load_model_file=PATH)
                    results = save_results(dlloc, rldataset, model_filename, pickle_filename)
                else:
                    results = save_results(dlloc, rldataset, model_filename, pickle_filename)
        else:
            with open(pickle_filename, 'rb') as f:
                results = pickle.load(f)
        for key in results['err']:
            print(key, results['err'][key].mean())
        if save_all_results:
            print('Loading results from %s' % pickle_filename)
            all_results[pickle_filename] = results
    if save_all_results:
        with open('ds2_grid2_adv_results.pkl', 'wb') as f:
            pickle.dump(all_results, f)
            
def save_results(dlloc: DLLocalization, rldataset, model_filename, pickle_filename):
    results = get_results(model_filename, dlloc, rldataset)
    with open(pickle_filename, 'wb') as f:
        print('Saving', pickle_filename)
        pickle.dump(results, f)
    return results

def get_results(filename: str, dlloc: DLLocalization, rldataset: RSSLocDataset):
    print('Loading model from %s for testing' % filename)
    dlloc.load_model(filename)
    results = {'err':{}, 'truth':{}, 'preds':{}}
    run_adv_attacks = True

    for key in [rldataset.train_key] + rldataset.test_keys:
        data = rldataset.data[key]
        res = dlloc.test(dataloader=data.ordered_dataloader, save_images=False)
        err = np.array(res['error']).flatten()
        preds = np.array(res['preds'])
        truth = data.ordered_dataloader.dataset.tensors[1].cpu().numpy()
        results['err'][key] = err
        results['truth'][key] = truth * rldataset.params.meter_scale
        results['preds'][key] = preds * rldataset.params.meter_scale
        if key != rldataset.train_key and run_adv_attacks:
            dlloc.set_rss_tensor()
            x_vecs=data.ordered_dataloader.dataset.tensors[0]#[:256]
            y_vecs=data.ordered_dataloader.dataset.tensors[1]#[:256]
            img_shape = (rldataset.img_height(), rldataset.img_width())
            rldataset.params.adv_train = True
            adv_pred = batch_wrapper(
                batch_tensors=(x_vecs,y_vecs),
                func=get_all_attack_preds_without_grad,
                batch_size=128,
                return_length=6,
                kwargs={
                    'dlloc': dlloc,
                    'img_shape':img_shape,
                    'include_worst_case':False
                }
            )
            #worst_lo, worst_hi, top, drop, hilo, hilotop = adv_pred
            #for res, str in [(worst_lo, 'worst_lo'), (worst_hi, 'worst_hi'), (top, 'top'), (drop, 'drop'), (hilo, 'hilo'), (hilotop, 'hilotop')]:
            top, drop, hilo, hilotop = adv_pred
            for res, str in [(top, 'top'), (drop, 'drop'), (hilo, 'hilo'), (hilotop, 'hilotop')]:
                res = res.cpu().numpy() * rldataset.params.meter_scale
                truth = np.array(y_vecs.cpu())[:,0,1:] * rldataset.params.meter_scale
                adv_err = np.linalg.norm(truth - res, axis=1)
                results['err'][key+'_' + str] = adv_err
                results['preds'][key+'_' + str] = res
    return results 


if __name__ == '__main__':
    main()
